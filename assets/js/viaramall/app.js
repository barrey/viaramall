/* CURRENCY */
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));
    
    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

/* ADD TO CART */
$('.add-product').on('click', function(e){
	e.preventDefault();
	title = $(this).data('produk');
	alert('anda berhasil menambahkan '+title);
});