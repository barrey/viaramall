<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Underscore\Types\Arrays;

class BP_Array extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$array = new Arrays(['foo' => 'bar']);
		
echo $array->foo;
	}

}

/* End of file  */
/* Location: ./application/controllers/ */