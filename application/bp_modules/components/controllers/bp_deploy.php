<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bp_deploy extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function generate_model(){
		/**
		when use this method, don't forget to set permission to read and write files.
		*/

		//this generate model based on every table on database
		//so this don't need parameter

		$this->load->helper('file');
		$tables = $this->db->list_tables();

		foreach ($tables as $table)
		{
			$data = $this->model_text($table);

			//check if model is exist, so skip it
			if(!get_file_info(APPPATH.'models/'.$table.'_model.php', 'name')){
				if ( ! write_file(APPPATH.'models/'.$table.'_model.php', $data))
				{
				     echo 'Unable to write the file';
				}

				else
				{
				     echo 'File written!';
				     echo 'Model '.$table.'_model.php berhasil dibuat!';
				}	
			}

		} 	
	}

	private function model_text($model_name){
		return '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');

			class '.ucfirst($model_name).'_model extends MY_Model {

				public $_table = "'.$model_name.'";

				public function __construct()
				{
					parent::__construct();
					
				}

			}

			/* End of file  */
			/* Location: ./application/models/'.$model_name.'_model.php */';
	}

	public function generate_controller($controller_name, $dir = NULL){
		$this->load->helper('file');

		if(empty($dir)){
			$dir = '';
		}

		else{
			//create directory if not exist
			if (!is_dir('./application/controllers/'.$dir)) {
			    mkdir('./application/controllers/'.$dir);
			}

			$dir = $dir.'/';
		}

		//check controller file name 
		//create file
		$data = $this->controller_text($controller_name, $dir);
		if(!get_file_info(APPPATH.'controllers/'.$dir.$controller_name.'.php', 'name')){
			if ( ! write_file(APPPATH.'controllers/'.$dir.$controller_name.'.php', $data))
			{
			     echo 'Tidak bisa membuat file, coba lihat hak akses user';
			}

			else
			{
			     echo 'Controller '.$dir.$controller_name.'.php berhasil dibuat!';
			}	
		}
	}

	private function controller_text($controller_name, $dir = NULL){
		if(empty($dir) || $dir == ''){
			$dir = '';
		}

		else{
			$dir = $dir.'/';
		}

		return '<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');

			class '.ucfirst($controller_name).' extends MY_Controller {

				public function __construct()
				{
					parent::__construct();
					
				}

			}

			/* End of file  */
			/* Location: ./application/controllers/'.$dir.$controller_name.'.php */';
	}

	public function generate_view($view_name, $dir = NULL){
		$this->load->helper('file');

		if(empty($dir)){
			$dir = '';
		}

		else{
			//create directory if not exist
			if (!is_dir('./application/views/'.$dir)) {
			    mkdir('./application/views/'.$dir);
			}

			$dir = $dir.'/';
		}

		//check controller file name 
		//create file
		$data = $this->view_text($view_name, $dir);
		if(!get_file_info(APPPATH.'views/'.$dir.$view_name.'.php', 'name')){
			if ( ! write_file(APPPATH.'views/'.$dir.$view_name.'.php', $data))
			{
			     echo 'Tidak bisa membuat file, coba lihat hak akses user';
			}

			else
			{
			     echo 'View '.$dir.$view_name.'.php berhasil dibuat!';
			}	
		}
	}

	private function view_text($view_name, $dir){
		return "";
	}

	public function index(){

		//render($view, array $data = array(), $master = NULL)
		$this->render('develop/index');
	}

	public function process(){
		if($this->input->post()){
			$controller = $this->input->post('input-controller');
			$controller_dir = $this->input->post('input-controller-dir');
			$model = $this->input->post('input-model');
			$model_dir = $this->input->post('input-model-dir');

			if(!empty($controller)){
				$this->generate_controller($controller, $controller_dir);
				$this->generate_model();
			}

		}

		else{
			redirect(base_url().'develop');
		}
	}
	/** USELESS function **/
	/*
	public function index(){
		$this->render('develop/index');
	}
	*/

	public function remote_content(){
		echo "remote content";
	}
	/**end of useless function **/

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */