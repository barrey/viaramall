<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bp_image extends MY_Controller {

	public $image_file; 

	public function __construct()
	{
		parent::__construct();
		// load SimpleImage
		$library_dir = APPPATH.'third_party/include';
        require_once($library_dir . '/image/SimpleImage.php');
	}

	public function set_img($image_file){
		return $this->image_file = $image_file;
	}

	public function get_width()
	{
        $img = new abeautifulsite\SimpleImage($this->image_file);
		return $img->get_width();
	}

	public function get_height(){
        $img = new abeautifulsite\SimpleImage($this->image_file);
		return $img->get_height();
	}

	public function create(null, $width, $height, $hex_background){
		//example $img = new abeautifulsite\SimpleImage(null, 200, 100, '#000');
		$img = new abeautifulsite\SimpleImage(null, $width, $height, $hex_background);
		return $img->save();
	}

	public function resize($width, $height){
		$img = new abeautifulsite\SimpleImage($this->image_file);
		$img->resize($width, $height);
		return $img->save();
	}

	public function fit_to_width($width){
		$img = new abeautifulsite\SimpleImage($this->image_file);
		$img->fit_to_width($width);
		return $img->save();
	}

	public function fit_to_height($height){
		$img = new abeautifulsite\SimpleImage($this->image_file);
		$img->fit_to_height($height);
		return $img->save();
	}

	public function best_fit($width, $height){
		$img = new abeautifulsite\SimpleImage($this->image_file);
		$img->best_fit($width, $height);
		return $img->save();
	}

	public function convert_ext($nama_file_baru, $ext){
		$img = new abeautifulsite\SimpleImage($this->image_file);
		return $img->save($nama_file_baru.$ext);
	}
}

/* End of file bp_image.php */
/* Location: ./application/controllers/bp_image.php */