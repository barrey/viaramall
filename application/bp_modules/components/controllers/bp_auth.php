<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BP_auth extends MY_Controller {

	public $store = array();
	public $pass = FALSE;
	public $register_data = array();

	//set for login validation
	private $param_login = array(
               array(
                     'field'   => 'email',
                     'label'   => 'Email',
                     'rules'   => 'required'
                  ),
               	array(
                     'field'   => 'password',
                     'label'   => 'Password',
                     'rules'   => 'required'
                  )
            );
	private $param_login_delimiter = array('<div class="error">','</div>');

	private $param_register = array(
				/*
				array(
                     'field'   => 'username',
                     'label'   => 'Username',
                     'rules'   => 'required'
                  ),
				*/
				array(
                     'field'   => 'first_name',
                     'label'   => 'Nama depan',
                     'rules'   => 'required'
                  ),
				array(
					 'field'   => 'middle_name',
                     'label'   => 'Nama tengah',
                     'rules'   => 'trim' 
				),
				array(
					 'field'   => 'last_name',
                     'label'   => 'Nama belakang',
                     'rules'   => 'trim'
				),
               	array(
                     'field'   => 'password_register',
                     'label'   => 'Password',
                     'rules'   => 'required|matches[confirm-password_register]|min_length[8]'
                  ),
               	array(
               		'field'    => 'confirm-password_register',
               		'label'    => 'Confirm password',
               		'rules'	   => 'required'
               	),
               	array(
                     'field'   => 'email_register',
                     'label'   => 'Email',
                     'rules'   => 'required'
                  ),
               	/*
               	array(
                     'field'   => 'jk',
                     'label'   => 'Jenis Kelamin',
                     'rules'   => 'required'
                  ),
                 */
			);
	
	private $param_register_delimiter = array('<div class="error">','</div>');

	/**
	LOGIN METHOD:
		login(); 			<-- general functions.
		login_check();		<-- check whether user has login
		login_handler();	<-- login handling input from users
		login_validation();	<-- login validation
		login_serve();		<-- output to serve login form

	REGISTER METHOD:
		register();				<-- general functions.
		register_handler();		<-- register handling input from users
		register_validation();  <-- validation 
		register_serve();       <-- serve register form

	LOGOUT METHOD:
		logout();

	UNREGISTER METHOD:
		_delete_soft();
		_delete();
	**/

	public function __construct()
	{
		parent::__construct();
		$this->load->model('components/bp_auth_model', 'bp_auth_model');
		$this->bp_log = modules::load('components/bp_log');
		$this->bp_log->classname = __CLASS__;
	}

	public function login($tipe = NULL){
		//return REDIRECT or TRUE

		//serve login if users has no login
			//check login
			if(!empty($tipe)){
				$this->pass = $this->login_check($tipe);

				if($this->pass == FALSE){
					//cek url whether @base_url().'login'
					if($this->uri->segment(2) == "login"){
						//serve login form
						$this->login_serve($data = NULL,$tipe);
					}

					else{
						redirect(base_url().'auth/login'.$tipe);
					}
				}

				else{
					return TRUE;
				}
			}

			else{
				$this->pass = $this->login_check();

				if($this->pass == FALSE){
					//cek url whether @base_url().'login'
					if($this->uri->segment(2) == "login"){
						//serve login form
						$this->login_serve();
					}

					else{
						redirect(base_url().'auth/login');
					}
				}

				else{
					return TRUE;
				}
			}
	}	

	public function login_check($parameter = null){
		//by checking value is_login @ session
		if($this->session->userdata('is_login')){
			if(!empty($parameter)){
				if($this->session->userdata('admin')){
					return TRUE;
				}
				else{
					return FALSE;
				}
			}

			return TRUE;
		}

		else{
			return FALSE;
		}
	}

	public function login_handler($input, $tipe = null){
		//input can be flexible. such more than username/email and password
		//format array $input = array('username' => $username, 'password' => $password, 'email' => $email)
		$param = array(
			//add more parameter here if you want more parameter
			'email' => array('where' => $input['email']), 
			'password' => array('where' => md5($input['password'])) 
		);
		
		if(!empty($tipe)){
			if($tipe == 'admin'){
				$this->bp_log->log_info('login admin');
				$this->bp_auth_model->set_table('user_admin');
			}
		}

		else{
			$this->bp_auth_model->set_table('user');
		}

		$this->bp_auth_model->param = $param;

		$data = $this->bp_auth_model->get_one();
	
		if($data){
			$this->bp_log->log_info(__CLASS__.''.__FUNCTION__.': Login sukses');
			
			$data_set = array('id' => $data->id, 'name' => $data->first_name);
			$this->_login_set($tipe, $data_set);

			//redirect ke url yang sebelumnya ada di session
			//cari url di session
			$session_url = $this->session->userdata('url');
				if($session_url){
					redirect(base_url().$session_url);
				}

				else{
					if($tipe == 'admin'){
						redirect(base_url().'admin/admin_index');
					}
					else{
						redirect(base_url());
					}
				}
		}

		else{
			$this->bp_log->log_info(__CLASS__.'/'.__FUNCTION__.': Login gagal');
			return false;
		}

	}

	private function _login_set($tipe, $data){
		$this->session->set_userdata('is_login', TRUE);

		//kalau punya store
		$this->load->model('store_model');
		$store = $this->store_model->is_exist($data['id']);
		if($store){
			$store_id = $store->id;
			$this->session->set_userdata('store_id', $store_id);
		}


		if(!empty($tipe)){
			if($tipe == 'admin'){
				$this->session->set_userdata('admin_id', $data['id']);
				$this->session->set_userdata('name', $data['name']);
			}
		}

		else{
			$this->session->set_userdata('user_id', $data['id']);
			$this->session->set_userdata('name', $data['name']);
		}
	}

	public function login_serve($data = NULL, $tipe = NULL){
		$data['head'] = '<link rel="stylesheet" type="text/css" media="all" href="'.base_url().'assets/css/viaramall/login.css">';

		if(!empty($tipe)){
			if(is_null($data)){
				$this->render('components/bp_auth_login'.$tipe);
			}

			else{
				$this->render('components/bp_auth_login_'.$tipe, $data);
			}
		}

		else{
			if(is_null($data)){
				$this->render('components/bp_auth_login');
			}

			else{
				$this->render('components/bp_auth_login', $data);
			}
		}
	}

	public function login_validation($input){
		//RETURN TRUE / FALSE
		if($this->validation($input, $this->param_login, $this->param_login_delimiter)){
			$this->bp_log->log_info(__CLASS__.'/'.__FUNCTION__.': login validation success');
			return TRUE;
		}

		else{
			$this->bp_log->log_error(__CLASS__.'/'.__FUNCTION__.': login validation failed');
			return FALSE;
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function register(){
		//serve login if users has no login
			//check login
				$this->pass = $this->login_check();

				if($this->pass == FALSE){
					//cek url whether @base_url().'login'
					if($this->uri->segment(2) == "register"){
						//serve login form
						$datas['head'] = '<link rel="stylesheet" type="text/css" media="all" href="'.base_url().'assets/css/viaramall/login.css">';
						$datas['register_open'] = TRUE;
						$this->render('components/bp_auth_login',$datas);
					}

					else{
						$session_url = $this->session->userdata('url');
						if($session_url){
							redirect(base_url().$session_url);
						}

						else{
							redirect(base_url());
						}
					}
				}

				else{
					return TRUE;
				}
	}

	public function register_handler($input){
		if($this->register_validation($input, $this->param_register, $this->param_register_delimiter)){
			$this->bp_log->log_info(__CLASS__.'/'.__FUNCTION__.': register validation success');
			if($this->register_uniq($input)){
				$data = array(
					'first_name' => trim($input['first_name']),
					'middle_name' => trim($input['middle_name']),
					'last_name' => trim($input['last_name']),
					'email' => trim($input['email_register']), 
					'password' => md5(trim($input['password_register'])),
					'join_date' => date("Y-m-d H:i:s")
					);

				$this->bp_auth_model->insert($data);

				return $this->db->insert_id();

			}

			else{
				$this->bp_log->log_error(__CLASS__.'/'.__FUNCTION__.': email not unique');
				$data['email_not_unique'] = TRUE;
				return $data;
			}

		}

		else{
			$this->bp_log->log_error(__CLASS__.'/'.__FUNCTION__.': register validation failed');
			return FALSE;
		}
	}

	public function register_validation($input, $param_register, $param_register_delimiter){
		//RETURN TRUE / FALSE
		if($this->validation($input, $this->param_register, $this->param_register_delimiter)){
			$this->bp_log->log_info(__CLASS__.'/'.__FUNCTION__.': login validation success');
			return TRUE;
		}

		else{
			$this->bp_log->log_error(__CLASS__.'/'.__FUNCTION__.': login validation failed');
			return FALSE;
		}
	}

	public function register_uniq($input){
		//check existing email di user
			$where = array('email' => array('where' => $input['email_register']));
			$this->bp_auth_model->set_param($where);
		
			if($this->bp_auth_model->check('insert')){
				return TRUE;
			}

			else{
				return FALSE;
			}
	}

	public function register_serve($data = null){
		//hacked layout
		if($this->uri->segment(2) == "register"){
			$datas['register_open'] = TRUE;
		}

		if(is_array($data)){
			if(!empty($data['email_not_unique'])){
				$datas['register_error'] = TRUE;
			}
		}

		$datas['head'] = '<link rel="stylesheet" type="text/css" media="all" href="'.base_url().'assets/css/viaramall/login.css">';
		$this->render('components/bp_auth_login', $datas);

	}

	private function _provide_captcha(){
		//method to provide captcha
	}
	
	public function unregister($input){
		//ganti status
		$param = array(
			//add more parameter here if you want more parameter
			'id' => array('where' => $input['id'])
		);

		$this->_delete_soft($param);
	}

	private function _delete_soft($param){
		$this->bp_auth_model->set_table('bp_user');
		$this->bp_auth_model->param = $param;
		$data = array('status' => 'n');
		$this->bp_auth_model->update($data);
	}

	private function _delete($param){
		$this->bp_auth_model->set_table('bp_user');
		$this->bp_auth_model->param = $param;
		$this->bp_auth_model->delete();
	}


}

/* End of file  */
/* Location: ./application/controllers/ */	