<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Debug extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// load KINT
		$library_dir = APPPATH.'third_party/include';
        require_once($library_dir . '/kint/Kint.class.php');

        if(ENVIRONMENT == 'production'){
	        Kint::enabled(false);
        }
	}

	public function dump($data)
	{
		Kint::dump( $data );
	}

	public function trace()
	{
		Kint::trace();
	}

}

/* End of file  */
/* Location: ./application/controllers/ */