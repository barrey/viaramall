<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bp_blog extends MY_Controller {

	protected $category_param;

	public function __construct()
	{
		parent::__construct();
	}

	public function editor_md($id_blogpost = NULL){
		//method to serve bootstrap markdown editor
		if(!empty($id_blogpost)){
			$data = $this->_editor_content($id_blogpost);
			$this->render('components/bp_blog_md', $data);
		}

		else{
			$this->render('components/bp_blog_md');
		}
	}

	public function editor($id_blogpost = NULL){
		//method to serve bootstrap texteditor
		if(!empty($id_blogpost)){
			$data = $this->_editor_content($id_blogpost);
			$this->render('components/bp_blog_editor', $data);
		}

		else{
			$this->render('components/bp_blog_editor');
		}
	}

	private function _editor_content($id_blogpost){
		return array();
	}

	public function publish($id_blogpost){
		//method to insert to database
	}

	public function publish_delay($id_blogpost, $date){

	}

	public function save(){
		//method to save blog as draft
	}

	public function delete($id_blogpost){

	}

	public function display($limit){

	}

	public function display_draft($limit){

	}

	public function tag(){

	}

	public function visibility(){

	}

	/**
	CATEGORY METHOD:
		category_insert();
		category_del();
		category_get();
		category_update();
		category_param();
	**/

	public function category_insert(){

	}

	public function category_del(){

	}

	public function category_get(){

	}

	public function category_update($data){
		$this->bp_blog->set_param($this->category_param);
		$this->bp_blog->update($data);
	}

	public function category_param($param){
		return $this->category_param;
	}

	/**
	COMMENTS METHOD:
		integrate_disqus();
		comments_get();
		comments_del();
		comments_insert();
	**/
	public function integrate_disqus(){
		//method to integrate disqus, so we don't have to interact with database
	}

	public function comments_get(){

	}

	public function comments_del(){

	}

	public function comments_insert(){

	}

	public function comments_reply(){

	}

	public function comments_serve(){
		$this->editor_md();
	}
	
	public function index(){
		echo "tes";
	}

}

/* End of file  */
/* Location: ./application/controllers/ */