<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Visitor extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function session(){
		$this->debug($this->session->all_userdata());
		$id = $this->session->userdata('user_id');
		$this->load->model('store_model');
		$data = $this->store_model->is_exist($id);
		$this->debug($data);
	}

}

/* End of file visitor.php */
/* Location: ./application/controllers/visitor.php */