<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\BrowserConsoleHandler;

class BP_log extends MY_Controller {

	public $log;
	public $classname;

	public function __construct()
	{
		parent::__construct();

		$this->log = new Logger('BP_LOG_SYSTEM');
		$this->log->pushHandler(new BrowserConsoleHandler());
	}

	public function log_func_name($funcname){
		if(ENVIRONMENT == "development"){
			return $this->log->addInfo($this->classname.'/'.$funcname);
		}
	}

	public function log_info($info){
		if(ENVIRONMENT == "development"){
			return $this->log->addInfo($info);
		}
	}

	public function log_error($info){
		if(ENVIRONMENT == "development"){
			return $this->log->addError($info);
		}
	}

	public function log_warning($info){
		if(ENVIRONMENT == "development"){
			return $this->log->addWarning($info);
		}
	}

	public function log_notice($info){
		if(ENVIRONMENT == "development"){
			return $this->log->addNotice($info);
		}
	}

	public function log_alert($info){
		if(ENVIRONMENT == "development"){
			return $this->log->addAlert($info);
		}
	}

	public function log_record($array_infolog, $info){
		//array $array_infolog contains: id_user, id_reflog, id_refmenu, date_log, ip_visitor

		//save to db
		if($this->bp_log->insert($array_infolog)){
			$info = "Success saving log to database";
			return $this->log_info($info);
		}

		else{
			$info = "Failed saving log to database";
			return $this->log_error($info);
		}
	}

}

/* End of file log.php */
/* Location: ./application/controllers/log.php */