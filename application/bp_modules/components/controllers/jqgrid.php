<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jqgrid extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function table($parameter){
		$data['sortorder'] = 'asc';
		$data['rownum'] = 20;
		$data['autowidth'] = 'true';
		$data['height'] = "auto";
		$data['rowlist'] = json_encode(array(10,20,30));
		$data['multiselect'] = 'false';
		$data['page'] = 'null';
		$data['class_pencarian'] = '.form-pencarian-jqgrid';
		$data['sorttype'] = 'integer';

		if(!empty($parameter['subgrid']))
		{
			$data['sortorder_subgrid'] = 'asc';
			$data['rownum_subgrid'] = 20;
			$data['rowlist_subgrid'] = $data['rowlist'];
			$data['height_subgrid'] = "auto";
			$data['postdata_subgrid'] = "row['id']";
		}

		$parse = array_merge($data,$parameter);
		return $this->load->view('components/jqgrid/view_jqgrid_table',$parse,true);
	}

	public function add($parameter){
		$data['title_add'] = 'Tambah data';
		$data['icon'] = 'ui-icon-plus';
		$parse = array_merge($data, $parameter);
		return $this->load->view('components/jqgrid/view_jqgrid_add',$parse,true);
	}

	public function edit($parameter){

	}

	public function edit_popup($parameter){

	}

}

/* End of file  */
/* Location: ./application/controllers/ */