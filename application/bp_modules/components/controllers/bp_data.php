<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BP_data extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bp_data_model','data_model');
	}

	public function get_province(){
		$data = $this->data_model->m_get_province();
		return $data;
	}

	public function get_province_json(){
		$data = $this->get_province();
		echo json_encode($data);
	}

	public function get_regency($id_bp_province){
		$data = $this->data_model->m_get_regency($id_bp_province);
		return $data;
	}

	public function get_regency_json($id_bp_province){
		$data = $this->get_regency($id_bp_province);
		echo json_encode($data);
	}

	public function get_regency_json_select2($id_bp_province){
		$data = $this->get_regency($id_bp_province);
		foreach ($data as $d) {
			$result[] = array('id' => $d->id, 'text' => $d->name);
		}
		echo json_encode($result);
	}

	public function get_subdistrict($id_bp_regency){
		$data = $this->data_model->m_get_subdistrict($id_bp_regency);
		return $data;
	}

	public function get_subdistrict_json($id_bp_regency){
		$this->get_subdistrict($id_bp_regency);
		echo json_encode($data);
	}

	public function get_subdistrict_json_select2($id_bp_regency){
		$data = $this->get_subdistrict($id_bp_regency);
		foreach ($data as $d) {
			$result[] = array('id' => $d->id, 'text' => $d->name);
		}
		echo json_encode($result);
	}


	/*** UNTESTED **/
	public function get_province_autocomplete($char){
		$char_data = strtolower($char);
		$data = $this->data_model->m_get_province_autocomplete($char_data);
		echo json_encode($data);
	}

	public function get_regency_autocomplete($char){
		$char_data = strtolower($char);
		$data = $this->data_model->m_get_regency_autocomplete($char_data);
		echo json_encode($data);
	}

	public function get_subdistrict_autocomplete($char){
		$char_data = strtolower($char);
		$data = $this->data_model->m_get_subdistrict_autocomplete($char_data);
		echo json_encode($data);
	}

	public function get_village(){
		//gak dipake
	}

	/** END OF UNTESTED **/
}

/* End of file bp_data.php */
/* Location: ./application/controllers/bp_data.php */