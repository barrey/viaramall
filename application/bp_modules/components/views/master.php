<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/bootstrap/font-awesome.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jquery-ui/jquery-ui-1.10.3.custom.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jqgrid/ui.jqgrid.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jqgrid/jqGrid.overrides.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/general.css">

	<script src="<?php echo base_url()?>assets/js/head.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jqgrid/grid.locale-id.js"></script>
	<script src="<?php echo base_url()?>assets/js/jqgrid/jquery.jqGrid.min.js"></script>
	<?php
		if(!empty($head))
		{
			echo $head;
		}
	?>
</head>
<body>
	<?php
		if(!empty($header))
		{
			echo $header;
		}
	?>

	<?php
		if(!empty($content))
		{
			echo $content;
		}
	?>

	<?php 
		if(!empty($footer))
		{
			echo $footer;
		}
	?>
	<?php
		if(!empty($arr))
		{
			foreach($arr as $data)
			{
				echo $data.'<br />';
			}
		}
	?>

	<script src="<?php echo base_url()?>assets/js/jquery.form-3.5.js"></script>
	


</body>
</html>