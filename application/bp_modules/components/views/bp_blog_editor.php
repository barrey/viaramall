<!-- should be able to customize like width height and theme -->
<!-- summernote editor -->
<script>
	head.load('<?php echo base_url()?>assets/css/bootstrap-wysiwyg/summernote.css', 
		'<?php echo base_url()?>assets/js/bootstrap-wysiwyg/summernote.js', function(){
		console.log('bootstrap editor loaded');
		
		$(document).ready(function() {
	      $('.bp-editor').summernote({
	        height: 300,
	        tabsize: 2,
	        codemirror: {
	          theme: 'monokai'
	        }
	      });
	    });

	});
	
</script>

<div id="text-editor" class="bp-editor">
	<?php if(!empty($content)){ echo $content; } ?>
</div>