<form action="<?php echo base_url()?>auth/register" method="POST" role="form">
	<legend>Form Register</legend>

	<div class="form-group">
		<label for="">Username</label>
		<input type="text" class="form-control" name="username" id="" placeholder="Input field" value="<?php echo set_Value('username');?>">
		<?php echo form_error('username'); ?>
	</div>

	<div class="form-group">
		<label for="">Email</label>
		<input type="text" class="form-control" name="email" id="" placeholder="Input field" value="<?php echo set_value('email');?>">
		<?php echo form_error('email'); ?>
	</div>

	<div class="form-group">
		<label for="">Password</label>
		<input type="password" class="form-control" name="password" id="" placeholder="Input field">
		<?php echo form_error('password'); ?>
	</div>

	<button type="submit" class="btn btn-primary">Login</button>
</form>