<!doctype html>
<html lang="en">
	<head>
		<!-- Basic page needs
		============================================ -->
		<title>ShopMe | Home</title>
		<meta charset="utf-8">
		<meta name="author" content="">
		<meta name="description" content="">
		<meta name="keywords" content="">

		<!-- Mobile specific metas
		============================================ -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="images/fav_icon.ico">

		<!-- Google web fonts
		============================================ -->
		<!--<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>-->
		
		<!-- Libs CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/viaramall/animate.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/viaramall/fontello.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/viaramall/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/js/viaramall/owlcarousel/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">

		
		<!-- Theme CSS
		============================================ -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/js/viaramall/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/js/viaramall/arcticmodal/jquery.arcticmodal.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/viaramall/style.css">

		<!-- JS Libs
		============================================ -->
		<script src="<?php echo base_url()?>assets/js/viaramall/modernizr.js"></script>

	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/bootstrap/font-awesome.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jqgrid/ui.jqgrid.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jqgrid/jqGrid.overrides.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/frontend.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/js/jquery-ui-bootstrap/jquery-ui-1.9.2.custom.css">



	<script src="<?php echo base_url()?>assets/js/head.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery-ui-bootstrap/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jqgrid/grid.locale-id.js"></script>
	<script src="<?php echo base_url()?>assets/js/jqgrid/jquery.jqGrid.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/datepicker/moment.js"></script>
	<script src="<?php echo base_url()?>assets/js/autonumeric/jquery.numeric.min.js"></script>
	
	
	<?php
		if(!empty($head))
		{
			echo $head;
		}
	?>
</head>
<body class="front_page">
	<?php
		if(!empty($header))
		{
				echo $header;
		}
	?>

	<?php
		if(!empty($content))
		{
			echo $content;
		}
	?>

	<?php 
		if(!empty($footer))
		{
			echo $footer;
		}
	?>
	<?php
		if(!empty($arr))
		{
			foreach($arr as $data)
			{
				echo $data.'<br />';
			}
		}
	?>

	<script src="<?php echo base_url()?>assets/js/jquery.form-3.5.js"></script>
	<!-- Include Libs & Plugins
		============================================ -->
	<script src="<?php echo base_url(); ?>assets/js/viaramall/queryloader2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/jquery.elevateZoom-3.0.8.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/fancybox/source/helpers/jquery.fancybox-media.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/jquery.appear.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/owlcarousel/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/jquery.countdown.plugin.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/jquery.countdown.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/arcticmodal/jquery.arcticmodal.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/colorpicker/colorpicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/viaramall/retina.min.js"></script>
	<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>

	<!-- Theme files
	============================================ -->
	<script src="<?php echo base_url()?>assets/js/viaramall/theme.plugins.js"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/theme.core.js"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/app.js"></script>


</body>
</html>