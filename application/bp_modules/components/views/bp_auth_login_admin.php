<script>
	$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

	<?php if(!empty($login_error)): ?>
		$('#login-form div').eq(2).before('<p>Kombinasi email atau password salah.</p>');
	<?php endif; ?>
});
</script>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-6">
							<a href="#" class="active" id="login-form-link">Login</a>
						</div>
						<div class="col-xs-6">
							<a href="#" id="register-form-link">Daftar</a>
						</div>
					</div>
					<hr>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<form id="login-form" action="<?php echo base_url()?>auth/login/admin" method="post" role="form" style="display: block;">
								<div class="form-group">
									<input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email" value=""><br>
									<?php echo form_error('email'); ?>
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password"><br>
									<?php echo form_error('password'); ?>
								</div>
								<div class="form-group text-center">
									<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
									<label for="remember"> Ingatkan saya</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Masuk">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="text-center">
												<a href="<?php echo base_url()?>login/forget_password" tabindex="5" class="forgot-password">Lupa Password?</a>
											</div>
										</div>
									</div>
								</div>
							</form>
							<form id="register-form" action="<?php echo base_url()?>auth/login/register" method="post" role="form" style="display: none;">
								<div class="form-group">
									<input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" placeholder="Nama depan" value="<?php echo set_value('first_name'); ?>"><br>
									<?php echo form_error('first_name'); ?>
								</div>
								<div class="form-group">
									<input type="text" name="middle_name" id="middle_name" tabindex="1" class="form-control" placeholder="Nama tengah" value="<?php echo set_value('middle_name'); ?>"><br>
									<?php echo form_error('middle_name'); ?>
								</div>
								<div class="form-group">
									<input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" placeholder="Nama belakang" value="<?php echo set_value('last_name'); ?>"><br>
									<?php echo form_error('last_name'); ?>
								</div>
								<br>
								<div class="form-group">
									<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
									<input type="hidden" name="tipe" value="admin">
									<br>
									<?php echo form_error('username'); ?>
								</div>
								<div class="form-group">
									<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value=""><br>
									<?php echo form_error('email'); ?>
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password"><br>
									<?php echo form_error('password'); ?>
								</div>
								<div class="form-group">
									<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password"><br>
									<?php echo form_error('confirm-password'); ?>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Daftar">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>