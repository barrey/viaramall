$('<?php echo $id_jqgrid ?>').on("click",'<?php echo $class_link_jqgrid ?>', function(e){
	e.preventDefault();
	var host = document.URL.replace("#","");
	var classes = $(this).attr("class");
	var class_extract = classes.split(" ");
	var status = class_extract[1];
	var idref = $(this).data('id');
	var del = $(this).data('status');
  /**
  send data to change status
  */
  var link = "<?php echo $url_to_process ?>";
  $.ajax({
      url: link,
      type: 'POST',
      
      //contentType: "application/json; charset=utf-8",
      dataType: 'html',
      beforeSend: function(response){
            $("#loadingstatus").show();
          },
         data: { id: idref, status: del },
          success: function(response) {
            set_status(response);
            $('#newapi<?php echo $gridname;?>').trigger("reloadGrid");
          }
  });
});