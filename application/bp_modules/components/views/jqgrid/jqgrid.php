<?php
echo Modules::run('dashboard/components/head_content');
?>

<script type="text/javascript">
var base_url = '<?php echo base_url() ?>';
var site_url = '<?php echo site_url() ?>';
$(document).ready(function(){
	<?php echo $table; ?>
</script>
<!--
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption"><i class="icon-search"></i>Pencarian</div>
        <div class="tools">
            <a class="expand" href="javascript:;"></a>
        </div>
    </div>
    <div class="portlet-body form" style="display: none;">
        <form class="form-horizontal" id="form_pencarian">
        <div class="control-group fc">
            <label class="control-label">Kode BPS Kabupaten</label>
            <div class="controls">
                <input id="kode_bps_kabupaten" name="code" class="span11" type="text" placeholder="Kode BPS Kabupaten">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label">Nama Kabupaten</label>
            <div class="controls">
                <input id="nama_kabupaten" name="name" type="text" class="span11" placeholder="Nama Kabupaten">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label">Kode BPS Propinsi</label>
            <div class="controls">
                <input id="kode_bps_propinsi" name="code_province" type="text" class="span11 " placeholder="Kode BPS">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label">Nama Propinsi</label>
            <div class="controls">
                <input id="nama_propinsi" name="name_province" type="text" class="span11" placeholder="Nama Propinsi">
                <input type="hidden" name="cari" value="TRUE"/>
            </div>
        </div>
        <button class="btn blue purple-stripe ml180" type="button" onclick="gridReload()">Cari
            <i class="m-icon-swapright m-icon-white"></i>
        </button>
        <button class="btn blue red-stripe" type="reset">Batal <i class=" icon-remove-sign"></i>
        </button>
    </form>
    </div>
</div>
-->
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption"><i class="icon-search"></i>Pencarian</div>
        <div class="tools">
            <a class="expand" href="javascript:;"></a>
        </div>
    </div>
    <div class="portlet-body mapworld form" style="display: none;">
        <form class="form-horizontal" id="form_pencarian">
        <div class="control-group fc">
            <label class="control-label putih">Kode BPS Kabupaten</label>
            <div class="controls">
                <input id="kode_bps_kabupaten" name="code" class="m-wrap medium putih" type="text" placeholder="Kode BPS Kabupaten">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label putih">Nama Kabupaten</label>
            <div class="controls">
                <input id="nama_kabupaten" name="name" type="text" class="m-wrap medium putih" placeholder="Nama Kabupaten">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label putih">Kode BPS Propinsi</label>
            <div class="controls">
                <input id="kode_bps_propinsi" name="code_province" type="text" class="m-wrap small putih" placeholder="Kode BPS">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label putih">Nama Propinsi</label>
            <div class="controls">
                <input id="nama_propinsi" name="name_province" type="text" class="m-wrap medium putih" placeholder="Nama Propinsi">
                <input type="hidden" name="cari" value="TRUE"/>
            </div>
        </div>
        <button class="btn blue purple-stripe ml180" type="button" onclick="gridReload()">Cari
            <i class="m-icon-swapright m-icon-white"></i>
        </button>
        <button class="btn blue red-stripe" type="reset">Batal <i class=" icon-remove-sign"></i>
        </button>
    </form>
    </div>
</div>
<!--
<a href="#" id="pencarian" class="no_underline">
    <div class="alert alert-info mb0 fc">
        <i class="icon-search"></i><span class="f14">&nbsp;Pencarian</span>
    </div>
</a>
<div id="form_pencarian" class="alert alert-info hidden">
    <form class="form-horizontal">
        <div class="control-group fc">
            <label class="control-label">Kode BPS Kabupaten</label>
            <div class="controls">
                <input class="m-wrap medium" type="text" placeholder="Kode BPS Kabupaten">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label">Nama Kabupaten</label>
            <div class="controls">
                <input type="text" class="m-wrap medium" placeholder="Nama Kabupaten">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label">Kode BPS Propinsi</label>
            <div class="controls">
                <input type="text" class="m-wrap small" placeholder="Kode BPS">
            </div>
        </div>
        <div class="control-group fc">
            <label class="control-label">Nama Propinsi</label>
            <div class="controls">
                <input type="text" class="m-wrap medium" placeholder="Nama Propinsi">
            </div>
        </div>
        <button class="btn blue purple-stripe ml180" type="button">Cari
            <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </form>
</div>
-->

<div class="span11" id="jqgrid_container" style="margin-bottom: 40px; margin-left: 0;">
<!-- jqGrid -->
<table class="gridcontainer span11" id="newapi<?php echo $grid['gridname'];?>"></table> 
<div class="span11" id="pnewapi<?php echo $grid['gridname'];?>"></div>
<!-- end jqGrid -->
</div>
<div id="dialogKabupaten"></div>