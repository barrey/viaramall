$('#newapi<?php echo $gridname?>').jqGrid({
	url: '<?php echo $dataurl?>',
    datatype: 'json',
    autowidth: <?php echo $autowidth?>,
    height: '<?php echo $height?>',
    pager: '#pnewapi<?php echo $gridname?>',
    caption: '<?php echo $caption?>',
    colModel: <?php echo $colmodel?>,
    ajaxGridOptions : {type:"POST"},
    sorttype: "<?php echo $sorttype?>",
    sortname:"<?php echo $sortname?>",
    sortorder: "<?php echo $sortorder?>",

    <?php if(!empty($subgrid)):?>

        subGrid: true,
        subGridOptions: {
          "reloadOnExpand" : false,
          "selectOnExpand" : true },
        subGridRowExpanded: function(subgrid_id, row_id){
            var subgrid_table_id, pager_id;
            var row = jQuery('#newapi<?php echo $gridname; ?>').getRowData(row_id);
            subgrid_table_id = subgrid_id + "_t";
            pager_id = "p_" + subgrid_table_id;
            $('#' + subgrid_id).html('<table id="' + subgrid_table_id + '" class="scroll"></table><div id="' + pager_id + '" class="scroll"></div>');
            jQuery('#' + subgrid_table_id).jqGrid({
                url: '<?php echo $dataurl_subgrid ?>',
                ajaxGridOptions: {type: 'POST'},
                postData: {'<?php echo $post_subgrid?>': <?php echo $postdata_subgrid; ?>},
                jsonReader: {
                    root: 'data',
                    repeatitems: false
                },
                datatype: 'json',
                colModel: <?php echo $colmodel_subgrid; ?>,
                rowNum: <?php echo $rownum_subgrid; ?>,
                rowList: <?php echo $rowlist_subgrid; ?>,
                pager: pager_id,
                viewrecords: true,
                sortname: '<?php echo $sortname_subgrid; ?>',
                sortorder: '<?php echo $sortorder_subgrid; ?>',
                height: '<?php echo $height_subgrid; ?>'
            });
            jQuery('#' + subgrid_table_id).jqGrid('navGrid', '#' + pager_id, {edit: false, add: false, del: false, search: false,view: true}
                );
        },
    
    <?php endif; ?>
    
    <?php if(!empty($treegrid)):?>

        gridview: true,
        treeGrid: true,
        treeGridModel: 'adjacency',
        ExpandColumn: '<?php echo $colexpand_treegrid; ?>',
        ExpandColClick: true,
        jsonReader: { repeatitems: false, root: function (obj) { return obj; } },

    <?php endif; ?>
    
    <?php if(empty($treegrid)):?>
        rowNum: '<?php echo $rownum?>',
        jsonReader : {
            root:"data",
            repeatitems: false
        },
        rowList:<?php echo $rowlist?>,
        altRows: true,
        multiselect: <?php echo $multiselect?>,
        viewrecords: true,
    <?php endif;?>
     
});

$("#newapi<?php echo $gridname;?>").jqGrid('navGrid',
'#pnewapi<?php echo $gridname;?>',
    {
        view:false,
        edit:false,
        add:false,
        del:false,
        search: false,
        beforeRefresh: function () {//refresh button: reset order & search params
            $("<?php echo $class_pencarian?>").find('input').val('');
            $("#newapi<?php echo $gridname;?>").jqGrid('setGridParam', {
                sortname: '<?php echo $sortname?>',
                sortorder: '<?php echo $sortorder?>',
                search: false,
                page: <?php echo $page ?>
            }).trigger('reloadGrid');
            
            $("span.s-ico",$(this)[0].grid.hDiv).hide(); //hide sort icons
        },
    },
    {closeAfterEdit:true,mtype: 'POST'}/*edit options*/,
    {closeAfterAdd:true,mtype: 'POST'} /*add options*/,
    {mtype: 'POST'} /*delete options*/,
    {sopt:['eq','cn','ge','le'],
    overlay:false,mtype: 'POST'}/*search options*/,
    {width: 500}
);


// Responsive handler dan CollumnChooser
/*
if( typeof(responsiveHandler) == "undefined" ){
    responsiveHandler = function(){
        var lebar_container = $('.jqgrid-content').width();
        $(".grid-container").setGridWidth(lebar_container, 'shrink');
        $(".grid-container").trigger('resize');
        console.log('resizing');
        return false;
    }
}
responsiveHandler.call();
*/
//App.addResponsiveHandler(responsiveHandler);

$(window).bind('resize', function() {
  var lebar_container = $('.jqgrid-content').width();
  $(".grid-container").setGridWidth(lebar_container, 'shrink');
}).trigger('resize');

/* 
el = '<a class="column-chooser-toggler ui-jqgrid-titlebar-setting " href="javascript:void(0)" role="link" style="color:white;"><i class="fa fa-gear"></i></a>';
$('#gview_newapi<?php echo $gridname;?> > .ui-jqgrid-titlebar').append(el);
*/

$('#gview_newapi<?php echo $gridname;?> .column-chooser-toggler').click(function(){
    /*$("#newapi<?php echo $gridname;?>").jqGrid('columnChooser', {
        "done": responsiveHandler
    });*/
});