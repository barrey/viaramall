$("#newapi<?php echo $gridname;?>").navGrid("#pnewapi<?php echo $gridname;?>",{})
  .navButtonAdd("#pnewapi<?php echo $gridname;?>",{
      caption:"",
      title:"<?php echo $title_add ?>",
      buttonicon:"<?php echo $icon ?>",
      onClickButton: function(){
          $('<?php echo $div_loader ?> .modal-content').load( "<?php echo $url_to_load ?>");
			  $('<?php echo $div_loader ?>').modal({backdrop: 'static', keyboard: false, show: true});
          return false;
      },
 position:"first"
});