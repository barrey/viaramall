<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bp_data_model extends MY_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}

	public function m_get_province(){
		$this->set_table('bp_province');
		$where = array('del' => array('where' => 'n'));
		$this->set_param($where);
		
		return $this->get_all();
	}

	public function m_get_regency($id_bp_province){
		$this->set_table('bp_regency');
		$where = array('del' => array('where' => 'n'), 'id_bp_province' => array('where' => $id_bp_province));
		$this->set_param($where);

		return $this->get_all();
	}

	public function m_get_subdistrict($id_bp_regency){
		$this->set_table('bp_subdistrict');
		$where = array('del' => array('where' => 'n'), 'id_bp_regency' => array('where' => $id_bp_regency));
		$this->set_param($where);

		return $this->get_all();
	}

	public function m_get_village(){

	}

	/*** LIST ***/

	public function m_get_province_autocomplete($char){
		$this->set_table('bp_province');
		
		$where = array(
			'del' => array('where' => 'n'), 
			'name' => array('like' => $char), 
			'limit' => array(0,10), 
			//'order_by' => array('name','asc')
		);

		$this->set_param($where);
		return $this->get_all();
	}

	public function m_get_regency_autocomplete($char){
		$this->set_table('bp_regency');
		
		$where = array(
			'del' => array('where' => 'n'), 
			'name' => array('like' => $char), 
			'limit' => array(0,10),
		);

		$this->set_param($where);
		return $this->get_all();
	}

	public function m_get_subdistrict_autocomplete($char){
		$this->set_table('bp_subdistrict');

		$where = array(
			'del' => array('where' => 'n'),
			'name' => array('like' => $char),
			'limit' => array(0,10),
		);

		$this->set_param($where);
		return $this->get_all();
	}
}

/* End of file bp_data_model.php */
/* Location: ./application/models/bp_data_model.php */