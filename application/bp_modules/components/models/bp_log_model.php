<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bp_log_model extends Public_Model {

	public $_table = 'bp_log';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file  */
/* Location: ./application/models/ */