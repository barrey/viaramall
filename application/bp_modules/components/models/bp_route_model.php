<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bp_route_model extends MY_Model {

	public $variable;
	public $set_param;
	public $_table = 'app_routes';

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all(){      
		#first, we set the rows, means LIMIT default is 20
        if(isset($_POST['rows'])){
            $limit=$this->input->get_post('rows');
        }
        else{
            $limit=20;
        }

        #second, we read whether there's a PAGE-ing request
        if(isset($_POST['page'])){
            $page=$this->input->get_post('page');
        }else{
            $page=1;
        }

        #third, we read the sort
        if(!empty($_POST['sidx'])){
            $sidx=$this->input->get_post('sidx');
            //$sidx = $this->col_identifier($sidx);
        }else{
            $sidx="id";
        }

        #fourth, read sorting method "desc" or "asc"
        if(isset($_POST['sord'])){
            $sord=$this->input->get_post('sord');
        }else{
            $sord="desc";
            $sord = $this->col_identifier($sord);
        }

        $this->db->from($this->_table);

        #buat record pencarian
        if(!empty($_POST['pencarian']))
        {
            $kode_bps_desa = str_replace('_', '', $this->input->post('kode_bps_desa'));
            $nama_desa = $this->input->post('nama_desa');
            $nama_propinsi = $this->input->post('nama_propinsi');
            if(!empty($kode_bps_desa))
            {
                $this->db->like('a.code', $kode_bps_desa);
            }
            if(!empty($nama_desa))
            {
                $this->db->like('a.name', $nama_desa);
            }
            if(!empty($nama_propinsi))
            {
                $this->db->like('d.name', $nama_propinsi);
            }
        }

        $q = $this->db->get();
        $count = $q->num_rows();
        
        if($count>0){
            $total_pages=ceil($count/$limit);
        }else{
            $total_pages=1;
        }
        if($page>$total_pages){
            $data['page']=$total_pages;
        }
        $start=($limit*$page)-$limit;
        $this->db->limit($limit,$start);
        $this->db->order_by("$sidx","$sord");

        	$this->db->from($this->_table);

            #buat result pencarian
            if(!empty($_POST['pencarian']))
            {
                if(!empty($kode_bps_desa))
                {
                    $this->db->like('a.code', $kode_bps_desa);
                }

                if(!empty($nama_desa))
                {
                    $this->db->like('a.name', $nama_desa);
                }

                if(!empty($nama_propinsi))
                {
                    $this->db->like('d.name', $nama_propinsi);
                }
            }

        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            foreach ($query->result() as $key) 
            {
            	/*
                    if($key->del=="y")
                    {
                        $status = '<a href="#" class="edit-status-desa offline" data-id="'.$key->id.'" data-status="'.$key->del.'">&nbsp;</a>';
                    }
                    else
                    {
                        $status = '<a href="#" class="edit-status-desa online" data-id="'.$key->id.'" data-status="'.$key->del.'">&nbsp;</a>';
                    }
                   
                    $name='<a href="#" title="Sunting" class="edit-dialog-desa link-biru" data-id="'.$key->id.'" alt="Sunting" style="font-weight:">'.$key->name.'</a>';

                    */
                    # code...
                    $record_items[] = array(
                          
                                'id' => $key->id,
                				'slug' => $key->slug,
                				'controller' => $key->controller,
                				'del' => $key->del,
                    );
            }
            $data['records']= $query->num_rows();
            $data['data'] = $record_items;
            $data['total'] = $total_pages;
            if(empty($page)){
            $data['page'] = 1;
            }else{
                $data['page']=$page;
            }        
            return $data;
        }
        else
        {
            //daripada return FALSE yang jqgrid gak kenalin
            //mending di return data record 0 aja
            $data['data'] = 0;
            $data['total'] = 0;
            $data['records'] = 0;
            return $data;
        }	
	}
}	

/* End of file  */
/* Location: ./application/models/ */