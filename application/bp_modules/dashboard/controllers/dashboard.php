<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\BrowserConsoleHandler;

class Dashboard extends MY_Controller {

	public $log;



	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		$this->load->model('user_model','user');
		$this->log = new Logger('name');
		$this->log->pushHandler(new BrowserConsoleHandler());
		$this->load->library(array('array_lib', 'parse_lib'));
	}
	public function index()
	{
		echo "tes";
		echo "Halo bro, you're in dashboard";
		// create a log channel

		/*
		$bp_auth=modules::load('components/bp_auth');
		$bp_auth->login();
		*/
	}

	public function login(){
		echo "login$";
		$bp_auth=modules::load('components/bp_auth');
		$bp_auth->login();
	}
	
	public function tes()
	{
		$this->config->load('my_config');
		$data['content'] = "Content";
		$data['header'] = "Header" ;
		$data['footer'] = "Footer";
		$data['arr']['tes'] = "tes";
		$data['arr']['tes2'] = "tes2";
		$data['arr']['tes3'] = "tes3";
		$data['arr']['script'] = $this->config->item('jquery');
		$data['arr']['css'] = $this->config->item('bootstrap')['css'];
		$this->load->view('components/master.php',$data);
	}
	public function home()
	{
		echo "Hei, nice to see you :)";
		echo "<br />";
		echo var_dump($this->user->get_all());
		//echo $this->user_model->tes();
		if(method_exists('dashboard', '_content'))
		{
			echo $data['content']=Modules::run('dashboard/_content');
			echo '<br />';
		}


		/**
		The right way to load cross controller/ component
		*/
		$objek=modules::load('dashboard/component');
		if(method_exists($objek, 'index'))
		{
			echo $objek->index();
		}
		
		/**
		end of load components
		*/

		echo $this->user->out();

	}
	public function _content()
	{
		echo "_COntent";
	}

	public function testes(){
		echo "&lt;?php";
	}

	public function register(){
		$this->bp_auth->register_data = array(
				'username' => 'dsdsdbukanadmin',
				'email' => 'reanmla', 
				'password' => '223',
				'status' => 'y'
			);

		$this->bp_auth->register();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */