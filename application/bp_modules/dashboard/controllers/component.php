<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo "Index dari module component";
	}

	public function eror404(){
		echo "Page not found bro :(";
	}

}

/* End of file  */
/* Location: ./application/controllers/ */