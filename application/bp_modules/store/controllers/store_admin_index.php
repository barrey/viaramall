<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_admin_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->admin_index = modules::load('admin/admin_index');
		$this->admin_index->check_login_admin();
	}

	public function index()
	{
		$data['sidebar'] = $this->load->view('admin/components/sidebar','',TRUE);
		$this->render('store/store_index',$data,'components/backend');
	}

	public function _content(){
		$col_model = json_encode(array(
				array('name' => 'store.id', 'label' => 'ID', 'hidden' => 1, 'classes' => 'hidden'),
				array('name' => 'store.name', 'label' => 'Nama Toko', 'width' => 300, 'align' => 'left'),
				array('name' => 'store.created_date', 'label' => 'Tanggal Buka Toko', 'width' => 100, 'align' => 'left'),
				array('name' => 'store.province', 'label' => 'Lokasi Toko (Propinsi)', 'width' => 150, 'align' => 'left'),
				array('name' => 'store.regency', 'label' => 'Lokasi Toko (Propinsi)', 'width' => 150, 'align' => 'left'),
				array('name' => 'store.status', 'label' => 'Status Toko', 'width' => 100, 'align' => 'center')
			));	

		$data = array(
				'gridname' => $gridname = '_'.time(),
				'dataurl' => site_url(''),
				'rownum' => 20,
				'caption' => 'Daftar Toko',
				'colmodel' => $col_model,
				'sortname' => 'store.name'
			);

		//pemanggilan cross controller [display table jqgrid]

		//pemanggilan cross controller [feature view toko]

		//load view 
		$data['sidebar'] = $this->load->view('admin/components/sidebar','',TRUE);
		$this->render('store/store_admin_index', $data, 'components/backend');
	}
}

/* End of file store_index.php */
/* Location: ./application/controllers/store_index.php */