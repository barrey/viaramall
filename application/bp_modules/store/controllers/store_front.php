<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_front extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo "nama toko nya ".$this->uri->segment(1);
	}

	public function kategori(){
		$kategori = $this->uri->segment(3);
		echo 'kategori '.$kategori;
	}
}

/* End of file  */
/* Location: ./application/controllers/ */