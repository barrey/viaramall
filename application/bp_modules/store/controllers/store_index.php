<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->user = modules::load('user/user_index');
		$this->bp_data = modules::load('components/bp_data');
		$this->user->check_login();
		$this->load->model('store_routes_model');
	}

	public function index()
	{	
		$id = $this->session->userdata('user_id');
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('store/store_index',$data,'components/backend');
	}

	public function add(){
		$post = $this->input->post();
		$user_id = $this->session->userdata('user_id');
		if(!$post){
			$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);
			$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
			$data['province'] = $this->bp_data->get_province();
			$this->render('store/store_add',$data,'components/backend');
		}

		else{
			//proses pembuatan toko
			//save to db
			$this->load->model('store_model');
			$user_id = $this->session->userdata('user_id');
			$data = array('seller_id' => $user_id, 'created_date' => date('Y-m-d'), 'store_name' => $post['store_name'], 'description' => $post['store_deskripsi'], 'location' => $post['location']);
			$this->store_model->insert($data);
			$id = $this->db->insert_id();

			if($this->file_uploaded('store_logo')){
				$config['upload_path'] = 'assets/store/logo';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '100';
				$config['max_width'] = '1024';
				$config['max_height'] = '768';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('store_logo'))
				{
					echo $this->debug( $this->upload->display_errors());
					$upload = false;
				}
				else
				{
					$upload = $this->upload->data();

					//update to db
					$where = $this->store_model->set_param(array('id' => array('where' => $id)));
					$data2 = array('img' => $upload['file_name']);
					$this->store_model->update($data2);
				}
			}

			//jika sudah sukses save to db
			if($this->db->affected_rows() > 0){
				//save to app_routes
				$store_route = array(
					'url_store' => $post['store_domain'],
					'store_id' => $id,
					'date_created' => date('Y-m-d')
				);

				$this->load->model('store_routes_model');
				$this->store_routes_model->insert($store_route);

				$this->store_add_success();
			}
			else{
				$this->store_add_failed();
			}
			
		}
	}

	public function store_add_success(){
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['head'] = '<link rel="stylesheet" href="'.base_url().'assets/css/viaramall/backend/etalase.css">';
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('store/store_success',$data,'components/backend');
	}

	public function store_add_failed(){
		$this->render('store/store_failed',$data,'components/backend');
	}

	public function domain_check(){
		$domain = $this->input->post('domain');
		echo $domain;

		$where = array('url_store' => array('where' => $domain), 'status' => array('where' => 'y'));
		$data = $this->store_routes_model->check('insert');

		if($data){
			echo "belum";
		}
		else{
			echo "ada";
		}
	}

	public function sales(){
		$this->load->model('transaksi_model');
		$id = $this->session->userdata('user_id');
		$data_sidebar = array();
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('store/store_sales',$data,'components/backend');
	}

	public function tes(){
		$id = $this->session->userdata('user_id');
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['head'] = '<link rel="stylesheet" href="'.base_url().'assets/css/viaramall/backend/etalase.css">';
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('store/store_success',$data,'components/backend');
	}
}

/* End of file store_index.php */
/* Location: ./application/controllers/store_index.php */