<form method="POST" action="<?php echo base_url() ?>store/store_index/add" role="form" enctype="multipart/form-data">
<section>
<div class="container m-bottom-40">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
         <div class="well">
              <div class="input-group">
                 <span style="font-size: 1.7em;">http://www.viaramall.com/</span>
                 <input class="btn btn-lg" name="store_domain" type="text" placeholder="Toko kamu" required="required">
                 <input type="hidden" name="store_domain_confirmed">
                 <a class="btn btn-info btn-lg p-left-40" id="check_domain">Cek ketersediaan</a>
              </div>
         </div>
        </div>
    </div>
</div>
</section>
<div class="item contact">
    	    
    		<div class="container">
    		     
        			<div class="col-md-4 col-md-offset-2">
        			    <div class="row">
                                <div class="col-md-12 narrow">
                                        <div class="col-md-6 col-md-offset-2">
                                        <h4>Logo Toko Anda</h4><br>
                                            <div>
                                                <img alt="" class="img-rounded img-responsive center" src="<?php echo base_url()?>assets/store/logo/store.jpg" style="width: 240px;">
                                            </div><br>
                                        </div> 
                                        <div class="col-md-12">
                                            <input type="file" class="btn btn-primary" name="store_logo">
                                        </div>
                                </div>
                        </div>
                        <div class="m-top-40">
            				<h4>Berjualan di viaramall</h4>
            				
            				<p>
            					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
            				</p>
            					
            				<p class="small">
            					Blabalaklab lab sdasdasdsd <a href="http://chillyorange.com">ssad a sfa f</a>.
            				</p>
        			    </div>
        			</div><!-- ./col-md-4 -->
        			
        			<div class="col-md-6">
        			    <div class="row">
                            <div class="col-md-12">
                				<h4>Informasi Toko anda</h4>
                			
                				
                				    <div class="row m-top-20">
                    					<div class="form-group">
                                            <label class="col-lg-3 col-md-3 control-label">Nama Toko anda</label>
                                            <div class="col-lg-9 col-md-9">
                    				    	<input type="text" name="store_name" id="name" class="form-control input-lg" required="required">
                                            </div>
                    				  	</div>
                                    </div>
                				  	 
                                    <div class="row m-top-20">	  	
                    				  	<div class="form-group">
                                            <label class="col-lg-3 col-md-3 control-label">Slogan Toko anda</label>
                                            <div class="col-lg-9 col-md-9">
                    				  		<input type="text" name="store_slogan" id="slogan" class="form-control input-lg" required="required">
                                            </div>
                    				  	</div>
                                    </div>
                				  	
                                    <div class="row m-top-20">
                    				  	<div class="form-group">
                                            <label class="col-lg-3 col-md-3 control-label">Deskripsi Toko anda</label>
                                            <div class="col-lg-9 col-md-9">
                    				  		<textarea rows="4" class="form-control" required="required" name="store_deskripsi"></textarea>
                                            </div>
                    				  	</div>
                				    </div>

                                    <div class="row m-top-20">
                                        <div class="form-group">
                                            <label class="col-lg-3 col-md-3 control-label">Lokasi pengiriman</label>
                                            <div class="col-lg-9 col-md-9">
                                            <select name="location" id="location" required="required">
                                                <option value="">-- Pilih lokasi--</option>
                                                <?php foreach($province as $p):?>
                                                    <option value="<?php echo $p->id; ?>"><?php echo $p->name; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                				
                            </div>
                        </div>
        			
        			</div><!-- /.col-md-4 -->
                    <div class="clearfix"></div>
                    <div class="col-md-2 col-md-offset-10 m-top-40 m-bottom-40">
                        <button class="btn btn-primary btn-embossed btn-lg btn-wide" type="submit" id="save-toko"><span class="fui-mail"></span> Simpan Toko Anda</button>
                    </div>
    		</div><!-- /.container -->
    		    	
    	</div>
</form>


        <script>
        $(document).ready(function(){
            $('#location').select2();
            $('#check_domain').on('click', function(e){
                domain = $(this).prev().prev().val();
                selector = '#check_domain';
                check_domain(domain, selector);
                
            });
    
            $('#save-toko').on('click', function(e){
                e.preventDefault();
                console.log($('input[name="store_domain_confirmed"]').val());
                if($('input[name="store_domain_confirmed"]').val() == ''){
                    alert('Klik cek ketersediaan domain toko terlebih dulu');
                    return false;
                }
                else{
                    $('form').submit();
                }
                
            });

            function check_domain(domain, selector){
                $this = $(selector);
                console.log(selector+' - '+domain);
                $this.attr('disabled','disabled');
                $.ajax({
                    url: '<?php echo base_url()?>store/store_index/domain_check',
                    type: 'POST',
                    dataType: 'html',
                    data: {name: domain},
                })
                .done(function(response) {
                    console.log("success");
                    if(response == 'belum'){
                        $.notify('Domain tersedia','info');
                        $this.prev().val(domain);
                        console.log($this.prev().attr('name'));
                    }else{
                        $.notify('Domain tidak tersedia');
                    }

                    $this.removeAttr('disabled');
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

                return true;
            }
        });
        </script>