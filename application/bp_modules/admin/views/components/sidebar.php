<aside>
		      <div id="sidebar"  class="nav-collapse ">
		          <!-- sidebar menu start-->
		          <ul class="sidebar-menu" id="nav-accordion">
		          
		          	  <p class="centered"><a href="profile.html"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
		          	  <h5 class="centered"><?php echo ucfirst($this->session->userdata('name')); ?></h5>
		          	  	
		              <li class="mt">
		                  <a class="active" href="<?php echo base_url()?>store/store_index/add">
		                      <i class="fa fa-dashboard"></i>
		                      <span>Daftar Toko</span>
		                  </a>
		              </li>

		              <li class="sub-menu">
		                  <a href="index.html" >
		                      <i class="fa fa-desktop"></i>
		                      <span>Diskusi Produk</span>
		                  </a>
		              </li>

		              <li class="sub-menu">
		                  <a href="index.html" >
		                      <i class="fa fa-cogs"></i>
		                      <span>Review</span>
		                  </a>
		              </li>
		              <li class="sub-menu">
		                  <a href="javascript:;" >
		                      <i class="fa fa-book"></i>
		                      <span>Notifikasi Nego</span>
		                  </a>
		              </li>
		              <li class="sub-menu">
		                  <a href="javascript:;" >
		                      <i class="fa fa-tasks"></i>
		                      <span>Pembelian</span>
		                  </a>
		              </li>
		              <li class="sub-menu">
		                  <a href="javascript:;" >
		                      <i class="fa fa-th"></i>
		                      <span>Pengaturan</span>
		                  </a>
		              </li>
		              <li class="sub-menu">
		                  <a href="<?php echo base_url()?>components/bp_auth/logout" >
		                      <i class=" fa fa-bar-chart-o"></i>
		                      <span>Keluar</span>
		                  </a>
		              </li>

		          </ul>
		          <!-- sidebar menu end-->
		      </div>
		</aside>