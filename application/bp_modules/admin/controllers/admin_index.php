<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//check admin login
		$this->check_login_admin();
	}

	public function index()
	{
		$data = array();
		$data['sidebar'] = $this->load->view('admin/components/sidebar','',TRUE);
		$this->render('admin/admin_index',$data,'components/backend');
	}

	public function check_login_admin(){
		$data = $this->session->all_userdata();
		if(!empty($data['is_login']) && !empty($data['admin_id'])){

		}
		else{
			redirect(base_url().'auth/login/admin');
		}
	}
}

/* End of file backend_admin_index.php */
/* Location: ./application/controllers/backend_admin_index.php */