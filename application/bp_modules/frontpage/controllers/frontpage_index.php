<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontpage_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('store_produk_model');

		$data = array();
		$footer_data = array();
		$header_data['user'] = $this->session->userdata('name');

		$data['product_list'] = $this->store_produk_model->product_list();

		$data['header'] = $this->load->view('frontpage/components/header', $header_data, TRUE);
		$data['footer'] = $this->load->view('frontpage/components/footer', $footer_data, TRUE);
		$this->render('frontpage/index2',$data,'components/frontend2');
	}	

	public function produk($product)
	{
		$data = array();//array('guide/index');
		$tes = array();
		$data['header'] = $this->load->view('components/frontend/header', $tes, TRUE);
		$data['footer'] = '<h1>FOOTER</h1>';
		$this->render('frontpage/category',$data,'components/frontend');
	}

	public function pencarian()
	{

	}

	public function akun()
	{

	}

	public function keranjang()
	{
		$data = array();
		$this->load->view('products/product_view', $data);
	}

	public function product_view(){
		$data = array();//array('guide/index');
		$footer_data = array();
		$header_data['user'] = $this->session->userdata('name');
		$content_data	 = array();

		$data['header'] = $this->load->view('frontpage/components/header', $header_data, TRUE);
		$data['footer'] = $this->load->view('frontpage/components/footer', $footer_data, TRUE);
		$this->render('frontpage/produk_detail',$data,'components/frontend');
	}


}

/* End of file  */
/* Location: ./application/controllers/ */