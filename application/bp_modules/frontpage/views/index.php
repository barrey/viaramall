<div class="page_wrapper">
	<div class="container">											
		<section data-animation="fadeInDown" class="section_offset animated visible fadeInDown">
			<h3 class="offset_title">Produk Hari ini</h3>
			<div class="tabs type_3 products initialized">
				<div class="tab_containers_wrap">
					<?php foreach($product_list as $produk):?>
					<div class="product_item col-md-3" style="height: 403px;">
						<div class="image_wrap">

							<img alt="" src="<?php echo base_url();?>assets/store/product/<?php echo $produk->seller_id; ?>/<?php echo $produk->product_img; ?>">

							<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

							<div class="actions_wrap">

								<div class="centered_buttons">

									<a class="button_dark_grey middle_btn quick_view pb" href="<?php echo base_url();?>products/product_index/quick_view/<?php echo $produk->id;?>">Quick View</a>
									
								</div><!--/ .centered_buttons -->

							</div><!--/ .actions_wrap-->
							
							<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->
						</div>
						<!--<div class="label_bestseller">Bestseller</div>-->
						<div class="description align_center">
							<p><a href="#"><b><?php echo ucfirst($produk->product_name); ?></b></a></p>
							<div class="clearfix product_info">
								<p class="product_price alignleft"><b>Rp. <?php echo number_format( $produk->product_harga , 0 , ',' , '.' );?></b></p>
							</div>
							<button class="button_blue middle_btn">Add to Cart</button>
							<!--<ul class="bottombar">
								<li><a href="#">Add to Wishlist</a></li>
								<li><a href="#">Add to Compare</a></li>
							</ul>-->
						</div>
					</div>
					<?php endforeach;?>
				</div>
			</div>
		</section>
	</div>
</div>


<!-- Modal HTML -->
    <div class="simple-modal" id="modal_product">
		  <div class="modal-dialog" style="overflow: hidden;">
		    <div class="modal-content">
		    </div>
		  </div>
	</div>
	<div style='display:none'>
		<img src='<?php echo base_url()?>assets/img/apps/x.png' alt='' />
	</div>