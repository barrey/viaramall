<div class="container">

	<p class="footer_message">This is custom text. Vestibulum sed ante. Donec sagittis euismod purus. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantim, totam rem aperiam, eaque ipse quae ab illo inventore veritatis.</p>

	<!-- - - - - - - - - - - - - - Payments - - - - - - - - - - - - - - - - -->

	<ul class="payments">

		<li><img src="images/payment_1.png" alt=""></li>
		<li><img src="images/payment_2.png" alt=""></li>
		<li><img src="images/payment_3.png" alt=""></li>
		<li><img src="images/payment_4.png" alt=""></li>
		<li><img src="images/payment_5.png" alt=""></li>
		<li><img src="images/payment_6.png" alt=""></li>
		<li><img src="images/payment_7.png" alt=""></li>
		<li><img src="images/payment_8.png" alt=""></li>

	</ul>
	
	<!-- - - - - - - - - - - - - - End of payments - - - - - - - - - - - - - - - - -->

	<!-- - - - - - - - - - - - - - Footer navigation - - - - - - - - - - - - - - - - -->

	<nav class="footer_nav">

		<ul class="bottombar">

			<li><a href="#">Medicine &amp; Health</a></li>
			<li><a href="#">Beauty</a></li>
			<li><a href="#">Personal Care</a></li>
			<li><a href="#">Vitamins &amp; Supplements</a></li>
			<li><a href="#">Baby Needs</a></li>
			<li><a href="#">Diet &amp; Fitness</a></li>
			<li><a href="#">Sexual Well-being</a></li>

		</ul>

	</nav>
	
	<!-- - - - - - - - - - - - - - End of footer navigation - - - - - - - - - - - - - - - - -->

	<p class="copyright">© 2015 <a href="index.html">Viaramall</a>. All Rights Reserved.</p>

</div>