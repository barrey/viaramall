<header id="header" class="type_5" style="padding-bottom: 0px;">

		<!-- - - - - - - - - - - - - - Bottom part - - - - - - - - - - - - - - - - -->

		<div class="bottom_part">

			<div class="container">

				<div class="row">

					<div class="main_header_row">

						<div class="col-sm-3">

							<!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->

							<a href="index.html" class="logo">

								<img src="images/logo.png" alt="">

							</a>

							<!-- - - - - - - - - - - - - - End of logo - - - - - - - - - - - - - - - - -->

						</div><!--/ [col]-->

						<div class="col-lg-6 col-md-5 col-sm-5">

							<!-- - - - - - - - - - - - - - Call to action - - - - - - - - - - - - - - - - -->

							<div class="call_us">

								<span>Call us toll free:</span> <b>+1888 234 5678</b>

							</div><!--/ .call_us-->

							<!-- - - - - - - - - - - - - - End call to action - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Search form - - - - - - - - - - - - - - - - -->

							<form class="clearfix search">

								<input type="text" name="" tabindex="1" placeholder="Search..." class="alignleft">
								
								<!-- - - - - - - - - - - - - - Categories - - - - - - - - - - - - - - - - -->

								<div class="search_category alignleft">

									<div class="open_categories">All Categories</div>

									<ul class="categories_list dropdown find">

										<li class="animated_item" style="transition-delay:0.1s"><a href="#">Medicine &amp; Health</a></li>
										<li class="animated_item" style="transition-delay:0.2s"><a href="#">Beauty</a></li>
										<li class="animated_item" style="transition-delay:0.3s"><a href="#">Personal Care</a></li>
										<li class="animated_item" style="transition-delay:0.4s"><a href="#">Vitamins &amp; Supplements</a></li>
										<li class="animated_item" style="transition-delay:0.5s"><a href="#">Baby Needs</a></li>
										<li class="animated_item" style="transition-delay:0.6s"><a href="#">Diet &amp; Fitness</a></li>
										<li class="animated_item" style="transition-delay:0.7s"><a href="#">Sexual Well-being</a></li>

									</ul>

								</div><!--/ .search_category.alignleft-->

								<!-- - - - - - - - - - - - - - End of categories - - - - - - - - - - - - - - - - -->

								<button class="button_blue def_icon_btn alignleft"></button>

							</form><!--/ #search-->
							
							<!-- - - - - - - - - - - - - - End search form - - - - - - - - - - - - - - - - -->

						</div><!--/ [col]-->

						<div class="col-lg-3 col-sm-4">

							<!-- - - - - - - - - - - - - - Loginbox & Wishlist & Compare - - - - - - - - - - - - - - - - -->

							<ul class="account_bar" style="margin-top: 40px;">

								<li>

									<div class="login_box"><div class="login_box_inner">
										<?php if(!empty($user) || $user != ""){
											echo $user;
											echo '. <a href="'.base_url().'auth/logout">Logout</a>';
										}else {?> 
											<a href="<?php echo base_url()?>auth/login">Login</a> or <a href="<?php echo base_url()?>auth/register">Register</a>
										<?php } ?>
									</div>
									</div>

								</li>

							</ul><!--/ .account_bar-->

							<!-- - - - - - - - - - - - - - End Loginbox & Wishlist & Compare - - - - - - - - - - - - - - - - -->

						</div><!--/ [col]-->

					</div><!--/ .main_header_row-->

				</div><!--/ .row-->

			</div><!--/ .container-->

		</div><!--/ .bottom_part -->

		<!-- - - - - - - - - - - - - - End of bottom part - - - - - - - - - - - - - - - - -->

		<!-- - - - - - - - - - - - - - Main navigation wrapper - - - - - - - - - - - - - - - - -->
		<div id="main_navigation_wrap" class="sticky_initialized">

			<div class="container">

				<div class="row">

					<div class="col-xs-12">

						<!-- - - - - - - - - - - - - - Sticky container - - - - - - - - - - - - - - - - -->

						<div class="sticky_inner type_2">

							<!-- - - - - - - - - - - - - - Navigation item - - - - - - - - - - - - - - - - -->

							<div class="nav_item size_3">

								<button class="open_categories_sticky">Categories</button>

								<!-- - - - - - - - - - - - - - Main navigation - - - - - - - - - - - - - - - - -->

								<ul class="theme_menu cats dropdown find">

									<li class="has_megamenu animated_item" style="transition-delay:0.1s">

										<a href="#">Medicine &amp; Health (1375)</a>

										<!-- - - - - - - - - - - - - - Mega menu - - - - - - - - - - - - - - - - -->

										<div class="mega_menu clearfix">

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">
											
												<ul class="list_of_links">

													<li><a href="#">Allergy &amp; Sinus</a></li>
													<li><a href="#">Children's Healthcare</a></li>
													<li><a href="#">Cough, Cold &amp; Flu</a></li>
													<li><a href="#">Diabetes Management</a></li>
													<li><a href="#">Digestion &amp; Nausea</a></li>
													<li><a href="#">Eye Care</a></li>
													<li><a href="#">First Aid</a></li>
													<li><a href="#">Foot Care</a></li>
													<li><a href="#">Health Clearance</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">

												<ul class="list_of_links">

													<li><a href="#">Home Health Care</a></li>
													<li><a href="#">Home Tests</a></li>
													<li><a href="#">Incontinence Aids</a></li>
													<li><a href="#">Natural &amp; Homeopathic</a></li>
													<li><a href="#">Pain &amp; Fever Relief</a></li>
													<li><a href="#">Skin Condition Treatments</a></li>
													<li><a href="#">Sleep &amp; Snoring aids</a></li>
													<li><a href="#">Stop Smoking Aids</a></li>
													<li><a href="#">Support &amp; Braces</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

										</div><!--/ .mega_menu-->

										<!-- - - - - - - - - - - - - - End of mega menu - - - - - - - - - - - - - - - - -->

									</li>
									<li class="has_megamenu animated_item" style="transition-delay:0.2s">

										<a href="#">Beauty (1687)</a>

										<!-- - - - - - - - - - - - - - Mega menu - - - - - - - - - - - - - - - - -->

										<div class="mega_menu type_2 clearfix">

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">
											
												<h6><b>By Category</b></h6>
											
												<ul class="list_of_links">

													<li><a href="#">Bath &amp; Spa</a></li>
													<li><a href="#">Beauty Clearance</a></li>
													<li><a href="#">Gift Sets</a></li>
													<li><a href="#">Hair Care</a></li>
													<li><a href="#">Makeup &amp; Accessories</a></li>
													<li><a href="#">Skin Care</a></li>
													<li><a href="#">Tools &amp; Accessories</a></li>
													<li><a href="#" class="all">View All Categories</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">

												<h6><b>By Brand</b></h6>
											
												<ul class="list_of_links">

													<li><a href="#">Abibas</a></li>
													<li><a href="#">Agedir</a></li>
													<li><a href="#">Aldan</a></li>
													<li><a href="#">Biomask</a></li>
													<li><a href="#">Gamman</a></li>
													<li><a href="#">Pallona</a></li>
													<li><a href="#">Pure Care</a></li>
													<li><a href="#" class="all">View All Brands</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">
												
												<a href="#">
													<img src="images/mega_menu_img_1.jpg" alt="">
												</a>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

										</div><!--/ .mega_menu-->

										<!-- - - - - - - - - - - - - - End of mega menu - - - - - - - - - - - - - - - - -->

									</li>
									<li class="has_megamenu animated_item" style="transition-delay:0.3s">

										<a href="#">Personal Care (1036)</a>

										<!-- - - - - - - - - - - - - - Mega menu - - - - - - - - - - - - - - - - -->

										<div class="mega_menu type_3 clearfix">

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">

												<ul class="list_of_links">

													<li><a href="#">Oral Care</a></li>
													<li><a href="#">Shaving &amp; Hair Removal</a></li>
													<li><a href="#">Men's</a></li>
													<li><a href="#">Sun Care</a></li>
													<li><a href="#">Clearance</a></li>
													<li><a href="#">Feminine Care</a></li>
													<li><a href="#">Gift Sets</a></li>
													<li><a href="#">Soaps &amp; Bodywash</a></li>
													<li><a href="#">Massage &amp; Relaxation</a></li>
													<li><a href="#">Foot Care</a></li>
													<li><a href="#" class="all">View All Categories</a></li>

												</ul>

											</div><!--/ .mega_menu_item -->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item products_in_mega_menu">

												<h6 class="widget_title"><b>Today's Deals</b></h6>

												<div class="row">

													<div class="col-sm-4">

														<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

														<div class="product_item">

															<!-- - - - - - - - - - - - - - Thumbnail - - - - - - - - - - - - - - - - -->

															<div class="image_wrap">

																<img src="images/product_img_11.jpg" alt="">

															</div><!--/. image_wrap-->

															<!-- - - - - - - - - - - - - - End thumbnail - - - - - - - - - - - - - - - - -->

															<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

															<div class="label_offer percentage">

																<div>30%</div>OFF

															</div>

															<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

															<!-- - - - - - - - - - - - - - Product description - - - - - - - - - - - - - - - - -->

															<div class="description">

																<p><a href="#">Tellus Dolor Dapibus Eget 24 fl oz</a></p>

																<div class="clearfix product_info">

																	<p class="product_price alignleft"><s>$9.99</s> <b>$5.99</b></p>

																</div><!--/ .clearfix.product_info-->

															</div>

															<!-- - - - - - - - - - - - - - End of product description - - - - - - - - - - - - - - - - -->

														</div><!--/ .product_item-->
														
														<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

													</div><!--/ [col]-->

													<div class="col-sm-4">

														<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

														<div class="product_item">

															<!-- - - - - - - - - - - - - - Thumbnail - - - - - - - - - - - - - - - - -->
															
															<div class="image_wrap">

																<img src="images/product_img_12.jpg" alt="">

															</div><!--/. image_wrap-->

															<!-- - - - - - - - - - - - - - End thumbnail - - - - - - - - - - - - - - - - -->

															<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

															<div class="label_offer percentage">

																<div>25%</div>OFF

															</div>

															<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

															<!-- - - - - - - - - - - - - - Product description - - - - - - - - - - - - - - - - -->

															<div class="description">

																<p><a href="#">Ipsum Dolor Sit Amet, Size 4 Diapers 29 ea</a></p>

																<div class="clearfix product_info">

																	<p class="product_price alignleft"><s>$16.99</s> <b>$14.99</b></p>

																</div><!--/ .clearfix.product_info-->

															</div>

															<!-- - - - - - - - - - - - - - End of product description - - - - - - - - - - - - - - - - -->

														</div><!--/ .product_item-->
														
														<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

													</div><!--/ [col]-->

													<div class="col-sm-4">

														<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

														<div class="product_item">

															<!-- - - - - - - - - - - - - - Thumbnail - - - - - - - - - - - - - - - - -->
															
															<div class="image_wrap">

																<img src="images/product_img_13.jpg" alt="">

															</div><!--/. image_wrap-->

															<!-- - - - - - - - - - - - - - End thumbnail - - - - - - - - - - - - - - - - -->

															<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

															<div class="label_offer percentage">

																<div>40%</div>OFF

															</div>

															<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

															<!-- - - - - - - - - - - - - - Product description - - - - - - - - - - - - - - - - -->

															<div class="description">

																<p><a href="#">Ut Tellus Dolor Dapbus Eget Dolor Ipsum...</a></p>

																<div class="clearfix product_info">

																	<p class="product_price alignleft"><s>$103.99</s> <b>$73.99</b></p>

																</div><!--/ .clearfix.product_info-->

															</div>

															<!-- - - - - - - - - - - - - - End of product description - - - - - - - - - - - - - - - - -->

														</div><!--/ .product_item-->
														
														<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

													</div><!--/ [col]-->
													
												</div><!--/ .row-->

												<hr>

												<a href="#" class="button_grey">View All Deals</a>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

										</div><!--/ .mega_menu-->

										<!-- - - - - - - - - - - - - - End of mega menu - - - - - - - - - - - - - - - - -->

									</li>
									<li class="has_megamenu animated_item" style="transition-delay:0.4s">

										<a href="#">Vitamins &amp; Supplements (202)</a>

										<!-- - - - - - - - - - - - - - Mega menu - - - - - - - - - - - - - - - - -->

										<div class="mega_menu type_4 clearfix">

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">

												<h6><b>By Condition</b></h6>
											
												<ul class="list_of_links">

													<li><a href="#">Aches &amp; Pains</a></li>
													<li><a href="#">Acne Solutions</a></li>
													<li><a href="#">Allergy &amp; Sinus</a></li>
													<li><a href="#" class="all">View All</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">

												<h6><b>Multivitamins</b></h6>

												<ul class="list_of_links">

													<li><a href="#">50+ Multivitamins</a></li>
													<li><a href="#">Children's Multivitamins</a></li>
													<li><a href="#">Men's Multivitamins</a></li>
													<li><a href="#" class="all">View All</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Mega menu item - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_item">

												<h6><b>Herbs</b></h6>

												<ul class="list_of_links">

													<li><a href="#">Aloe Vera</a></li>
													<li><a href="#">Ashwagandha</a></li>
													<li><a href="#">Astragalus</a></li>
													<li><a href="#" class="all">View All</a></li>

												</ul>

											</div><!--/ .mega_menu_item-->

											<!-- - - - - - - - - - - - - - End of mega menu item - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Banner - - - - - - - - - - - - - - - - -->

											<div class="mega_menu_banner">

												<a href="#">
													<img src="images/mega_menu_img_2.jpg" alt="">
												</a>

											</div><!--/ .mega_menu_banner-->

											<!-- - - - - - - - - - - - - - End of banner - - - - - - - - - - - - - - - - -->

										</div><!--/ .mega_menu-->

										<!-- - - - - - - - - - - - - - End of mega menu - - - - - - - - - - - - - - - - -->

									</li>
									<li class="has_megamenu animated_item" style="transition-delay:0.5s"><a href="#">Baby Needs (525)</a></li>
									<li class="has_megamenu animated_item" style="transition-delay:0.6s"><a href="#">Diet &amp; Fitness (135)</a></li>
									<li class="has_megamenu animated_item" style="transition-delay:0.7s"><a href="#">Sexuall Well-being (298)</a></li>
									<li class="has_megamenu animated_item" style="transition-delay:0.8s"><a href="#" class="all"><b>All Categories</b></a></li>

								</ul>

								<!-- - - - - - - - - - - - - - End of main navigation - - - - - - - - - - - - - - - - -->

							</div><!--/ .nav_item-->

							<!-- - - - - - - - - - - - - - End of navigation item - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Navigation item - - - - - - - - - - - - - - - - -->

							<div class="nav_item">

								<button class="toggle_menu"></button><nav class="main_navigation">

									<ul>

										<li><a href="index.html">Home</a></li>
										<li><a href="<?php echo base_url() ?>user/user_index">My Account</a></li>
										<li><a href="<?php echo base_url() ?>user/pembelian">Pembelian</a></li>
										<!--<li><a href="<?php echo base_url() ?>user/wishlist">Wishlist</a></li>-->
										<!--<li><a href="blog_v1.html">Blog</a></li>-->
										<!--<li><a href="additional_page_contact.html">Contact Us</a></li>-->
										<li class="has_submenu">
											<a href="index.html">Pages</a>
											<ul class="theme_menu submenu">
												<li class="has_submenu current">
													<a href="index.html">Homepage Layouts</a>
													<ul class="theme_menu submenu">
														<li><a href="index.html">Home 1</a></li>
														<li><a href="home_v2.html">Home 2</a></li>
														<li><a href="home_v3.html">Home 3</a></li>
														<li><a href="home_v4.html">Home 4</a></li>
														<li class="current"><a href="home_v5.html">Home 5</a></li>
														<li><a href="home_v6.html">Home 6</a></li>
													</ul>
												</li>
												<li class="has_submenu">
													<a href="category_page_v1.html">Category Page Layouts</a>
													<ul class="theme_menu submenu">
														<li><a href="category_page_v1.html">Category page 1</a></li>
														<li><a href="category_page_v2.html">Category page 2</a></li>
														<li><a href="category_page_v3.html">Category page 3</a></li>
														<li><a href="category_page_v4.html">Category page 4</a></li>
													</ul>
												</li>
												<li class="has_submenu">
													<a href="product_page_v1.html">Product Page Layouts</a>
													<ul class="theme_menu submenu">
														<li><a href="product_page_v2.html">Product page 2</a></li>
														<li><a href="product_page_v3.html">Product page 3</a></li>
													</ul>
												</li>
												<li class="has_submenu">
													<a href="shop_shopping_cart.html">Other Shop Pages</a>
													<ul class="theme_menu submenu">
														<li><a href="shop_shopping_cart.html">Shopping cart</a></li>
														<li><a href="shop_checkout.html">Checkout</a></li>
														<li><a href="shop_wishlist.html">Wishlist</a></li>
														<li><a href="shop_product_comparison.html">Product Comparison</a></li>
														<li><a href="shop_my_account.html">My Account</a></li>
														<li><a href="shop_manufacturers.html">Manufacturers</a></li>
														<li><a href="shop_manufacturer_page.html">Manufacturer Page</a></li>
														<li><a href="shop_orders_list.html">Order List</a></li>
														<li><a href="shop_order_page.html">Order Page</a></li>
													</ul>
												</li>
												<li class="has_submenu">
													<a href="additional_page_about.html">Additional Pages</a>
													<ul class="theme_menu submenu">
														<li><a href="additional_page_about.html">About Us</a></li>
														<li><a href="additional_page_contact.html">Contact Us</a></li>
														<li><a href="additional_page_faq.html">FAQ</a></li>
														<li><a href="additional_page_404.html">404 Page</a></li>
														<li><a href="additional_page_sitemap.html">Sitemap</a></li>
													</ul>
												</li>
												<li class="has_submenu">
													<a href="extra_stuff_elements.html">Extra Stuff</a>
													<ul class="theme_menu submenu">
														<li><a href="extra_stuff_elements.html">Elements</a></li>
														<li><a href="extra_stuff_typography.html">Typography</a></li>
														<li><a href="extra_stuff_columns.html">Columns</a></li>
													</ul>
												</li>
												<li class="has_submenu">
													<a href="blog_v1.html">Blog Pages</a>
													<ul class="theme_menu submenu">
														<li><a href="blog_v1.html">Blog v1</a></li>
														<li><a href="blog_v2.html">Blog v2</a></li>
														<li><a href="blog_v3.html">Blog v3</a></li>
														<li><a href="blog_post_v1.html">Blog Post v1</a></li>
														<li><a href="blog_post_v2.html">Blog Post v2</a></li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</nav>
							</div>
							<div class="nav_item size_3">
								<button id="open_shopping_cart" class="open_button" data-amount="">
									<b class="title">My Cart</b>
									<b class="total_price"><span style="font-size: 12px;"></span></b>
								</button>
								<div class="shopping_cart dropdown">
										<div class="animated_item" style="transition-delay:0.1s">
											<p class="title">Recently added item(s)</p>
											<div class="clearfix sc_product">
												<a href="#" class="product_thumb"><img src="images/sc_img_1.jpg" alt=""></a>
												<a href="#" class="product_name">Aenean Auctor Wisi Et Urna Ipsum...</a>
												<p>1 x $499.00</p>
												<button class="close"></button>
											</div>
										</div>
										<div class="animated_item" style="transition-delay:0.2s">
											<div class="clearfix sc_product">
												<a href="#" class="product_thumb"><img src="images/sc_img_2.jpg" alt=""></a>
												<a href="#" class="product_name">Lorem Ipsum Dolor Sit Amet...</a>
												<p>1 x $499.00</p>
												<button class="close"></button>
											</div>
										</div>
										<div class="animated_item" style="transition-delay:0.3s">
											<div class="clearfix sc_product">
												<a href="#" class="product_thumb"><img src="images/sc_img_3.jpg" alt=""></a>
												<a href="#" class="product_name">Nemo Enim Ipsam <br>Voluptatem 30 ea</a>
												<p>1 x $499.00</p>
												<button class="close"></button>
											</div>
										</div>
										<div class="animated_item" style="transition-delay:0.4s">
											<ul class="total_info">
												<li><span class="price">Tax:</span> $0.00</li>
												<li><span class="price">Discount:</span> $37.00</li>
												<li class="total"><b><span class="price">Total:</span> $999.00</b></li>
											</ul>
										</div>
										<div class="animated_item" style="transition-delay:0.5s">
											<a href="#" class="button_grey">View Cart</a>
											<a href="#" class="button_blue">Checkout</a>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</header>