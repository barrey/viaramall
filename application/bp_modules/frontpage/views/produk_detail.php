<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/viaramall/fancybox/source/jquery.fancybox.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/viaramall/fancybox/source/helpers/jquery.fancybox-thumbs.css">

<div class="secondary_page_wrapper">

				<div class="container">

					<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
						<!--
						<ul class="breadcrumbs">

							<li><a href="index.html">Home <?php echo $id_store_product; ?></a></li>
							<li><a href="#">Beauty</a></li>
							<li><a href="#">Skin Care</a></li>
							<li><a href="#">Cleansers</a></li>
							<li><a href="#">Liquid</a></li>
							<li>Metus nulla facilisi, Original 24 fl oz (709 ml)</li>

						</ul>
						-->
					<!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->

					<div class="row">

						<main class="col-md-9 col-sm-8">

							<!-- - - - - - - - - - - - - - Product images & description - - - - - - - - - - - - - - - - -->

							<section class="section_offset">

								<div class="clearfix">

									<!-- - - - - - - - - - - - - - Product image column - - - - - - - - - - - - - - - - -->

									<div class="single_product">

										<!-- - - - - - - - - - - - - - Image preview container - - - - - - - - - - - - - - - - -->

										<div class="image_preview_container">
											<?php if(!empty($produk[0]->product_img)):?>
												<img id="img_zoom" data-zoom-image="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $produk[0]->product_img; ?>" src="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $produk[0]->product_img; ?>" alt="">
											<?php endif; ?>
											<!--<button class="button_grey_2 icon_btn middle_btn open_qv"><i class="icon-resize-full-6"></i></button>-->

										</div><!--/ .image_preview_container-->

										<!-- - - - - - - - - - - - - - End of image preview container - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Prodcut thumbs carousel - - - - - - - - - - - - - - - - -->
										
										<div class="product_preview">

											<div class="owl_carousel" id="thumbnails">
												<?php if(!empty($produk[0]->product_img)):?>
													<?php foreach($produk as $p):?>
														<?php if($p->product_img != ''):?>
															<a href="#" data-image="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $p->product_img; ?>" data-zoom-image="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $p->product_img; ?>">
																<img src="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $p->product_img; ?>" data-large-image="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $p->product_img; ?>" alt="">
															</a>
														<?php endif; ?>
													<?php endforeach; ?>
												<?php endif; ?>
											</div><!--/ .owl-carousel-->

										</div><!--/ .product_preview-->
										
										<!-- - - - - - - - - - - - - - End of prodcut thumbs carousel - - - - - - - - - - - - - - - - -->

									</div>

									<!-- - - - - - - - - - - - - - End of product image column - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product description column - - - - - - - - - - - - - - - - -->

									<div class="single_product_description">

										<h3 class="offset_title"><?php echo ucfirst($produk[0]->name); ?></h3>

										<!-- - - - - - - - - - - - - - Page navigation - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - End of page navigation - - - - - - - - - - - - - - - - -->

										<div class="description_section v_centered">

											<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->
										
											<ul class="rating">

												<li class="active"></li>
												<li class="active"></li>
												<li class="active"></li>
												<li></li>
												<li></li>

											</ul>
												
											<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

											<!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

											<ul class="topbar">

												<li><a href="#">3 Review(s)</a></li>
												<li><a href="#">Add Your Review</a></li>

											</ul>

											<!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

										</div>

										<div class="description_section">

											<table class="product_info">

												<tbody>

													<tr>

														<td>Toko </td>
														<td><a href="#"><?php echo ucfirst($toko->store_name);?></a></td>

													</tr>

													<tr>

														<td>Dilihat </td>
														<td><?php if($produk[0]->visitor_view != ''){echo $produk[0]->visitor_view;}?></td>
														<td style="margin-left: 5em; display: inline-block; width: 9em;">Berat</td>
														<td><span class="in_stock"><?php echo $produk[0]->berat.' '.$produk[0]->berat_tipe; ?></td>
													</tr>
													<tr>
														<td>Terjual</td>
														<td><?php echo $produk[0]->terjual; ?></td>
														<td style="margin-left: 5em; display: inline-block;">Asuransi</td>
														<td><span class="in_stock"><?php if($produk[0]->asuransi == 'y'){echo "Ya";}else{echo "Tidak";} ?></td>
													</tr>
													<tr>
														<td>Kondisi</td>
														<td><span class="in_stock"><?php echo $produk[0]->kondisi; ?></td>
														<td style="margin-left: 5em; display: inline-block;">Pemesanan min</td>
														<td><span class="in_stock"><?php echo $produk[0]->minimum_order; ?></td>
													</tr>

												</tbody>

											</table>

										</div>

										<hr>

										<p class="product_price">Rp. <?php echo number_format($produk[0]->harga, 0, ',','.'); ?></p>
										<input type="hidden" name="harga_satuan" type="hidden" value="<?php echo $produk[0]->harga; ?>" id="harga_satuan">
										<!-- - - - - - - - - - - - - - Product size - - - - - - - - - - - - - - - - -->

										<!--<div class="description_section_2 v_centered">
											
											<span class="title">Size:</span>

											<div class="custom_select min">

												<select>

													<option value="Small">Small</option>
													<option value="Middle">Middle</option>
													<option value="Big">Big</option>

												</select>

											</div>

										</div>-->

										<!-- - - - - - - - - - - - - - End of product size - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Quantity - - - - - - - - - - - - - - - - -->

										<div class="description_section_2 v_centered">
											
											<span class="title">Jumlah pembelian :</span>

											<div class="qty min clearfix">

												<button class="theme_button calculate" data-direction="minus">&#45;</button>
												<input type="text" name="jumlah" value="1" id="jumlah">
												<button class="theme_button calculate" data-direction="plus">&#43;</button>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of quantity - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

										<div class="buttons_row">

											<button data-modal-url="http://localhost:1234/viaramall/products/product_index/beli/35" class="button_blue middle_btn">Beli</button>
											<button data-modal-url="http://localhost:1234/viaramall/products/product_index/nego/35" class="button_dark_grey middle_btn" style="margin-left: 20px;">Nego</button>
											
											<!--
											<button class="button_dark_grey def_icon_btn middle_btn add_to_wishlist tooltip_container"><span class="tooltip top">Add to Wishlist</span></button>

											<button class="button_dark_grey def_icon_btn middle_btn add_to_compare tooltip_container"><span class="tooltip top">Add to Compare</span></button>
											-->

										</div>

										<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

									</div>

									<!-- - - - - - - - - - - - - - End of product description column - - - - - - - - - - - - - - - - -->

								</div>

							</section><!--/ .section_offset -->

							<!-- - - - - - - - - - - - - - End of product images & description - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->

							<div class="section_offset">

								<div class="tabs type_2">

									<!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->

									<ul class="tabs_nav clearfix">

										<li><a href="#tab-1">Description</a></li>
										<li><a href="#tab-2">Diskusi</a></li>
										<li><a href="#tab-3">Review (3)</a></li>
										<!--<li><a href="#tab-4">Custom Tab</a></li>-->

									</ul>
									
									<!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->

									<div class="tab_containers_wrap">

										<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

										<div id="tab-1" class="tab_container">

											<p><?php echo ucfirst($produk[0]->deskripsi); ?></p>

										</div><!--/ #tab-1-->

										<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

										<div id="tab-2" class="tab_container">

											<ul class="specifications">
												<?php if(!empty($diskusi)):?>
													<?php foreach($diskusi as $d): ?>
														<li>
															<span><?php echo $d->nama; ?></span>
															<p><?php echo $d->diskusi; ?></p>
															<span><?php echo $d->created_date; ?></span>
														</li>
													<?php endforeach;?>
												<?php endif;?>
											</ul>

										</div><!--/ #tab-2-->

										<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

										<div id="tab-3" class="tab_container">

											<section class="section_offset">

												<ul class="reviews">
													<article class="review">

														<!-- - - - - - - - - - - - - - Rates - - - - - - - - - - - - - - - - -->

														

														<!-- - - - - - - - - - - - - - End of rates - - - - - - - - - - - - - - - - -->

														<!-- - - - - - - - - - - - - - Review body - - - - - - - - - - - - - - - - -->

														<div class="review-body">

															<div class="review-meta">
																
																<h5 class="bold">Good Quality</h5>

																Review oleh <a class="bold" href="#">Ivana Wrong</a> on 12/4/2014

															</div>

															<p>Aliquam Erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo.</p>

														</div>

														<!-- - - - - - - - - - - - - - End of review body - - - - - - - - - - - - - - - - -->
													</article>
												</ul>
												
												<!-- pagination bootstrap -->
												<a href="#" class="button_grey middle_btn">1</a>
												<a href="#" class="button_grey middle_btn">2</a>
												<a href="#" class="button_grey middle_btn">3</a>
												<!-- end of pagination bootstrap -->

											</section><!--/ .section_offset -->

											<section class="section_offset">

												<h3>Berikan review anda</h3>

												<div class="row">

													<div class="col-lg-12">

														<!-- - - - - - - - - - - - - - Rate the - - - - - - - - - - - - - - - - -->

														<div class="table_wrap rate_table">

															<table>

																<thead>

																	<tr>
																		<th>1 Star</th>
																		<th>2 Stars</th>
																		<th>3 Stars</th>
																		<th>4 Stars</th>
																		<th>5 Stars</th>
																	</tr>

																</thead>

																<tbody>

																	<tr>
																		
																		<td>
																				
																			<input checked type="radio" name="price_rate" id="rate_1">
																			<label for="rate_1"></label>

																		</td>

																		<td>
																				
																			<input type="radio" name="price_rate" id="rate_2">
																			<label for="rate_2"></label>

																		</td>

																		<td>
																				
																			<input type="radio" name="price_rate" id="rate_3">
																			<label for="rate_3"></label>

																		</td>

																		<td>
																				
																			<input type="radio" name="price_rate" id="rate_4">
																			<label for="rate_4"></label>

																		</td>

																		<td>
																				
																			<input type="radio" name="price_rate" id="rate_5">
																			<label for="rate_5"></label>

																		</td>

																	</tr>

																</tbody>

															</table>

														</div>

														<!-- - - - - - - - - - - - - - End of rate the - - - - - - - - - - - - - - - - -->

													</div><!--/ [col]-->

													<div class="col-lg-12">

														<!-- - - - - - - - - - - - - - Review form - - - - - - - - - - - - - - - - -->

														<form class="type_2">

															<ul>

																<li class="row">

																	<div class="col-sm-12">
																		
																		<label for="summary">Judul</label>
																		<input type="text" name="" id="summary">

																	</div>

																</li>

																<li class="row">

																	<div class="col-xs-12">

																		<label for="review_message">Review</label>

																		<textarea rows="5" id="review_message"></textarea>

																	</div>

																</li>

																<li class="row">

																	<div class="col-xs-12">
																	
																		<button class="button_dark_grey middle_btn">Simpan Review</button>

																	</div>

																</li>

															</ul>

														</form>

														<!-- - - - - - - - - - - - - - End of review form - - - - - - - - - - - - - - - - -->

													</div>

												</div><!--/ .row -->

											</section><!--/ .section_offset -->

										</div><!--/ #tab-3-->

										<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

										<!--<div id="tab-4" class="tab_container">


											<div class="video_wrap">

												<iframe src="http://www.youtube.com/embed/-BrDlrytgm8?controls=1&amp;autohide=0&amp;wmode=transparent"></iframe>

											</div>


										</div>-->

										<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

									</div><!--/ .tab_containers_wrap -->

									<!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->

								</div><!--/ .tabs-->

							</div><!--/ .section_offset -->

							<!-- - - - - - - - - - - - - - End of tabs - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Related products - - - - - - - - - - - - - - - - -->

							<section class="section_offset">

								<h3 class="offset_title">Related Products</h3>

								<div class="owl_carousel related_products">

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">
											<img src="images/product_img_30.jpg" alt="">
											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Beli</a>
													<a href="#" class="button_blue add_to_cart" style="margin-left: 20px; display: inline-block;">Nego</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_new">New</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Leo vel metus nulla facilisi etiam cursus 750mg...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$44.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_31.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Ut pharetra augue nec augue, 200 ea</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$4.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_32.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_bestseller">Bestseller</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Mauris fermentum dictum magna sed laoreet ...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$17.99</b></p>

												<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

												<ul class="rating alignright">

													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li></li>
													<li></li>

												</ul>
												
												<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_33.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Vestibulum libero nisl porta vel scelerisque eget...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$12.59</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_30.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_new">New</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Leo vel metus nulla facilisi etiam cursus 750mg...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$44.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_31.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Ut pharetra augue nec augue, 200 ea</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$4.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_32.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_bestseller">Bestseller</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Mauris fermentum dictum magna sed laoreet ...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$17.99</b></p>

												<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

												<ul class="rating alignright">

													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li></li>
													<li></li>

												</ul>
												
												<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_33.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Vestibulum libero nisl porta vel scelerisque eget...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$12.59</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

								</div><!--/ .owl_carousel -->

							</section><!--/ .section_offset -->

							<!-- - - - - - - - - - - - - - End of related products - - - - - - - - - - - - - - - - -->

							<section class="section_offset">

								<h3 class="offset_title">Other Products From This Seller</h3>

								<div class="owl_carousel other_products">

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_6.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue middle_btn add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey middle_btn def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey middle_btn def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Enzymatic Therapy CoQ10, 100mg, Softgels 120 ea</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$75.39</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_14.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue middle_btn add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey middle_btn def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey middle_btn def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Nisl porta vel scelerisque eget libero, Vcaps 60 ea</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$79.99</b></p>

												<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

												<ul class="rating alignright">

													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li></li>

												</ul>
												
												<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/product_img_15.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue middle_btn add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey middle_btn def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey middle_btn def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_hot">Hot</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Lorem ipsum dolor sit amet consectetuer adipis mauris 12 ea</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$24.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/tabs_img_1.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue middle_btn add_to_cart">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn middle_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn middle_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .actions_wrap-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_new">New</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Leo vel metus nulla facilisi etiam cursus 750mg, Softgels 120 ea</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$44.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/tabs_img_2.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey middle_btn quick_view pb" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue middle_btn add_to_cart pb">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn middle_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn middle_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .centered_btns-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Vestibulum libero nisl, porta vel 30</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$44.99</b></p>

												<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

												<ul class="rating alignright">

													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li></li>

												</ul>
												
												<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<div class="product_item">

										<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

										<div class="image_wrap">

											<img src="images/tabs_img_3.jpg" alt="">

											<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

											<div class="actions_wrap">

												<div class="centered_buttons">

													<a href="#" class="button_dark_grey middle_btn quick_view pb" data-modal-url="modals/quick_view.html">Quick View</a>

													<a href="#" class="button_blue middle_btn add_to_cart pb">Add to Cart</a>

												</div><!--/ .centered_buttons -->

												<a href="#" class="button_dark_grey def_icon_btn middle_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

												<a href="#" class="button_dark_grey def_icon_btn middle_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

											</div><!--/ .centered_btns-->
											
											<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

										</div><!--/. image_wrap-->

										<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

										<div class="label_hot">Hot</div>

										<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

										<div class="description">

											<a href="#">Amet consectetuer adipis mauris lorem ipsum dolor sit  fl oz (75ml)</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$44.99</b></p>

											</div>

										</div>

										<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

									</div><!--/ .product_item-->
									
									<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

								</div><!--/ .owl_carousel -->

							</section><!--/ .section_offset -->

						</main><!--/ [col]-->

						<aside class="col-md-3 col-sm-4">

							<!-- - - - - - - - - - - - - - Seller Information - - - - - - - - - - - - - - - - -->

							<section class="section_offset">

								<h3>Seller Information</h3>

								<div class="theme_box">

									<div class="seller_info clearfix">

										<a href="#" class="alignleft photo">

											<img src="images/seller_photo_1.jpg" alt="">

										</a>

										<div class="wrapper">

											<a href="#"><b>John Smith</b></a>

											<p class="seller_category">Member since Mar 2013</p>

										</div>

									</div><!--/ .seller_info-->

									<ul class="seller_stats">

										<li>
											
											<ul class="topbar">
												
												<li>China (Mainland)</li>

												<li><a href="#">Contact Details</a></li>

											</ul>

										</li>

										<li><span class="bold">99.8%</span> Positive Feedback</li>

										<li><span class="bold">7606</span> Transactions</li>

									</ul>

									<div class="v_centered">

										<a href="#" class="button_blue mini_btn">Contact Seller</a>

										<a href="#" class="small_link">Chat Now</a>

									</div>

								</div><!--/ .theme_box -->

								<footer class="bottom_box">
									
									<a href="#" class="button_grey middle_btn">View This Seller's Products</a>

								</footer>

							</section>

							<!-- - - - - - - - - - - - - - End of seller information - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

							<div class="section_offset">

								<section class="infoblock type_2">

									<i class="icon-money"></i>

									<h4 class="caption"><b>100% Money Back Guaranteed</b></h4>

									<p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna.</p>

									<a href="#" class="button_dark_grey middle_btn">Read More</a>

								</section><!--/ .infoblock.type_2-->

							</div>

							<!-- - - - - - - - - - - - - - End infoblock - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - You might also like - - - - - - - - - - - - - - - - -->

							<section class="section_offset">

								<h3 class="offset_title">You Might Also Like</h3>

								<div class="owl_carousel widgets_carousel">

									<ul class="products_list_widget">

										<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

										<li>
											
											<a href="#" class="product_thumb">
												
												<img src="images/product_thumb_4.jpg" alt="">

											</a>

											<div class="wrapper">

												<a href="#" class="product_title">Adipiscing aliquet sed in lacus...</a>

												<div class="clearfix product_info">

													<p class="product_price alignleft"><b>$5.99</b></p>

													<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

													<ul class="rating alignright">

														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li></li>

													</ul>
													
													<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

												</div>

											</div>

										</li>

										<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

										<li>
											
											<a href="#" class="product_thumb">
												
												<img src="images/product_thumb_5.jpg" alt="">

											</a>

											<div class="wrapper">

												<a href="#" class="product_title">Adipis mauris lorem ipsum dolor...</a>

												<div class="clearfix product_info">

													<p class="product_price alignleft"><b>$8.99</b></p>

													<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

													<ul class="rating alignright">

														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>

													</ul>
													
													<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

												</div>

											</div>

										</li>

										<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

										<li>
											
											<a href="#" class="product_thumb">
												
												<img src="images/product_thumb_6.jpg" alt="">

											</a>

											<div class="wrapper">

												<a href="#" class="product_title">Donec porta diam eu massa quisque...</a>

												<div class="clearfix product_info">

													<p class="product_price alignleft"><b>$76.99</b></p>

													<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

													<ul class="rating alignright">

														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>

													</ul>
													
													<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

												</div>

											</div>

										</li>

										<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

									</ul><!--/ .list_of_products-->

									<ul class="products_list_widget">

										<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

										<li>
											
											<a href="#" class="product_thumb">
												
												<img src="images/product_thumb_7.jpg" alt="">

											</a>

											<div class="wrapper">

												<a href="#" class="product_title">Diam eu massa quisque donec...</a>

												<div class="clearfix product_info">

													<p class="product_price alignleft"><b>$5.99</b></p>

													<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

													<ul class="rating alignright">

														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li></li>

													</ul>
													
													<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

												</div>

											</div>

										</li>

										<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

										<li>
											
											<a href="#" class="product_thumb">
												
												<img src="images/product_thumb_8.jpg" alt="">

											</a>

											<div class="wrapper">

												<a href="#" class="product_title">Ut pharetra augue nec augue...</a>

												<div class="clearfix product_info">

													<p class="product_price alignleft"><b>$8.99</b></p>

													<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

													<ul class="rating alignright">

														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>

													</ul>
													
													<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

												</div>

											</div>

										</li>

										<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

										<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

										<li>
											
											<a href="#" class="product_thumb">
												
												<img src="images/product_thumb_9.jpg" alt="">

											</a>

											<div class="wrapper">

												<a href="#" class="product_title">Donec porta diam eu massa...</a>

												<div class="clearfix product_info">

													<p class="product_price alignleft"><b>$76.99</b></p>

													<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

													<ul class="rating alignright">

														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>
														<li class="active"></li>

													</ul>
													
													<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

												</div>

											</div>

										</li>

										<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

									</ul><!--/ .list_of_products-->

								</div>

							</section>

							<!-- - - - - - - - - - - - - - End of you might also like - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

							<div class="section_offset">

								<section class="infoblock type_2">

									<i class="icon-lock"></i>

									<h4 class="caption"><b>Safe &amp; Secure Payment</b></h4>

									<p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna.</p>

									<a href="#" class="button_dark_grey middle_btn">Read More</a>

								</section><!--/ .infoblock.type_2-->

							</div>

							<!-- - - - - - - - - - - - - - End infoblock - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Already viewed products - - - - - - - - - - - - - - - - -->

							<section class="section_offset">

								<h3>Already Viewed Products</h3>

								<ul class="products_list_widget">

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<li>
										
										<a href="#" class="product_thumb">
											
											<img src="images/product_thumb_4.jpg" alt="">

										</a>

										<div class="wrapper">

											<a href="#" class="product_title">Adipiscing aliquet sed in lacus...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><b>$5.99</b></p>

												<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

												<ul class="rating alignright">

													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li class="active"></li>
													<li></li>

												</ul>
												
												<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

											</div>

										</div>

									</li>

									<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

									<li>
										
										<a href="#" class="product_thumb">
											
											<img src="images/product_thumb_11.jpg" alt="">

										</a>

										<div class="wrapper">

											<a href="#" class="product_title">Ut pharetra augue nec augue,...</a>

											<div class="clearfix product_info">

												<p class="product_price alignleft"><s>$19.99</s> <b>$13.99</b></p>

											</div>

										</div>

									</li>

									<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

								</ul><!--/ .list_of_products-->

							</section>

							<!-- - - - - - - - - - - - - - End of already viewed products - - - - - - - - - - - - - - - - -->

						</aside><!--/ [col]-->

					</div><!--/ .row-->

				</div><!--/ .container-->

			</div>


<script type="text/javascript">
	var harga = $('#harga_satuan').val();

	$('.calculate').on('click', function(){
		operasi = $(this).data('direction');
		if(operasi == 'plus'){
			jml = $(this).parent().find($('#jumlah')).val();
			jml2 = parseInt(jml) + 1;
			$(this).parent().find($('#jumlah')).val(jml);
		}else{
			jml = $(this).parent().find($('#jumlah')).val();
			jml2 = parseInt(jml) - 1;
			$(this).parent().find($('#jumlah')).val(jml);
		}
		total = harga * parseInt(jml2);
		if(total <= 0){
			alert('Ups, harap perbaiki jumlah pembelian');
			return false;
		}
		$(this).parent().parent().parent().find('.product_price').text('Rp '+total.format(0, 3, '.', ','));
	});

	$('#jumlah').on('change', function(){
		jml = $(this).val();
		total = harga * parseInt(jml);
		if(total <= 0){
			alert('Ups, harap perbaiki jumlah pembelian');
			$(this).val('1');
			$(this).parent().parent().parent().find('.product_price').text('Rp '+parseInt(harga).format(0, 3, '.', ','));
			return false;
		}
		$('.product_price').text('Rp '+total.format(0, 3, '.', ','));
	});

</script>