
					<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

					<div class="page_wrapper">

						<div class="container">

							<section data-animation="fadeInDown" class="section_offset animated visible fadeInDown">
								<h3 class="offset_title">Produk Hari ini</h3>
								<div class="tabs type_3 products initialized">
									<div class="tab_containers_wrap">
										<?php foreach($product_list as $produk):?>
										<div class="product_item col-md-3" style="height: 403px;">
											<div class="image_wrap">

												<img alt="" src="<?php echo base_url();?>assets/store/product/<?php echo $produk->seller_id; ?>/<?php echo $produk->product_img; ?>">

												<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

												<div class="actions_wrap">

													<div class="centered_buttons">

														<a class="button_dark_grey middle_btn quick_view pb" href="#" data-modal-url="http://localhost:1234/viaramall/products/product_index/quick_view2/<?php echo $produk->id; ?>">Quick View</a>

													</div><!--/ .centered_buttons -->

												</div><!--/ .actions_wrap-->
												
												<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->
											</div>
											<!--<div class="label_bestseller">Bestseller</div>-->
											<div class="description align_center">
												<p><a href="#"><b><?php echo ucfirst($produk->product_name); ?></b></a></p>
												<div class="clearfix product_info">
													<p class="product_price alignleft"><b>Rp. <?php echo number_format( $produk->product_harga , 0 , ',' , '.' );?></b></p>
												</div>
												<button class="button_blue middle_btn">Beli</button>
												<!--<ul class="bottombar">
													<li><a href="#">Tambah ke Wishlist</a></li>
												</ul>-->
												<span style="display: block; margin-top: 20px;"><a href="#"><?php echo $produk->nama_toko; ?> | Jakarta</a></span>
											</div>
										</div>
										<?php endforeach;?>
									</div>
								</div>
							</section>

						</div><!--/ .container-->

					</div><!--/ .page_wrapper-->
					
					<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

					<footer id="footer">

						<!-- - - - - - - - - - - - - - Footer section- - - - - - - - - - - - - - - - -->

						<div class="footer_section_2">

							<div class="container">
								
								<div class="row">
									
									<!-- - - - - - - - - - - - - - Newsletter - - - - - - - - - - - - - - - - -->

									<div class="col-md-6 col-sm-7">

										<section class="streamlined_type_2">
										
											<h4 class="streamlined_title">Sign up to our newsletter</h4>

											<form class="newsletter subscribe" novalidate>

												<input type="email" name="sc_email" placeholder="Enter your email address">

												<button class="button_blue def_icon_btn"></button>

											</form>

										</section><!--/ .streamlined-->

									</div><!--/ [col]-->
									
									<!-- - - - - - - - - - - - - - End newsletter - - - - - - - - - - - - - - - - -->

									<!-- - - - - - - - - - - - - - Follow us - - - - - - - - - - - - - - - - -->

									<div class="col-md-6 col-sm-5">

										<section class="streamlined">
										
											<h4 class="streamlined_title">Follow Us</h4>

											<!-- - - - - - - - - - - - - - Social icon's list - - - - - - - - - - - - - - - - -->

											<ul class="social_btns">

												<li>
													<a href="#" class="icon_btn middle_btn social_facebook tooltip_container"><i class="icon-facebook-1"></i><span class="tooltip top">Facebook</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_twitter tooltip_container"><i class="icon-twitter"></i><span class="tooltip top">Twitter</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_googleplus tooltip_container"><i class="icon-gplus-2"></i><span class="tooltip top">GooglePlus</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_pinterest tooltip_container"><i class="icon-pinterest-3"></i><span class="tooltip top">Pinterest</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_flickr tooltip_container"><i class="icon-flickr-1"></i><span class="tooltip top">Flickr</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_youtube tooltip_container"><i class="icon-youtube"></i><span class="tooltip top">Youtube</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_vimeo tooltip_container"><i class="icon-vimeo-2"></i><span class="tooltip top">Vimeo</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_instagram tooltip_container"><i class="icon-instagram-4"></i><span class="tooltip top">Instagram</span></a>
												</li>

												<li>
													<a href="#" class="icon_btn middle_btn social_linkedin tooltip_container"><i class="icon-linkedin-5"></i><span class="tooltip top">LinkedIn</span></a>
												</li>

											</ul>
											
											<!-- - - - - - - - - - - - - - End social icon's list - - - - - - - - - - - - - - - - -->

										</section><!--/ .streamlined-->
									
									</div><!--/ [col]-->
									
									<!-- - - - - - - - - - - - - - End follow us - - - - - - - - - - - - - - - - -->

								</div><!--/ .row-->

							</div><!--/ .container-->

						</div><!--/ .footer_section_2-->

						<!-- - - - - - - - - - - - - - End footer section- - - - - - - - - - - - - - - - -->

						<hr>

						<!-- - - - - - - - - - - - - - Footer section- - - - - - - - - - - - - - - - -->

						<div class="footer_section">

							<div class="container">
								
								<div class="row">

									<div class="col-md-3 col-sm-6">

										<!-- - - - - - - - - - - - - - Featured products widget- - - - - - - - - - - - - - - - -->

										<section class="widget">

											<h4>Featured Products</h4>

											<ul class="products_list_widget">

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_1.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Nemo Enim Ipsam Voluptatem, Lemon 4 fl oz (118 ml)</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$6.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_2.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Lorem Ipsum Dolor Sit Amet Consectetuer Elit</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$14.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_3.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Ipsum Dolor Sit Amet Consectetuer Elit</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$24.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

											</ul><!--/ .list_of_products-->

										</section>
										
										<!-- - - - - - - - - - - - - - End of featured products widget - - - - - - - - - - - - - - - - -->

									</div><!--/ [col]-->

									<div class="col-md-3 col-sm-6">

										<!-- - - - - - - - - - - - - - Bestseller products widget - - - - - - - - - - - - - - - - -->

										<section class="widget">

											<h4>Bestseller Products</h4>

											<ul class="products_list_widget">

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_4.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Aenean Auctor Wisi Et Urna Amet, Liqui-gels 24...</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$5.99</b></p>

															<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

															<ul class="rating alignright">

																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>
																<li></li>

															</ul>
															
															<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_5.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Dolor Sit Amet Consectetuer Amet Ipsum</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$8.99</b></p>

															<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

															<ul class="rating alignright">

																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>

															</ul>
															
															<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_6.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Dolor Sit Amet Lorem, 2mg, Ice Mint 160 ea</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$76.99</b></p>

															<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

															<ul class="rating alignright">

																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>
																<li class="active"></li>

															</ul>
															
															<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

											</ul><!--/ .list_of_products-->

										</section><!--/ .widget-->
										
										<!-- - - - - - - - - - - - - - End of bestseller products widget - - - - - - - - - - - - - - - - -->
									
									</div><!--/ [col]-->

									<div class="col-md-3 col-sm-6">

										<!-- - - - - - - - - - - - - - New products widget - - - - - - - - - - - - - - - - -->

										<section class="widget">

											<h4>New Products</h4>

											<ul class="products_list_widget">

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_7.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Lorem Ipsum Dolor Sit Amet Consectetuer Elit...</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$2.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_8.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Bounty - Paper Napkins, Assorted, 200 ea</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$4.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_9.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Lorem Ipsum Dolor Sit Amet, Deap Clean, Cool</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><b>$3.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

											</ul><!--/ .list_of_products-->

										</section><!--/ .widget-->

										<!-- - - - - - - - - - - - - - End of new products widget - - - - - - - - - - - - - - - - -->

									</div><!--/ [col]-->

									<div class="col-md-3 col-sm-6">

										<!-- - - - - - - - - - - - - - On sale products widget - - - - - - - - - - - - - - - - -->

										<section class="widget">

											<h4>On Sale Products</h4>

											<ul class="products_list_widget">

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_10.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Ipsum Dolor Sit Amet Adipiscing Elit Lorem...</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><s>$29.99</s> <b>$21.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_11.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Amet Consectetuer Adipiscing Elit Ut Dolor Amet...</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><s>$19.99</s> <b>$13.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

												<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

												<li>
													
													<a href="#" class="product_thumb">
														
														<img src="images/product_thumb_12.jpg" alt="">

													</a>

													<div class="wrapper">

														<a href="#" class="product_title">Vestibulum Laculis Lacinia Est Dolor Amet</a>

														<div class="clearfix product_info">

															<p class="product_price alignleft"><s>$5.99</s> <b>$2.99</b></p>

														</div>

													</div>

												</li>

												<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

											</ul><!--/ .list_of_products-->

										</section><!--/ .widget-->

										<!-- - - - - - - - - - - - - - End of on sale products widget - - - - - - - - - - - - - - - - -->

									</div>

								</div><!--/ .row-->

							</div><!--/ .container-->

						</div><!--/ .footer_section_2-->

						<!-- - - - - - - - - - - - - - End footer section- - - - - - - - - - - - - - - - -->

						<hr>

						<!-- - - - - - - - - - - - - - Footer section - - - - - - - - - - - - - - - - -->

						<div class="footer_section_3 align_center">

							<div class="container">

								<p class="footer_message">This is custom text. Vestibulum sed ante. Donec sagittis euismod purus. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantim, totam rem aperiam, eaque ipse quae ab illo inventore veritatis.</p>

								<!-- - - - - - - - - - - - - - Payments - - - - - - - - - - - - - - - - -->

								<ul class="payments">

									<li><img src="images/payment_1.png" alt=""></li>
									<li><img src="images/payment_2.png" alt=""></li>
									<li><img src="images/payment_3.png" alt=""></li>
									<li><img src="images/payment_4.png" alt=""></li>
									<li><img src="images/payment_5.png" alt=""></li>
									<li><img src="images/payment_6.png" alt=""></li>
									<li><img src="images/payment_7.png" alt=""></li>
									<li><img src="images/payment_8.png" alt=""></li>

								</ul>
								
								<!-- - - - - - - - - - - - - - End of payments - - - - - - - - - - - - - - - - -->

								<!-- - - - - - - - - - - - - - Footer navigation - - - - - - - - - - - - - - - - -->

								<nav class="footer_nav">

									<ul class="bottombar">

										<li><a href="#">Medicine &amp; Health</a></li>
										<li><a href="#">Beauty</a></li>
										<li><a href="#">Personal Care</a></li>
										<li><a href="#">Vitamins &amp; Supplements</a></li>
										<li><a href="#">Baby Needs</a></li>
										<li><a href="#">Diet &amp; Fitness</a></li>
										<li><a href="#">Sexual Well-being</a></li>

									</ul>

								</nav>
								
								<!-- - - - - - - - - - - - - - End of footer navigation - - - - - - - - - - - - - - - - -->

								<p class="copyright">&copy; 2015 <a href="index.html">Shopme</a>. All Rights Reserved.</p>

							</div><!--/ .container-->

						</div><!--/ .footer_section-->

						<!-- - - - - - - - - - - - - - End footer section - - - - - - - - - - - - - - - - -->

					</footer>
					
					<!-- - - - - - - - - - - - - - End Footer - - - - - - - - - - - - - - - - -->

