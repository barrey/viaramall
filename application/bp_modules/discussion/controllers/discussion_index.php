<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class discussion_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('store_model');
	}

	public function index()
	{
		//data2 sidebar
			$user_id = $this->session->userdata('user_id');	
			$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);

		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('discussion/discussion_index', $data, 'components/backend');
	}

}

/* End of file discussion_index.php */
/* Location: ./application/controllers/discussion_index.php */