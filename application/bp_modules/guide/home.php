<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//render with default master template (master/frontend)
		$this->render('guide/home');

		//if you want to override default master template
		//$data = array(); //it's necessary to include array parameter in 'render' function
		//$this->render('welcome_message',$data,'master/backend');
	}

	public function pretty_url(){
		/*
		this method provide simple access url. for example you want your user to access guide/pretty_url but you have pretty_url in 'guide/home'.
		normally to access this method is user must enter 'guide/home/pretty_url' but with this method you can simply enter with 'guide/pretty_url'and if users enter guide/home/pretty_url will be redirect to guide/pretty_url.
		but first, route must be set in routes.php
		set in routes.php:
		$route['guide/pretty_url'] = 'guide/home/pretty_url';
		*/
		$this->general->check_url('3', 'pretty_url', 'guide/pretty_url');
		echo "pretty_url";
	}

	public function get_count_data_user(){
		$this->load->model('user_model');

		$manual = "select count(*) as i from user where username='admin'";
		echo $this->user_model->	($manual, 'i');
		echo '<br>';
		echo $this->db->last_query();

	}

	/**
	DB INTERACTION
	*/

	//GET

	public function grab_one_table_all(){
		$this->load->model('user_model');
		$user = $this->user_model->get_all();
		echo $this->db->last_query();

		var_dump($user);
	}

	public function grab_one_table_single(){
		$this->load->model('user_model');

		$where = array('id' => array('where' => 4));
		$this->user_model->set_param($where);
		$user = $this->user_model->get_one();
		echo $this->db->last_query();
		var_dump($user);

		/**
		NEW QUERY BUT WITH SAME PARAMETER
		*/

		$this->user_model->set_table('level');
		$user = $this->user_model->get_one();
		echo $this->db->last_query();
		var_dump($user);

		/**
		NEW QUERY WITH NEW PARAMETER
		*/

		$this->user_model->set_table('ref_menu');
		$this->user_model->clear_param();

		$where = array('id' => array('where' => 1));
		$this->user_model->set_param($where);
		$user = $this->user_model->get_one();

		echo $this->db->last_query();
		var_dump($user);
	}

	public function grab_join_table_all(){
		$this->load->model('user_model');

		$this->user_model->set_table_from("user");

		$this->user_model->set_table_join(array('level as a' => array('a.id = user.level_id')));

		$where = array('a.id' => array('where' => 2), 'limit' => array(1,2), 'order_by' => array('email', 'asc'));
		$this->user_model->set_param($where);

		//buggy: 
		/**
		1. masih butuh select_cell (seharusnya default adalah *) FIXED
		2. parameter harus ada (seharusnya tidak harus) FIXED
		*/

		$data = $this->user_model->get_join_all();
		echo $this->db->last_query();
		var_dump($data);


	}

	public function grab_join_table_single(){
		$this->load->model('user_model');

		$this->user_model->set_table_from("user");

		$this->user_model->set_table_join(array('level as a' => array('a.id = user.level_id')));
		$this->user_model->set_select(array('email, password'));
		$where = array('a.id' => array('where' => 2));
		$this->user_model->set_param($where);
		
		//buggy: 
		/**
		1. masih butuh select_cell (seharusnya default adalah *) FIXED
		2. parameter harus ada (seharusnya tidak harus) FIXED
		*/

		$data = $this->user_model->get_join_one();
		echo $this->db->last_query();
		var_dump($data);
	}

	public function count_join(){
		$this->load->model('user_model');

		$this->user_model->set_table_from("user");

		$this->user_model->set_table_join(array('level as a' => array('a.id = user.level_id')));

		$data = $this->user_model->count_join();
		echo $this->db->last_query();
		var_dump($data);
		echo $data;

	}

	//INSERT
	public function insert(){
		$this->load->model('user_model');
		$data = array('username' => 'ita', 'realname' => 'ita purwasih', 'email' => 'itaaja', 'password' => 'teh');
		$this->user_model->insert($data);
	}

	//UPDATE
	public function update(){
		$this->load->model('user_model');

		//you better check insert first!!!
		$this->user_model->set_param(array('username' => array('where'=>'ita')));
		$data = array('username' => 'Rita', 'realname' => 'ita purwasih', 'email' => 'itaaja', 'password' => 'teh');
		$this->user_model->update($data);
	}

	//DELETE
	public function delete(){

		//you better check update first!!!
		$this->load->model('user_model');
		$this->user_model->set_param(array('username' => array('where' => 'Rita')));
		$this->user_model->delete();
	}

	public function check(){
		//this function to check duplication for data
		$this->load->model('user_model');
		/*
			in this case, username = "skdasjd" ada double di database.
		*/
		$where = array('username' => array('where' => 'skdasjd'));
		$this->user_model->set_param($where);
		if($this->user_model->check('insert')){
			echo "UNIK";
		}else{
			echo "TIDAK UNIK";
		}	
		echo ' untuk username skdasjd (metode insert)';
		echo '<br>';
		/*
			in this case, username = "tesuser" sudah ada satu di database dan akan dilakukan metode update data
		*/
		$where = array('username' => array('where' => 'tesuser'));
		$this->user_model->set_param($where);
		if($this->user_model->check('edit')){
			echo "UNIK";
		}else{
			echo "TIDAK UNIK";
		}		
		echo ' untuk username tesuser (metode edit)';
		echo '<br>';
		/*
			in this case, username = "tesuser" sudah ada satu di database dan akan dilakukan metode insert data
		*/
		$where = array('username' => array('where' => 'tesuser'));
		$this->user_model->set_param($where);
		if($this->user_model->check('insert')){
			echo "UNIK";
		}else{
			echo "TIDAK UNIK";
		}
		echo ' untuk username tesuser (metode insert)';
	}

}

/* End of file  */
/* Location: ./application/controllers/ */