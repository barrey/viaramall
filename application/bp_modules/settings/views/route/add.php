<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<style>
	.chosen-single span{
		margin: 6px;
	}
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Tambah Route</h4>
</div>
<div class="modal-body">
	<?php echo form_open("settings/route/add/$gridname",array('name'=>'formItem','id'=>'route-add','method'=>'POST', 'class'=>"form-horizontal"));?>
	<div class="form-group">
		<label class="col-md-3 control-label">Slug</label>
		<div class="col-md-9">
			<input type="text" name="slug" class="form-control" placeholder="Slug Route">
    		<?php echo form_error('slug'); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">Controller</label>
		<div class="col-md-9">
			<input type="text" class="form-control" placeholder="Kode BPS" name="code" value="<?php echo  set_value('code'); ?>">
			<!--<span class="help-inline">Some hint here</span>-->
			<?php echo form_error('code');?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">Nama Desa</label>
		<div class="col-md-9">
			<input type="text" class="form-control" placeholder="Nama Desa" name="name" value="<?php echo  set_value('name'); ?>">
			<!--<span class="help-inline">Some hint here</span>-->
			<?php echo form_error('name');?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">Status</label>
		<div class="col-md-9">
			<label class="radio-inline">
				<input type="radio" name="status" value="n" checked="checked"> Aktif
			</label>
			<label class="radio-inline">
				<input type="radio" name="status" value="y"> Tidak Aktif
			</label>
			<input type="hidden" name="method" value="add" />
			<!--<span class="help-inline">Some hint here</span>-->
		</div>
	</div>
	<div class="form-group">
	    <div class="col-md-9 col-md-offset-3">
		    <button type="submit" class="btn btn-primary">Simpan</button>
		    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
	    </div>
	  	</div>
	<?php echo form_close();?> 
</div>

<!--<script src="<?php echo base_url().'/'.$this->config->item('com_validator');?>"></script>-->
<script type="text/javascript">
$(document).ready(function() {
	
	$(".chosen").chosen({no_results_text: "Maaf, Nama Kecamatan tidak ditemukan!"}); 

	<?php if(set_value('subdistrict') != ''):?>
		$('.chosen').val(<?php echo set_value('subdistrict'); ?>);
		$('.chosen').trigger('chosen:updated');
	<?php endif; ?>
	
	$('input[name="code"]').inputmask({"mask": "999999999"});

	$('#desa-add').ajaxForm({
        target: '#dialog-desa .modal-content',
        type: "POST",
        beforeSubmit: function () {
            $('#dialog-desa .modal-dialog').block({
                message: '<div><img style="" src="<?php echo base_url('/assets/images/loader/ajax-modal-loading.gif'); ?>"></div>',
                css: { border: 'none', background: 'transparent' }
            });
        },
        success: function() {
            $("#newapi<?php echo $gridname;?>").jqGrid('setGridParam',{
                url:"<?php echo $dataurl;?>",
                page:1
            }).trigger("reloadGrid");
            $('#dialog-desa .modal-dialog').unblock();
            console.log('<?php echo $gridname.' '.$dataurl;?> ');
        }
    });

    /*var optionss = { 
	    target: '#desa_add',
	    type: 'POST',
	    success: function(response){
	        $("#newapi<?php echo $gridname;?>").jqGrid('setGridParam',{
	            url:"<?php echo $dataurl;?>",
	            page:1
	        }).trigger("reloadGrid");
	    }
	}; 
	    $('#form_desa').validate({
	    	errorElement: 'label',
	    	errorClass: 'help-inline',
	    	focusInvalid: false,
	    	ignore: [],
	    	rules:{
	    		subdistrict: {
	    			required: true
	    		},
	    		code: {
	    			required: true
	    		},
	    		name: {
	    			required: true
	    		},
	    		status: {
	    			required: true
	    		}
	    	},
	    	messages: {
	    		subdistrict: {
	    			required: "Nama Kecamatan harap diisi."
	    		},
	    		code: {
	    			required: "Kode BPS harap diisi."
	    		},
	    		name: {
	    			required: "Nama Desa harap diisi."
	    		},
	    		status: {
	    			required: "Status harap dipilih."
	    		}
	    	},
	    	invalidHandler: function(event, validator){
	    		$('.alert-error', $('#form_desa')).show();
	    	},
	    	highlight: function(element){
	    		$(element).closest('.control-group').addClass('error');
	    	},
	    	success: function(label){
	    		label.closest('.control-group').removeClass('error').addClass('success');
	    		label.remove();
	    		$("#form_desa").ajaxForm(optionss);
	    	},
	    	errorPlacement: function(error, element){
	    		if (element.attr("name") == "subdistrict") { 
	    		// for chosen elements, need to insert the error after the chosen container
                    error.addClass('help-small no-left-padding').insertAfter("#subdistrict_chzn");;
                }
                else
                {
	    			error.addClass('help-small no-left-padding').insertAfter(element.closest('.medium'));
	    		}
	    	}
	    });
            
            
            //apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
			var formx = $('#form_desa');
                    $('.chosen, .chosen-with-diselect', formx).change(function () {
                        formx.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                    });

    
 
                
                $('input[name="code"]').inputmask({"mask": "999999999"});
		<?php
		if(!empty($sukses))
		{

		}
		?>*/	
	});
</script> 
