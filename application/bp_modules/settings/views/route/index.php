<?php echo Modules::run('dashboard/components/head_content'); ?>

<!-- PENCARIAN -->
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption"><i class="icon-search"></i>Pencarian</div>
        <div class="tools">
            <a class="expand" href="javascript:;"></a>
        </div>
    </div>
    <div class="portlet-body mapworld form" style="display: none;">
      <div class="col-md-6" style="padding-top: 2%; padding-bottom: 2%;">
        <form class="form-horizontal form-pencarian-jqgrid" id="form_pencarian">
        <div class="form-group fc">
            <label class="control-label putih col-md-4">Kode BPS Desa</label>
            <div class="controls col-md-5 ml5p">
                <input id="kode-bps-desa" name="code" class="m-wrap medium angka" type="text" placeholder="Kode BPS Desa" size="15">
            </div>
        </div>
        <div class="form-group fc">
            <label class="control-label putih col-md-4">Nama Desa</label>
            <div class="controls col-md-5 ml5p">
                <input id="nama-desa" name="name" type="text" class="m-wrap medium" placeholder="Nama Desa">
            </div>
        </div>
        <!--<div class="form-group fc">
            <label class="control-label putih col-md-4">Kode BPS Propinsi</label>
            <div class="controls col-md-5 ml5p">
                <input id="kode-bps-propinsi" name="code_province" type="text" class="m-wrap small" placeholder="Kode BPS">
            </div>
        </div>-->
        <div class="form-group fc">
            <label class="control-label putih col-md-4">Nama Propinsi</label>
            <div class="controls col-md-5 ml5p">
                <input id="nama-propinsi" name="name_province" type="text" class="m-wrap medium" placeholder="Nama Propinsi">
                <input type="hidden" name="cari" value="TRUE"/>
            </div>
        </div>
        <button class="btn default purple-stripe ml200" type="button" onclick="gridReload()">Cari
            <i class="m-icon-swapright m-icon-white"></i>
        </button>
        <button class="btn default red-stripe col-md-offset-1" type="reset">Batal <i class=" icon-remove-sign"></i>
        </button>
    </form>
    </div>
    <div class="clearfix"></div>
    </div>
</div>

<!-- JQGRID -->
<div class="jqgrid-content" id="jqgrid-desa">
    <table class="grid-container" id="newapi<?php echo $gridname;?>"></table>
    <div class="col-md-12" id="pnewapi<?php echo $gridname; ?>"></div>
</div>

<!-- MODAL -->
<div class="modal fade" id="dialog-route" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Desa</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>

<script>

  head.ready(document, function() {
    head.load('<?php echo base_url()?>assets/plugins/metronic/plugins/chosen/chosen.jquery.min.js', function(){
      console.log('loaded');
    });
  });

  $(document).ready(function(){

    //jqGrid component
    <?php echo $table; ?>

    <?php if(!empty($add)){echo $add;} ?>
    <?php if(!empty($edit_popup)){echo $edit_popup;} ?>
    <?php if(!empty($edit_status)){echo $edit_status;} ?>
    //end of jqGrid component

    // set jqGrid's width onload
    var lebar_container = $('.jqgrid-content').width();
    $(".grid-container").setGridWidth(lebar_container, 'shrink');
    // end set

  });

  //$('.angka').inputmask({"mask": "999999999"});


  function gridReload(){
    $("#newapi<?php echo $gridname;?>").jqGrid('setGridParam',{
              url:"<?php echo $dataurl;?>",
              postData: {
                  kode_bps_desa: function(){return $('#kode-bps-desa').val()},
                  nama_desa: function(){return $('#nama-desa').val()},
                  nama_propinsi: function(){return $('#nama-propinsi').val()},
                  pencarian: function(){return true;}
              },
              page:1
          }).trigger("reloadGrid");
      }

</script>