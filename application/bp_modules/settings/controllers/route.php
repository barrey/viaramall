<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Route extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$colmodel = json_encode(array(
            array('name'=>'id', 'label'=>'ID', 'hidden'=> 1, 'classes'=>'hidden','width'=>40, 'align'=>'left'),
            array('name'=>'slug','label'=>'Slug','width'=>160,'align'=>'center'),
            array('name'=>'controller','label'=>'Controller','width'=>250,'align'=>'left'),
            array('name'=>'del','label'=>'Status','width'=>60,'align'=>'center')
        ));
		
        //parsing data to view
        $data = array(
            'gridname' => $gridname = '_'.time(),
            'dataurl' => site_url('/settings/route/get_all/'),
            'rownum' => '20',
            'caption' => 'Route',
            'colmodel' => $colmodel,
            'sortname' => 'slug',
        );

		$jqgrid=modules::load('components/jqgrid');
		$data['table'] = $jqgrid->table($data);

		//pemanggilan cross controller [add data jqgrid]
		
        $data_add = array(
            'div_loader'=>'#dialog-route',
            'url_to_load'=>base_url().'settings/route/add/'.$gridname
            );
        $data2 = array_merge($data,$data_add);
        $data['add'] = $jqgrid->add($data2); 
		
        $this->render('settings/route/index', $data);
	}

	public function add(){
		$this->render('settings/route/add');
	}

	public function get_all(){
		//method untuk provide data jqgrid
		$this->load->model('components/bp_route_model', 'app_routes');
        echo json_encode($this->app_routes->get_all());
	}

	public function read_me(){
		echo "untuk membuat jqgrid, agak ribet nih.<br>";
		echo "setting parameter di method index.";
		echo "buat model get_all di model lalu di json_encode() <br>";
		echo "lalu buat view, view nya ada struktur nya";
	}

}

/* End of file route.php */
/* Location: ./application/controllers/route.php */