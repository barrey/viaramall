<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//render view email settings
		$data = array();
		$this->render('settings/email/index', $data);
	}

	public function update()
	{
		//to save new data or edit
	}

}

/* End of file  */
/* Location: ./application/controllers/ */