<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/bootstrap/font-awesome.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jquery-ui/jquery-ui-1.10.3.custom.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jqgrid/ui.jqgrid.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/jqgrid/jqGrid.overrides.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url()?>assets/css/frontend.css">

	<script src="<?php echo base_url()?>assets/js/head.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()?>asset/js/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jqgrid/grid.locale-id.js"></script>
	<script src="<?php echo base_url()?>assets/js/jqgrid/jquery.jqGrid.min.js"></script>
	<script class="include" type="text/javascript" src="<?php echo base_url()?>assets/js/viaramall/backend/jquery.dcjqaccordion.2.7.js"></script>
	
	<?php
		if(!empty($head))
		{
			echo $head;
		}
	?>
</head>
<body>
	<?php
		if(!empty($header))
		{
			echo $header;
		}
	?>

	<?php
		if(!empty($content))
		{
			echo $content;
		}
	?>

	<?php 
		if(!empty($footer))
		{
			echo $footer;
		}
	?>
	<?php
		if(!empty($arr))
		{
			foreach($arr as $data)
			{
				echo $data.'<br />';
			}
		}
	?>

	<script src="<?php echo base_url()?>assets/js/jquery.form-3.5.js"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/backend/jquery.scrollTo.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/backend/jquery.nicescroll.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/backend/jquery.sparkline.js"></script>
	<script src="<?php echo base_url()?>assets/js/viaramall/backend/common-scripts.js"></script>

	<script type="text/javascript" src="<?php echo base_url()?>assets/js/viaramall/backend/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/viaramall/backend/gritter-conf.js"></script>

	<!--script for this page-->
	<script src="<?php echo base_url()?>assets/js/viaramall/backend/sparkline-chart.js"></script>    
	<script src="<?php echo base_url()?>assets/js/viaramall/backend/zabuto_calendar.js"></script>	
	
	<script type="text/javascript">
	    $(document).ready(function () {
	    var unique_id = $.gritter.add({
	        // (string | mandatory) the heading of the notification
	        title: 'Welcome to Dashgum!',
	        // (string | mandatory) the text inside the notification
	        text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
	        // (string | optional) the image to display on the left
	        image: 'assets/img/ui-sam.jpg',
	        // (bool | optional) if you want it to fade out on its own or just sit there
	        sticky: true,
	        // (int | optional) the time you want it to be alive for before fading out
	        time: '',
	        // (string | optional) the class name you want to apply to that specific message
	        class_name: 'my-sticky-class'
	    });

	    return false;
	    });
	</script>

	<script type="application/javascript">
	    $(document).ready(function () {
	        $("#date-popover").popover({html: true, trigger: "manual"});
	        $("#date-popover").hide();
	        $("#date-popover").click(function (e) {
	            $(this).hide();
	        });
	    
	        $("#my-calendar").zabuto_calendar({
	            action: function () {
	                return myDateFunction(this.id, false);
	            },
	            action_nav: function () {
	                return myNavFunction(this.id);
	            },
	            ajax: {
	                url: "show_data.php?action=1",
	                modal: true
	            },
	            legend: [
	                {type: "text", label: "Special event", badge: "00"},
	                {type: "block", label: "Regular event", }
	            ]
	        });
	    });
	    
	    
	    function myNavFunction(id) {
	        $("#date-popover").hide();
	        var nav = $("#" + id).data("navigation");
	        var to = $("#" + id).data("to");
	        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
	    }
	</script>
</body>
</html>