<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends Public_Model {

	public $variable;
	protected $table = "user";

	public function __construct()
	{
		parent::__construct();
	}

	public function table(){
		echo $this->table;
	}

	public function set_table($input){
		return $this->table = $input;
	}

}

/* End of file  */
/* Location: ./application/models/ */