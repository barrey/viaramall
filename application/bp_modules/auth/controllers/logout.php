<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->bp_auth=modules::load('components/bp_auth');
	}

	public function index()
	{
		$this->bp_auth->logout();
	}

}

/* End of file  */
/* Location: ./application/controllers/ */