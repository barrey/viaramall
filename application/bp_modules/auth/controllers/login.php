<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public $classname;
	public $funcname;
	public $pass;

	public function __construct()
	{
		parent::__construct();
		$this->bp_auth = modules::load('components/bp_auth');
		$this->bp_log = modules::load('components/bp_log');
		$this->load->model('auth/login_model', 'login');
		$this->bp_log->classname = __CLASS__;
	}

	public function index()
	{
		//check POST
		//validation
		//handler

		$this->bp_log->log_func_name(__FUNCTION__);

		//should be note, form processor and login serve must on the same url. for validation and error feedback
		$input = $this->input->post();

		//no input
			if(!$input){
				if(!$this->bp_auth->login()){
					return FALSE;
				}

				else{
					$url = $this->session->userdata('url');
					redirect(base_url().$url);
				}
			}

		//there's input
			else{
				$input = $this->input->post();

				//login validation
				$this->pass = $this->bp_auth->login_validation($input);

				if($this->pass == TRUE){
					//login failed
					if(!$this->bp_auth->login_handler($input)){
						$data['login_error'] = TRUE;
						$this->bp_auth->login_serve($data);
					}	
				}

				else{
					$this->bp_auth->login_serve();
				}
				
			}
	}

	public function admin()
	{
		$tipe = 'admin';
		//check POST
		//validation
		//handler

		$this->bp_log->log_func_name(__FUNCTION__);

		//should be note, form processor and login serve must on the same url. for validation and error feedback
		$input = $this->input->post();

		//no input
			if(!$input){
				if(!$this->bp_auth->login($tipe)){
					return FALSE;
				}

				else{
					$url = $this->session->userdata('url');
					redirect(base_url().$url);
				}
			}

		//there's input
			else{
				$input = $this->input->post();
				
				//login validation
				$this->pass = $this->bp_auth->login_validation($input);

				if($this->pass == TRUE){
					//login failed
					if(!$this->bp_auth->login_handler($input, 'admin')){
						$data['login_error'] = TRUE;
						$this->bp_auth->login_serve($data, 'admin');
					}	
				}

				else{
					$this->bp_auth->login_serve('admin');
				}
				
			}
	}

	public function register()
	{
		$tipe = $this->input->post('tipe');
		$post = $this->input->post();

		if(!empty($tipe)){
			if($tipe == 'admin'){
				$data = $this->_register_admin($post);
				if($data){
					$this->render('admin/admin_register_success');
				}
			}
		}

		else{
			//validasi
			//send email
			//serve
		}
	}

	function _register_admin($post)
	{
		$this->load->model('user_admin_model');
		$this->user_admin_model->insert(array(
			'first_name' => $post['first_name'],
			'middle_name' => $post['middle_name'],
			'last_name' => $post['last_name'],
			'username' => $post['username'], 
			'password' => md5($post['password']),
			'email' => $post['email'],
			'created_date' => date('Y-m-d'))
		);

		if($this->db->affected_rows()){
			return TRUE;
		}
	}

	public function forget_password()
	{
		$data['head'] = '<link rel="stylesheet" type="text/css" media="all" href="'.base_url().'assets/css/viaramall/login.css">';
		$this->render('auth/forget_password', $data);
	}
}

/* End of file  */
/* Location: ./application/controllers/ */