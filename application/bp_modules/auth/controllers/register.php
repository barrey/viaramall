<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {

	public $classname;
	public $funcname;

	public function __construct()
	{
		parent::__construct();
		$this->bp_auth = modules::load('components/bp_auth');
		$this->bp_log = modules::load('components/bp_log');
		$this->bp_log->classname = __CLASS__;
	}

	public function index()
	{
		$this->bp_log->log_func_name(__FUNCTION__);

		//should be note, form processor and login serve must on the same url. for validation and error feedback
		$input = $this->input->post();

		//no input
		if(!$input){
			if(!$this->bp_auth->register()){
				return FALSE;
			}

			else{
				$url = $this->session->userdata('url');
				redirect(base_url().$url);
			}
		}

		//there's input
		else{

			$input = $this->input->post();
			$validation = $this->bp_auth->register_handler($input);
			if($validation){
				$this->session->set_userdata(array('user_id' => $validation));
				$this->register_detail('register');
			}

			else if(is_array($validation)){
				//email not unique
				$this->bp_auth->register_serve($validation);
			}

			else{
				$this->bp_auth->register_serve();
			}

		}
	}

	public function register_detail($method = null){
		$user_id = $this->session->userdata('user_id');
		//if no input
		$this->bp_data = modules::load('components/bp_data');

		$post = $this->input->post();

		if(empty($method)){
			//post: jk, birth_date, id_province, id_regency, id_subdistrict, alamat, hp_no, user_id
			//save detail user info
			$this->load->model('user_model');
			$this->load->model('user_address_model');
			$this->load->model('user_telp_model');
			
			$param_update = array('id' => array('where' => $user_id));
			$data = array('sex'=> $this->input->post('jk'),'birth_date' => $this->input->post('birth_date'));
			$data2 = array('user_id' => $user_id, 'alamat' => $this->input->post('alamat'), 'regency_id' => $this->input->post('regency'), 'subdistrict_id' => $this->input->post('subdistrict'));
			$data3 = array('user_id' => $user_id, 'no_telp' => $this->input->post('hp_no'));
			
			$this->user_model->set_param($param_update);
			$this->user_model->update($data);
			$this->user_address_model->insert($data2);
			$this->user_telp_model->insert($data3);

			//redirect ke halaman index
			redirect(base_url());
		}

		else{
			//next step after register
			$data['head'] = '<link rel="stylesheet" type="text/css" media="all" href="'.base_url().'assets/css/viaramall/login.css">';
			$data['province'] = $this->bp_data->get_province();
			$data['user_id'] = $user_id;
			//$this->render('user/register_detail',$data);
			$this->render('user/register_detail',$data,'components/frontend');
		}
	}

	public function unregister(){
		$this->bp_auth->_table = 'bp_user';
		$input = array('id' => 1);
		$this->bp_auth->unregister($input);	
	}

	public function register_confirm($link){
	}

	public function tes(){
		$this->bp_data = modules::load('components/bp_data');
		$data = array();	
		$data['user_id'] = $this->session->userdata('user_id');
			$data['province'] = $this->bp_data->get_province();

		$this->render('user/register_detail',$data,'components/frontend');
	}

}

/* End of file  */
/* Location: ./application/controllers/ */