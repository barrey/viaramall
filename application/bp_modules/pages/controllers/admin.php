<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->render('admin/admin_index',$data,'components/backend');
	}

	public function login()
	{
		$this->render('admin/login');
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */