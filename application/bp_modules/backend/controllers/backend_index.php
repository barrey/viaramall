<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function session()
	{
		$this->debug($this->session->all_userdata());
	}

}

/* End of file  */
/* Location: ./application/controllers/ */