<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_index extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('store_model');
	}

	public function view(){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();
		//data2 sidebar
			$user_id = $this->session->userdata('user_id');	
			$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);

		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('products/product_view', $data, 'components/backend');
	}

	public function edit($id_product){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();
		$post = $this->input->post();
		
		if(!empty($post)){
			//save to db

			//redirect ke list product

		}
		else{
			$this->load->model(array('store_produk_model','image_produk_model'));
			$data = array();
			$data_sidebar = array();
			$user_id = $this->session->userdata('user_id');
			$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);
			$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
			$data['category'] = $this->list_category();
			$data['user_id'] = $user_id;
			//populate data produk
				$where = array('id' => array('where' => $id_product));
				$this->store_produk_model->set_param($where);
				$data['produk'] = $this->store_produk_model->get_one();

				$store_id = $data['produk']->store_id;
				$where2 = array('store_id' => array('where' => $store_id), 'store_produk_id' => array('where' => $id_product));
				$this->image_produk_model->set_param($where2);
				$data['image'] = $this->image_produk_model->get_all();

				$this->render('products/product_edit', $data, 'components/backend');
		}

	}

	public function list_product(){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();

		//method to display all product list with jqgrid
		$objek = modules::load('components/jqgrid');

		//name, deskripsi, kategori, etalase(nanti dulu), harga, stok, upload date (created date)
		$col_model = json_encode(array(
			array('name' => 'product_img', 'label' => 'Gambar Produk', 'width' => 250, 'align' => 'center'),
			array('name' => 'product_name', 'label' => 'Nama Produk', 'width' => 300, 'align' => 'left'),
			array('name' => 'product_kategori', 'label' => 'Produk Kategori', 'width' => 200, 'align' => 'left'),
			array('name' => 'product_status', 'label' => 'Aksi', 'width' => 250, 'align' => 'center')
		));

		//parsing data to view
		$data = array(
			'gridname' => $gridname = '_'.time(),
			'dataurl' => site_url('products/product_index/list_json'),
			'rownum' => '20',
			'caption' => 'Produk',
			'colmodel' => $col_model,
			'sortname' => 'a.created_date',
		);

		$data['table'] = $objek->table($data);

		/*
		//pemanggilan cross controller [add data jqgrid]
		$data_add = array(
			'div_loader'=>'#dialog-produk',
			'url_to_load'=>base_url().'products/product_index/add/'.$gridname
                );
		$data2 = array_merge($data,$data_add);
		$data['add'] = $objek->add($data2);
		*/
		//pemanggilan cross controller [feature edit popup]
		$data_edit = array(
			'id_jqgrid'=>'#newapi'.$gridname,
			'class_link_jqgrid'=>'.edit-dialog-propinsi',
			'url_to_load'=>base_url().'referensi/propinsi/edit'
			);
		$data3 = array_merge($data,$data_edit);
		$data['edit_popup'] = $objek->edit_popup($data3);

		$user_id = $this->session->userdata('user_id');
		$data_sidebar = array();
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		//load view propinsi
		$this->render('products/product_index', $data, 'components/backend');
	}

	public function list_json(){
		/*
			select * from store_produk as a 
			left join kategori as b on b.id = a.kategori_id
			left join image_produk as c on c.store_produk_id = a.id and a.store_id = c.store_id
		*/
		$this->load->model('store_produk_model');
		$response = $this->store_produk_model->get_data();
        $rs = json_encode($response);
        echo $rs;
	}

	public function add(){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();

		$this->load->model('store_produk_model');
		$user_id = $this->session->userdata('user_id');	
		$store_id = $this->session->userdata('store_id');
		//add data product
		$submit_only = $this->input->post('submit_only');
		if(!empty($submit_only)){

			$post = $this->input->post();

			$product = array(
				'name' => $post['produk_name'],
				'deskripsi' => $post['deskripsi'],
				'kategori_id' => $post['produk_category'],
				'store_id' => $store_id,
				//'store_etalase_id' => ,
				'harga' => $post['harga'],
				'berat' => $post['berat'],
				'berat_tipe' => $post['berat_type'],
				'asuransi' => $post['asuransi'],
				'kondisi' => $post['kondisi'],
				'pengembalian' => $post['pengembalian'],
				'stok' => $post['stok'],
				'minimum_order' => $post['minimum_order'],
				'created_date' => date('Y-m-d')
			);

			//insert to store produk
			$this->store_produk_model->insert($product);
			
			$product_id = $this->db->insert_id();

			$input_field = 'produk_gambar';

			if($this->file_uploaded($input_field)){
				//check directory
					$this->load->library('general');
					$this->load->library('upload');

					$this->general->directory_check('assets/store/product/'.$user_id);

					$files = $_FILES;
    				$cpt = count($_FILES['produk_gambar']['name']);
    				for($i=0; $i<$cpt; $i++)
				    {           
				        $_FILES['produk_gambar']['name']= $files['produk_gambar']['name'][$i];
				        $_FILES['produk_gambar']['type']= $files['produk_gambar']['type'][$i];
				        $_FILES['produk_gambar']['tmp_name']= $files['produk_gambar']['tmp_name'][$i];
				        $_FILES['produk_gambar']['error']= $files['produk_gambar']['error'][$i];
				        $_FILES['produk_gambar']['size']= $files['produk_gambar']['size'][$i];    

				        $this->upload->initialize($this->set_upload_options($i, $user_id));
				        $this->upload->do_upload('produk_gambar');

				        $this->load->model('image_produk_model');
						$upload = $this->upload->data();

						//resizing image
						$image = $upload['file_name'];
						$location = 'assets/store/product/'.$user_id.'/';
						//$this->_resize($image,$location);

						//insert to db
						$product_image = array(
							'store_id' => $store_id,
							'store_produk_id' => $product_id,
							'name' => $image,
							'created_date' => date('Y-m-d'),
							);
						$this->image_produk_model->insert($product_image);
				    }
				
			}
			//abis itu redirect ke list produk
			redirect(base_url().'products/product_index/list_product');
		}
		else{
			//jika ada data post sebelumnya
			$post = $this->input->post();
			if(!empty($post)){

				//save to db
				//name, deskripsi, store_id, harga, stok, created_date, berat, berat_tipe
				$product = array(
					'name' => $post['produk_name'],
					'deskripsi' => $post['deskripsi'],
					'kategori_id' => $post['produk_category'],
					'store_id' => $store_id,
					//'store_etalase_id' => ,
					'harga' => $post['harga'],
					'berat' => $post['berat'],
					'berat_tipe' => $post['berat_type'],
					'asuransi' => $post['asuransi'],
					'kondisi' => $post['kondisi'],
					'pengembalian' => $post['pengembalian'],
					'stok' => $post['stok'],
					'minimum_order' => $post['minimum_order'],
					'created_date' => date('Y-m-d')
				);

				$this->store_produk_model->insert($product);

				$product_id = $this->db->insert_id();

				$input_field = 'produk_gambar';

				if($this->file_uploaded($input_field)){
					//check directory
						$this->load->library('general');
						$this->load->library('upload');

						$this->general->directory_check('assets/store/product/'.$user_id);

						$files = $_FILES;
	    				$cpt = count($_FILES['produk_gambar']['name']);
	    				for($i=0; $i<$cpt; $i++)
					    {           
					        $_FILES['produk_gambar']['name']= $files['produk_gambar']['name'][$i];
					        $_FILES['produk_gambar']['type']= $files['produk_gambar']['type'][$i];
					        $_FILES['produk_gambar']['tmp_name']= $files['produk_gambar']['tmp_name'][$i];
					        $_FILES['produk_gambar']['error']= $files['produk_gambar']['error'][$i];
					        $_FILES['produk_gambar']['size']= $files['produk_gambar']['size'][$i];    

					        $this->upload->initialize($this->set_upload_options($i, $user_id));
					        $this->upload->do_upload('produk_gambar');

					        $this->load->model('image_produk_model');
							$upload = $this->upload->data();

							//resizing image
							$image = $upload['file_name'];
							$location = 'assets/store/product/'.$user_id.'/';
							//$this->_resize($image,$location);

							//insert to db
							$product_image = array(
								'store_id' => $store_id,
								'store_produk_id' => $product_id,
								'name' => $image,
								'created_date' => date('Y-m-d'),
								);
							$this->image_produk_model->insert($product_image);
					    }
					
				}

				redirect(base_url().'products/product_index/add');
			}

			$data = array();
			$data_sidebar = array();
			$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);
			$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
			$data['category'] = $this->list_category();
			$this->render('products/product_add', $data, 'components/backend');
		}	
	}

	public function del($id){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();

		$this->load->model(array('image_produk_model','store_produk_model'));

		//delete fisik image
		
		$where = array('id' => array('where' => $id));
		$this->store_produk_model->set_param($where);
		$data = $this->store_produk_model->get_one();

		$where2 = array('store_id' => array('where' => $data->store_id), 'store_produk_id' => array('where' => $data->id));
		$this->image_produk_model->set_param($where2);
		$this->image_produk_model->delete();
		$this->store_produk_model->set_param($where);
		$this->store_produk_model->delete();

		redirect('products/product_index/list_product');		
	}

	public function set_upload_options($i, $user_id){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();

		//upload an image options		
	    $file_name = time().'_'.$i;
		$ext = strtolower(end(explode(".", $_FILES['produk_gambar']['name'][$i])));

		$config['upload_path'] = 'assets/store/product/'.$user_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 3000;
		$config['file_name'] = $file_name.$ext;

		return $config;
	}

	public function list_category(){
		$this->user = modules::load('user/user_index');
		$this->user->check_login();

		$this->load->model('kategori_model');
		$where = array('status' => array('where'=>'y'));
		$this->kategori_model->set_param($where);
		return $this->kategori_model->get_all();
	}

	public function list_category_json(){
		$this->load->model('store_produk_model');
		$produk = $this->store_produk_model->get_detail(35);
		$this->debug($produk);
	}

	private function _resize($image, $location){
		//load module
		$this->bp_image = modules::load('components/bp_image');

		$config['image_library'] = $this->config->item('image_library');
		$config['source_image'] = $location.$image;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']	= 620;
		$config['height']	= 350;

		//check width & height image first
		$width = $this->bp_image->get_width();
		$height = $this->bp_image->get_height();

		if($height > 350){
			//resize ke 350
			$this->bp_image->fit_to_height(350);
		}else if($height < 300){
			return FALSE;
		}

		return TRUE;
	}

	public function quick_view(){
		$data = array();
		$id_product = $this->uri->segment(4);
		$this->load->model('store_produk_model');
		$produk = $this->store_produk_model->get_detail($id_product);
		$data['produk'] = $produk;
		$this->load->view('products/product_quick_view', $data);
	}

	public function quick_view2(){
		$data = array();
		$id_product = $this->uri->segment(4);
		$this->load->model('store_produk_model');

		//counter visitor view
		$this->visitor_view_counter($id_product);

		$produk = $this->store_produk_model->get_detail($id_product);
		$data['produk'] = $produk;
		$this->load->view('products/product_quick_view2', $data);
	}

	public function detail($id_store_product){
		$this->load->model(array('store_produk_model','diskusi_model','review_model'));
		$data = array();
		$footer_data = array();
		$header_data['user'] = $this->session->userdata('name');
		
		//counter visitor view
		$this->visitor_view_counter($id_store_product);

		//passing variable to view
		$data = array(
			'produk' => $this->store_produk_model->get_detail($id_store_product),
			'review' => $this->review_model->get_review($id_store_product),
			'diskusi' => $this->diskusi_model->get_diskusi($id_store_product),
			'toko' => $this->store_model->store_detail($id_store_product)
		);
		
		$data['header'] = $this->load->view('frontpage/components/header', $header_data, TRUE);
		$data['footer'] = $this->load->view('frontpage/components/footer', $footer_data, TRUE);
		$this->render('frontpage/produk_detail', $data, 'components/frontend2');
	}

	function visitor_view_counter($id_store_product){
		//jika bukan admin toko yang lihat
		$this->load->model('store_produk_model');
		$store_detail = $this->store_produk_model->get_store_by_product($id_store_product);
		
		if($store_detail->id != $this->session->userdata('store_id')){
			$data = $this->store_produk_model->get_visitor_view($id_store_product);
			$update = $data->visitor_view+1;

			$where = array('id' => array('where' => $id_store_product));
			$this->store_produk_model->set_param($where);
			$data_update = array('visitor_view' => $update);
			$this->store_produk_model->update($data_update);
		}
	}

	function checkout(){
	}

	function beli(){
		//muncul popup agen kurir
		//paket pengiriman
		//ongkos kirim
		//sub total
		//alamat pengirim
		$data = array();
		$this->load->view('products/beli', $data);
	}

	function nego(){
		$data = array();
		$this->load->view('products/nego', $data);
	}	
}

/* End of file product_index.php */
/* Location: ./application/controllers/product_index.php */