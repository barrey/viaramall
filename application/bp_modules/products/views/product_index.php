<h4>Daftar Produk</h4><br>
<div class="jqgrid-content" id="produk-daftar">
	<table class="grid-container" id="newapi<?php echo $gridname;?>"></table>
	<div class="col-md-12" id="pnewapi<?php echo $gridname; ?>"></div>
</div>

<!-- MODAL -->
<div class="modal fade" id="dialog-produk" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true"  style="overflow: hidden;">
	  <div class="modal-dialog" style="overflow: hidden;">
	    <div class="modal-content">
	    </div>
	  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//jqgrid
        <?php echo $table; ?>
        <?php if(!empty($add)):?>
        	<?php echo $add; ?>
        <?php endif; ?>
        <?php echo $edit_popup; ?>
        <?php //echo $edit_status; ?>

        // set jqGrid's width onload
        var lebar_container = $('.jqgrid-content').width();
        $(".grid-container").setGridWidth(lebar_container, 'shrink');
        // end set

        function gridReload(){
	        $("#newapi<?php echo $gridname;?>").jqGrid('setGridParam',{
	                  url:"<?php echo $dataurl;?>",
	                  postData: {
	                      kode_propinsi: function() {return $("#kode-bps-propinsi").val()},
	                      nama_propinsi: function() {return $("#nama-propinsi").val()},
	                      pencarian: function() {return true;}
	                  },
	                  page:1
	              }).trigger("reloadGrid");
	    }

	    $(window).bind('resize', function() {
        var lebar_container = $('.jqgrid-content').width();
        $(".grid-container").setGridWidth(lebar_container, 'shrink');
        }).trigger('resize'); 

        $('.del').on('click', function(){
        	console.log('tes');
        });
	});
</script>