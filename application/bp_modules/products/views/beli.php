<div id="beli_view" class="modal_window">
	<button class="close arcticmodal-close"></button>
	<div class="clearfix">
		<div class="container_fluid">
			<div class="row">
				<div class="detail_beli" style="padding: 10px;">
					Alamat tujuan
					<textarea type="text" name="address" col="2"></textarea><br>
					<select name="kurir" style="width: 100px; display: inline-block;">
						<option value="jne">JNE</option>
						<option value="tiki">TIKI</option>
					</select>
					<select name="paket_pengiriman" style="width: 100px; display: inline-block; margin-left: 10px;">
						<option value="paket1">YES</option>
						<option value="paket2">OKE</option>
					</select>
					<button class="button_dark_grey middle_btn" style="display: inline-block; margin-left: 20px;" id="beli_kirim">Beli</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#beli_kirim').on('click', function(){
			$('#beli_view').arcticmodal('close');
		});
	});
</script>