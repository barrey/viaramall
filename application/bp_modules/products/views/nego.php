<div id="nego_view" class="small_window">
	<button class="close arcticmodal-close"></button>
	<div class="clearfix">
		<div class="nego_offer" style="max-width: 500px;">
			<input type="text" name="nego" placeholder="Nego untuk produk ini" style="width: 200px;">
			<button class="button_dark_grey middle_btn" style="display: inline-block; margin-left: 20px; width: 220px;" id="nego_kirim">Kirim penawaran nego</button>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){

		$('#nego_kirim').on('click', function(){
			$('#nego_view').arcticmodal('close');
		});
	});
</script>