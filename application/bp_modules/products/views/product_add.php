<div class="row">
        <h4 style="padding-left: 10px;">Tambah Produk</h4>
    <?php echo form_open_multipart("products/product_index/add/",array('name'=>'formItem','id'=>'form-product','method'=>'POST', 'class'=>"form-horizontal"));?>
    <div class="col-md-12 m-top-40">
        <div class="form-group">
            <label class="control-label col-sm-3">Nama Produk</label>
            <div class="col-sm-9">
                    <input type="text" class="form-control" name="produk_name"><br />
                    <?php echo form_error('produk_name');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Kategori</label>
            <div class="col-sm-9">
                    <select class="" name="produk_category" id="category">
                    	<option value="">Pilih Kategori</option>
                    	<?php if(!empty($category)):?>
                    		<?php foreach($category as $c): ?>
                    			<option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                    		<?php endforeach; ?>
                    	<?php endif; ?>
                    </select>
                    <?php echo form_error('produk_category');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Pemesanan Minimum/ Buah</label>
            <div class="col-sm-9">
                    <input type="text" class="angka"  name="minimum_order"><br />
                    <?php echo form_error('minimum_order');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Stok</label>
            <div class="col-sm-9">
                    <input type="text" class="angka"  name="stok"><br />
                    <?php echo form_error('stok');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Berat</label>
            <div class="col-sm-9">
            		<select name="berat_type" required="required">
            			<option value="">Pilih jenis</option>
            			<option value="kg">Kilogram (Kg)</option>
            			<option value="g">Gram (g)</option>
            		</select>
                    <input type="text" class="angka"  name="berat"><br />
                    <?php echo form_error('berat');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Harga</label>
            <div class="col-sm-9">
           			<input type="text" class="angka" name="harga">
                    <?php echo form_error('harga');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Gambar</label>
            <div class="col-sm-9">
                    <input type="file" class=""  name="produk_gambar[]"  multiple="" style="display: inline;"><br />
                    <?php echo form_error('produk_gambar');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Asuransi</label>
            <div class="col-sm-9">
                    <select name="asuransi" id="asuransi">
                    	<option value="">Pilih kondisi</option>
                    	<option value="y">Ya</option>
                    	<option value="n">Tidak</option>
                    </select>
                    <?php echo form_error('asuransi');?>
            </div>
        </div>
        <!--
        <div class="form-group">
            <label class="control-label col-sm-3">Tambahkan ke Etalase</label>
            <div class="col-sm-9">
                    <input type="text" class="form-control"  name="name"><br />
                    <?php echo form_error('name');?>
            </div>
        </div>
        -->
        <div class="form-group">
            <label class="control-label col-sm-3">Kondisi</label>
            <div class="col-sm-9">
                    <select name="kondisi" required="required">
                    	<option value="">Pilih Kondisi</option>
                    	<option value="baru">Baru</option>
                    	<option value="bekas">Bekas</option>
                    </select>
                    <?php echo form_error('kondisi');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Pengembalian Produk</label>
            <div class="col-sm-9">
                    <select name="pengembalian" required="required">
                    	<option value="">Pilih Kondisi</option>
                    	<option value="ya">Ya</option>
                    	<option value="tidak">Tidak</option>
                    </select><br />
                    <?php echo form_error('pengembalian');?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Deskripsi Produk</label>
            <div class="col-sm-9">
                    <textarea class="form-control"  name="deskripsi"></textarea><br />
                    <?php echo form_error('deskripsi');?>
            </div>
        </div>
        <div class="form-group m-top-40">
            <label class="control-label col-sm-3"></label>
            <div class="col-sm-9">
                    <button class="btn btn-primary">Simpan</button> &nbsp;  &nbsp; 
                    <button class="btn btn-info" id="add_new">Simpan dan tambah baru</button> &nbsp;  &nbsp; 
                    <button class="btn btn-warning">Batal</button>
                    <input type="hidden" name="submit_only" value="submit_only">
            </div>
        </div>
    </div>
    <?php echo form_close();?>
</div>


<script type="text/javascript">
	$(document).ready(function(){
			$('.angka').numeric();
			$('#category').select2();
			$('#add_new').on('click', function(e){
				e.preventDefault();
				$('input[name="submit_only"]').val('');
				$('form').submit();
			});
	});
</script>