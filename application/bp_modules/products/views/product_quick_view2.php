<!--==================================
		Quick view modal window
======================================-->

<div id="quick_view" class="modal_window">

	<button class="close arcticmodal-close"></button>

	<div class="clearfix">

		<!-- - - - - - - - - - - - - - Product image column - - - - - - - - - - - - - - - - -->

		<div class="single_product">

			<!-- - - - - - - - - - - - - - Image preview container - - - - - - - - - - - - - - - - -->

			<div class="image_preview_container" id="qv_preview">

				<!--<img id="img_zoom" data-zoom-image="images/qv_large_1.JPG" src="images/qv_img_1.jpg" alt="">-->
				<img src="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $produk[0]->product_img; ?>" data-large-image="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $produk[0]->product_img; ?>" alt="">

			</div><!--/ .image_preview_container-->

			<!-- - - - - - - - - - - - - - End of image preview container - - - - - - - - - - - - - - - - -->


			<!-- - - - - - - - - - - - - - Prodcut thumbs carousel - - - - - - - - - - - - - - - - -->
			
			<div class="product_preview" data-output="#qv_preview">

				<div class="owl_carousel" id="thumbnails">
					<?php if(!empty($produk[0]->product_img)):?>
						<?php foreach($produk as $p):?>
							<?php if($p->product_img != ''):?>
								<img src="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $p->product_img; ?>" data-large-image="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $p->product_img; ?>" alt="">
							<?php endif; ?>
						<?php endforeach;?>
					<?php endif; ?>
					
				</div><!--/ .owl-carousel-->

			</div><!--/ .product_preview-->
			
			<!-- - - - - - - - - - - - - - End of prodcut thumbs carousel - - - - - - - - - - - - - - - - -->


			<!-- - - - - - - - - - - - - - Share - - - - - - - - - - - - - - - - -->
			
			<!--<div class="v_centered">

				<span class="title">Share this:</span>

				<div class="addthis_widget_container">
					<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_preferred_1"></a>
					<a class="addthis_button_preferred_2"></a>
					<a class="addthis_button_preferred_3"></a>
					<a class="addthis_button_preferred_4"></a>
					<a class="addthis_button_compact"></a>
					<a class="addthis_counter addthis_bubble_style"></a>
					</div>
				</div>
				
			</div>-->
			
			<!-- - - - - - - - - - - - - - End of share - - - - - - - - - - - - - - - - -->

		</div>

		<!-- - - - - - - - - - - - - - End of product image column - - - - - - - - - - - - - - - - -->

		<!-- - - - - - - - - - - - - - Product description column - - - - - - - - - - - - - - - - -->

		<div class="single_product_description">

			<h3><a href="#"><?php echo ucwords($produk[0]->name); ?></a></h3>

			<div class="description_section v_centered">

				<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->
			
				<ul class="rating">

					<li class="active"></li>
					<li class="active"></li>
					<li class="active"></li>
					<li></li>
					<li></li>

				</ul>
					
				<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

				<ul class="topbar">

					<li><a href="#">3 Review(s)</a></li>
					<li><a href="#">Diskusi Produk</a></li>

				</ul>

				<!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

			</div>

			<div class="description_section">

				<table class="product_info">

					<tbody>

						<tr>

							<td>Toko  </td>
							<td>: <a href="#">Chanel</a></td>

						</tr>

						<tr>

							<td>Availability </td>
							<td>: <span class="in_stock">in stock</span> 20 item(s)</td>

						</tr>

					</tbody>

				</table>

			</div>

			<hr>

			<div class="description_section">

				<p><?php echo ucfirst($produk[0]->deskripsi); ?></p>

			</div>
			
			<hr>
			<div class="description_section">
				<p>Kondisi <?php echo $produk[0]->kondisi; ?></p>
			</div>
			<hr>


			<span style="display: inline-block; margin-right: 20px;">Harga barang </span><span class="product_price">Rp <?php echo number_format($produk[0]->harga, 0, ',','.'); ?></span>
			<input type="hidden" name="harga_satuan" id="harga_satuan" value="<?php echo $produk[0]->harga; ?>">

			<!-- - - - - - - - - - - - - - Product size - - - - - - - - - - - - - - - - -->

			<!--<div class="description_section_2 v_centered">
				
				<span class="title">Size:</span>

				<div class="custom_select min">

					<select>

						<option value="Small">Small</option>
						<option value="Middle">Middle</option>
						<option value="Big">Big</option>

					</select>

				</div>

			</div>-->

			<!-- - - - - - - - - - - - - - End of product size - - - - - - - - - - - - - - - - -->

			<!-- - - - - - - - - - - - - - Quantity - - - - - - - - - - - - - - - - -->
			<br>
			<br>
			<div class="description_section_2 v_centered">
				
				<span class="title">Jumlah pembelian :</span>

				<div class="qty min clearfix">

					<button class="theme_button calculate" data-direction="minus">&#45;</button>
					<input type="text" name="jumlah" value="1" id="jumlah">
					<button class="theme_button calculate" data-direction="plus">&#43;</button>

				</div>

			</div>

			<!-- - - - - - - - - - - - - - End of quantity - - - - - - - - - - - - - - - - -->

			<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

			<div class="buttons_row">

				<button class="button_blue middle_btn">Beli</button>

				<!--<button class="button_dark_grey def_icon_btn middle_btn add_to_wishlist tooltip_container">-->
				<!--<span class="tooltip top">Tambahkan ke Wishlist</span></button>-->

				<!--<button class="button_dark_grey def_icon_btn middle_btn add_to_compare tooltip_container">
				<span class="tooltip top">Add to Compare</span></button> -->

			</div>

			<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

		</div>

		<!-- - - - - - - - - - - - - - End of product description column - - - - - - - - - - - - - - - - --
>

	</div>

</div>

<!--==================================
		End quick view modal window
====================================== -->
<script type="text/javascript">
var harga = $('#harga_satuan').val();

$('.calculate').on('click', function(){
	operasi = $(this).data('direction');
	if(operasi == 'plus'){
		jml = $(this).parent().find($('#jumlah')).val();
		jml2 = parseInt(jml) + 1;
	}else{
		jml = $(this).parent().find($('#jumlah')).val();
		jml2 = parseInt(jml) - 1;
	}
	total = harga * parseInt(jml2);
	if(total <= 0){
		alert('Ups, harap perbaiki jumlah pembelian');
		return false;
	}
	$(this).parent().parent().parent().find('.product_price').text('Rp '+total.format(0, 3, '.', ','));
});

$('#jumlah').on('change', function(){
	jml = $(this).val();
	total = harga * parseInt(jml);
	if(total <= 0){
		alert('Ups, harap perbaiki jumlah pembelian');
		$(this).val('1');
		$(this).parent().parent().parent().find('.product_price').text('Rp '+parseInt(harga).format(0, 3, '.', ','));
		return false;
	}
	$('.product_price').text('Rp '+total.format(0, 3, '.', ','));
});

</script>