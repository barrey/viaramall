		<!-- - - - - - - - - - - - - - Product image column - - - - - - - - - - - - - - - - -->

		<div class="single_product">

			<!-- - - - - - - - - - - - - - Image preview container - - - - - - - - - - - - - - - - -->

			<div id="qv_preview" class="image_preview_container">

				<img alt="" src="<?php echo base_url()?>assets/store/product/<?php echo $produk[0]->seller_id; ?>/<?php echo $produk[0]->product_img; ?>" data-zoom-image="images/qv_large_1.JPG" id="img_zoom" style="max-width: 360x;">

			</div><!--/ .image_preview_container-->

			<!-- - - - - - - - - - - - - - End of image preview container - - - - - - - - - - - - - - - - -->

			<!-- - - - - - - - - - - - - - Prodcut thumbs carousel - - - - - - - - - - - - - - - - -->
			
			<div data-output="#qv_preview" class="product_preview">
				<div class="row">
						<?php 
							$count_img_product = count($produk);
							if($count_img_product > 1):
								$a = 1;
								for ($i=1; $i < $count_img_product; $i++):
						?>
							<div class="col-md-4" style="margin: 0; padding: 5px;">
								<img alt="" src="<?php echo base_url()?>assets/store/product/<?php echo $produk[$i]->seller_id; ?>/<?php echo $produk[$i]->product_img; ?>" data-zoom-image="images/qv_large_1.JPG" id="img_zoom">
							</div>
						<?php endfor; endif; ?>
				</div>
			
			</div><!--/ .product_preview-->
			
			<!-- - - - - - - - - - - - - - End of prodcut thumbs carousel - - - - - - - - - - - - - - - - -->

			<!-- - - - - - - - - - - - - - Share - - - - - - - - - - - - - - - - -->
			
			<div class="v_centered">
				
			</div>
			
			<!-- - - - - - - - - - - - - - End of share - - - - - - - - - - - - - - - - -->

		</div>

		<!-- - - - - - - - - - - - - - End of product image column - - - - - - - - - - - - - - - - -->

		<!-- - - - - - - - - - - - - - Product description column - - - - - - - - - - - - - - - - -->

		<div class="single_product_description">

			<h3><a href="#"><?php echo $produk[0]->name; ?></a></h3>

			<div class="description_section v_centered">

				<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->
			
				<!--<ul class="rating">

					<li class="active"></li>
					<li class="active"></li>
					<li class="active"></li>
					<li></li>
					<li></li>

				</ul>-->
					
				<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->
				
				<!--
				<ul class="topbar">

					<li><a href="#">3 Review(s)</a></li>
					<li><a href="#">Add Your Review</a></li>

				</ul>
				-->

				<!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

			</div>

			<div class="description_section">
				<!--
				<table class="product_info">

					<tbody>

						<tr>

							<td>Manufacturer: </td>
							<td><a href="#">Chanel</a></td>

						</tr>

						<tr>

							<td>Availability: </td>
							<td><span class="in_stock">in stock</span> 20 item(s)</td>

						</tr>

						<tr>

							<td>Product Code: </td>
							<td>PS06</td>

						</tr>

					</tbody>

				</table>-->

			</div>

			<hr>

			<div class="description_section">

				<p><?php echo $produk[0]->deskripsi; ?></p>

			</div>
			
			<hr>
			<p>Kondisi <?php echo $produk[0]->kondisi; ?></p>
			<hr>

			<p class="product_price"><b class="theme_color">Rp <?php echo number_format($produk[0]->harga, 0, ',','.'); ?></b></p>

			<!-- - - - - - - - - - - - - - Product size - - - - - - - - - - - - - - - - -->
			<div class="description_section_2 v_centered">
				
				<span class="title">Jumlah</span>

				<div class="qty min clearfix">

					<!--<button data-direction="minus" class="theme_button">-</button>-->
					<input type="text" value="1" name="jumlah" class="angka">
					<!--<button data-direction="plus" class="theme_button">+</button>-->

				</div>

			</div>

			<div class="description_section_2 v_centered">
				
				<span class="title">Keterangan</span>

					<textarea name="keterangan" id="keterangan" cols="5" rows="5" style="width: 90%; display: block; margin-left: 0;"></textarea>


			</div>

			<!-- - - - - - - - - - - - - - End of product size - - - - - - - - - - - - - - - - -->

			<!-- - - - - - - - - - - - - - Quantity - - - - - - - - - - - - - - - - -->

			

			<!-- - - - - - - - - - - - - - End of quantity - - - - - - - - - - - - - - - - -->

			<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

			<div class="buttons_row">

				<a href="<?php echo base_url()?>products/product_index/detail" class="button_blue middle_btn add-product" data-produk="<?php echo $produk[0]->id; ?>" >Beli</a>
				<!--
				<button class="button_dark_grey def_icon_btn middle_btn add_to_wishlist tooltip_container"><span class="tooltip top">Add to Wishlist</span></button>

				<button class="button_dark_grey def_icon_btn middle_btn add_to_compare tooltip_container"><span class="tooltip top">Add to Compare</span></button>
				-->

			</div>

			<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

		</div>

		<!-- - - - - - - - - - - - - - End of product description column - - - - - - - - - - - - - - - - -->


<script>
	$(document).ready(function(){
		$('.angka').numeric();
	});
</script>