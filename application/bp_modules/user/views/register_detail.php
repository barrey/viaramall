<main class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2 m_bot_100">
<form action="<?php echo base_url()?>auth/register/register_detail" id="user-detail" method="POST">
	<div class="section_offset">

		<header class="top_box">

			<div class="buttons_row">

				<span>Satu langkah lagi	untuk kenyamanan berbelanja anda.</span>

			</div>

		</header>

		<div class="table_wrap">
				<table class="table_type_1 order_table">

					<tbody>

						<tr>
							
							<th>Saya</th>

							<td>
								<div class="col-md-3 col-sm-6">
								<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
								<select name="jk" required="required">
									<option value="m">Pria</option>
									<option value="f">Wanita</option>
								</select>
								</div>
							</td>

						</tr>

						<tr>
							
							<th>Tanggal lahir</th>

							<td>
								<div class="col-md-4 col-lg-4">
									<input type="text" class="datepicker" name="birth_date" required="required">
								</div>
							</td>

						</tr>

						<tr>
							
							<th>Alamat</th>

							<td>
								<div class="col-md-3 col-sm-5">
									<p class="lbl_register">Propinsi</p>
									<p class="lbl_register">Kota</p>
									<p class="lbl_register">Kecamatan</p>
								</div>
								<div class="col-md-4 col-sm-6">
									<select name="province" class="m_bot_10 select2 province" required="required">
										<option value="">Pilih Propinsi</option>
										<?php foreach($province as $p):?>
											<option value="<?php echo $p->id; ?>"><?php echo $p->name; ?></option>
										<?php endforeach;?>
									</select><br><br>
									<select name="regency" class="m_bot_10 select2 regency" required="required"></select><br><br>
									<select name="subdistrict" class="m_bot_10 select2 subdistrict" required="required"></select><br><br>
								</div>
								<div class="col-md-8 col-lg-8 col-md-offset-3 col-lg-offset-3">
									<textarea col="3" name="alamat" required="required"></textarea>
								</div>
							</td>

						</tr>

						<tr>
							
							<th>No. Telp/HP</th>

							<td>
								<div class="col-md-4 col-lg-4">
									<input type="text" name="hp_no" class="angka" required="required"></td>
								</div>

						</tr>


					</tbody>
				</table>
			
		</div>

	</div>
	<div class="section_offset">
		<a href="<?php echo base_url();?>" class="button_dark_grey middle_btn">Lewati</a>
		<input type="submit" href="#" id="saveUserDetail" class="button_blue middle_btn f_right" value="Simpan">
	</div>
	</form>
</main>
<script>
	$(document).ready(function(){
		$('.datepicker').datepicker({
			dateFormat: "yy-mm-dd"
		});
		$('.angka').numeric();
		$('.select2').select2();

		var $province = $(".province");
		var $regency = $(".regency");
		
		$province.on('select2:select', function(e){
			console.log($(this).val());
			id = $(this).val();
			$.ajax({
				url: '<?php echo base_url()?>components/bp_data/get_regency_json_select2/'+id,
				type: 'POST',
				dataType: 'json',
				data: {id_bp_province: $(this).val()},
			})
			.done(function(response) {
				$('.regency').html('').select2({data: {id:null, text: null}});
				$('.regency').select2({data: response});
			});
			
		});

		$regency.on('select2:select', function(e){
			id = $(this).val();
			$.ajax({
				url: '<?php echo base_url()?>components/bp_data/get_subdistrict_json_select2/'+id,
				type: 'POST',
				dataType: 'json',
				data: {id_bp_province: $(this).val()},
			})
			.done(function(response) {
				$('.subdistrict').html('').select2({data: {id:null, text: null}});
				$('.subdistrict').select2({data: response});
			});
		});


//$('#user-detail').validate();
/*
		$('#saveUserDetail').on('click', function(e){
			e.preventDefault();
			$('#user-detail').validate();
			$.ajax({
				url: '<?php echo base_url()?>',
				type: 'POST',
				dataType: 'json',
				data: $('form').serialize(),
			})
			.done(function() {
				console.log("success");
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log($('form').serialize());
			});
			
			return false;
		});
*/
	});
</script>