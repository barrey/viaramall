<div class="secondary_page_wrapper">

				<div class="container">

					<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

					<ul class="breadcrumbs">

						<li><a href="index.html">Home</a></li>
						<li>Checkout</li>

					</ul>

					<h1 class="page_title">Checkout</h1>

					<!-- - - - - - - - - - - - - - Checkout method - - - - - - - - - - - - - - - - -->

					<section class="section_offset">

						<h3 class="offset_title">1. Checkout Method</h3>

						<div class="relative">

							<a href="#" class="icon_btn button_dark_grey edit_button"><i class="icon-pencil"></i></a>


							<div class="table_layout">

								<div class="table_row">

									<div class="table_cell">

										<section>

											<h4>Checkout as a Guest or Register</h4>

											<p class="subcaption">Register with us for future convenience:</p>

											<form>

												<ul>
													
													<li>

														<input type="radio" id="radio_button_1" name="radio_2" checked="">
														<label for="radio_button_1">Checkout as Guest</label>

													</li>

													<li>

														<input type="radio" id="radio_button_2" name="radio_2">
														<label for="radio_button_2">Register</label>

													</li>

												</ul>

											</form>

											<h5 class="sub bold">Register and save time!</h5>

											<p class="subcaption">Register with us for future convenience:</p>

											<ul class="list_type_7">

												<li>Fast and easy check out</li>
												<li>Easy access to your order history and status</li>

											</ul>

										</section>

									</div><!--/ .table_cell -->

									<div class="table_cell">

										<section>

											<h4>Login</h4>

											<p class="subcaption">Already registered? Please log in below:</p>

											<form class="type_2" id="login_form">

												<ul>

													<li class="row">

														<div class="col-xs-12">

															<label class="required" for="login_email">Email address</label>
															<input type="email" id="login_email" name="">

														</div>

													</li>

													<li class="row">

														<div class="col-xs-12">

															<label class="required" for="login_password">Password</label>
															<input type="password" id="login_password" name="">

														</div>

													</li>

													<li class="row">

														<div class="col-xs-12">

															<div class="on_the_sides">

																<div class="left_side">

																	<a class="small_link" href="#">Forgot your password?</a>

																</div>

																<div class="right_side">

																	<span class="prompt">Required Fields</span>

																</div>

															</div>

														</div>

													</li>

												</ul>

											</form>

										</section>

									</div><!--/ .table_cell -->

								</div><!--/ .table_row -->

								<div class="table_row">

									<div class="table_cell">

										<a class="button_blue middle_btn" href="#">Continue</a>

									</div><!--/ .table_cell -->

									<div class="table_cell">

										<div class="on_the_sides login_with">

											<div class="left_side">

												<button class="button_blue middle_btn" form="login_form" type="submit">Login</button>

											</div>

											<div class="right_side v_centered">

												<h4>OR Log In With</h4>

												<ul class="horizontal_list social_btns">

													<li><a class="icon_btn middle_btn social_facebook tooltip_container" href="#"><i class="icon-facebook-1"></i><span class="tooltip top">Facebook</span></a></li>

													<li><a class="icon_btn middle_btn social_twitter tooltip_container" href="#"><i class="icon-twitter"></i><span class="tooltip top">Twitter</span></a></li>

													<li><a class="icon_btn middle_btn social_googleplus tooltip_container" href="#"><i class="icon-gplus-2"></i><span class="tooltip top">GooglePlus</span></a></li>

												</ul><!--/ .horizontal_list.social_btns-->

											</div>

										</div>

									</div><!--/ .table_cell -->

								</div><!--/ .table_row -->

							</div><!--/ .table_layout -->

						</div><!--/ .relative -->

					</section><!--/ .section_offset -->

					<!-- - - - - - - - - - - - - - End of checkout method - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Billing information - - - - - - - - - - - - - - - - -->

					<section class="section_offset">

						<h3>2. Billing Information</h3>

						<div class="theme_box">

							<a href="#" class="icon_btn button_dark_grey edit_button"><i class="icon-pencil"></i></a>

							<form class="type_2">
								
								<ul>
									
									<li class="row">
										
										<div class="col-sm-6">
											
											<label class="required" for="first_name">First Name</label>
											<input type="text" id="first_name" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">
											
											<label class="required" for="last_name">Last Name</label>
											<input type="text" id="last_name" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">
										
										<div class="col-sm-6">
											
											<label for="company_name">Company Name</label>
											<input type="text" id="company_name" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">
											
											<label class="required" for="email_address">Email Address</label>
											<input type="text" id="email_address" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">	

										<div class="col-xs-12">

											<label class="required" for="address">Address</label>
											<input type="text" id="address" name="">

										</div><!--/ [col] -->

									</li><!-- / .row -->

									<li class="row">

										<div class="col-xs-12">

											<input type="text" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">
											
											<label class="required" for="city">City</label>
											<input type="text" id="city" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label class="required">State/Province</label>

											<div class="custom_select"><div class="active_option open_select">Alabama</div><ul class="options_list dropdown"><li class="animated_item" style="transition-delay:0.1s"><a href="#">Alabama</a></li><li class="animated_item" style="transition-delay:0.2s"><a href="#">Illinois</a></li><li class="animated_item" style="transition-delay:0.3s"><a href="#">Kansas</a></li></ul>

												<select name="" style="display: none;">

													<option value="Alabama">Alabama</option>
													<option value="Illinois">Illinois</option>
													<option value="Kansas">Kansas</option>

												</select>

											</div>

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">

											<label class="required" for="postal_code">Zip/Postal Code</label>
											<input type="text" id="postal_code" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label class="required">Country</label>

											<div class="custom_select"><div class="active_option open_select">USA</div><ul class="options_list dropdown"><li class="animated_item" style="transition-delay:0.1s"><a href="#">USA</a></li><li class="animated_item" style="transition-delay:0.2s"><a href="#">Australia</a></li><li class="animated_item" style="transition-delay:0.3s"><a href="#">Austria</a></li><li class="animated_item" style="transition-delay:0.4s"><a href="#">Argentina</a></li><li class="animated_item" style="transition-delay:0.5s"><a href="#">Canada</a></li></ul>

												<select name="" style="display: none;">
													
													<option value="USA">USA</option>
													<option value="Australia">Australia</option>
													<option value="Austria">Austria</option>
													<option value="Argentina">Argentina</option>
													<option value="Canada">Canada</option>

												</select>

											</div>

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">

											<label class="required" for="telephone">Telephone</label>
											<input type="text" id="telephone" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label for="fax">Fax</label>
											<input type="text" id="fax" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">

											<label class="required" for="password">Password</label>
											<input type="password" id="password" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label class="required" for="confirm">Confirm Password</label>
											<input type="password" id="confirm" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-xs-12">

											<input type="radio" id="radio_1" name="ship" checked="">
											<label for="radio_1">Ship to this address</label>

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-xs-12">

											<input type="radio" id="radio_2" name="ship">
											<label for="radio_2">Ship to different address</label>

										</div>

									</li><!--/ .row -->

								</ul>

							</form>

						</div>

						<footer class="bottom_box on_the_sides">

							<div class="left_side">

								<a class="button_blue middle_btn" href="#">Continue</a>

							</div>

							<div class="right_side">

								<span class="prompt">Required Fields</span>

							</div>

						</footer>

					</section><!--/ .section_offset -->

					<!-- - - - - - - - - - - - - - End of billing information - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Shipping information - - - - - - - - - - - - - - - - -->

					<section class="section_offset">

						<h3>3. Shipping Information</h3>

						<div class="theme_box">

							<a href="#" class="icon_btn button_dark_grey edit_button"><i class="icon-pencil"></i></a>

							<form class="type_2">
								
								<ul>
									
									<li class="row">
										
										<div class="col-sm-6">
											
											<label class="required" for="first_name_1">First Name</label>
											<input type="text" id="first_name_1" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">
											
											<label class="required" for="last_name_1">Last Name</label>
											<input type="text" id="last_name_1" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">
										
										<div class="col-sm-6">
											
											<label for="company_name_1">Company Name</label>
											<input type="text" id="company_name_1" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">
											
											<label class="required" for="email_address_1">Email Address</label>
											<input type="text" id="email_address_1" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-xs-12">

											<label class="required" for="address_1">Address</label>
											<input type="text" id="address_1" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-xs-12">

											<input type="text" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">
											
											<label class="required" for="city_1">City</label>
											<input type="text" id="city_1" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label class="required">State/Province</label>

											<div class="custom_select"><div class="active_option open_select">Alabama</div><ul class="options_list dropdown"><li class="animated_item" style="transition-delay:0.1s"><a href="#">Alabama</a></li><li class="animated_item" style="transition-delay:0.2s"><a href="#">Illinois</a></li><li class="animated_item" style="transition-delay:0.3s"><a href="#">Kansas</a></li></ul>

												<select name="" style="display: none;">

													<option value="Alabama">Alabama</option>
													<option value="Illinois">Illinois</option>
													<option value="Kansas">Kansas</option>

												</select>

											</div>

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">

											<label class="required" for="postal_code_1">Zip/Postal Code</label>
											<input type="text" id="postal_code_1" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label class="required">Country</label>

											<div class="custom_select"><div class="active_option open_select">USA</div><ul class="options_list dropdown"><li class="animated_item" style="transition-delay:0.1s"><a href="#">USA</a></li><li class="animated_item" style="transition-delay:0.2s"><a href="#">Australia</a></li><li class="animated_item" style="transition-delay:0.3s"><a href="#">Austria</a></li><li class="animated_item" style="transition-delay:0.4s"><a href="#">Argentina</a></li><li class="animated_item" style="transition-delay:0.5s"><a href="#">Canada</a></li></ul>

												<select name="" style="display: none;">
													
													<option value="USA">USA</option>
													<option value="Australia">Australia</option>
													<option value="Austria">Austria</option>
													<option value="Argentina">Argentina</option>
													<option value="Canada">Canada</option>

												</select>

											</div>

										</div><!--/ [col] -->

									</li><!--/ .row -->

									<li class="row">

										<div class="col-sm-6">

											<label class="required" for="telephone_1">Telephone</label>
											<input type="text" id="telephone_1" name="">

										</div><!--/ [col] -->

										<div class="col-sm-6">

											<label for="fax_1">Fax</label>
											<input type="text" id="fax_1" name="">

										</div><!--/ [col] -->

									</li><!--/ .row -->

								</ul>

							</form>

							<a class="button_grey middle_btn" href="#">Use Billing Address</a>

						</div>

						<footer class="bottom_box on_the_sides">

							<div class="left_side buttons_row">

								<a class="button_grey middle_btn" href="#">Back</a>

								<a class="button_blue middle_btn" href="#">Continue</a>

							</div>

							<div class="right_side">

								<span class="prompt">Required Fields</span>

							</div>

						</footer>

					</section><!--/ .section_offset -->

					<!-- - - - - - - - - - - - - - End of billing information - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Shipping method - - - - - - - - - - - - - - - - -->

					<section class="section_offset">

						<h3>4. Shipping Method</h3>

						<div class="theme_box">

							<a href="#" class="icon_btn button_dark_grey edit_button"><i class="icon-pencil"></i></a>

							<ul class="shipping_method">

								<li>

									<p class="subcaption bold">Free Shipping</p>

									<input type="radio" id="radio_button_3" name="radio_3" checked="">
									<label for="radio_button_3">Free $0</label>

								</li>

								<li>

									<p class="subcaption bold">Flat Rate</p>

									<input type="radio" id="radio_button_4" name="radio_3">
									<label for="radio_button_4">Standard Shipping $5.00</label>

								</li>

							</ul>

						</div>

						<footer class="bottom_box">

							<a class="button_blue middle_btn" href="#">Continue</a>

						</footer>

					</section>


					<!-- - - - - - - - - - - - - - End of shipping method - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Payment information - - - - - - - - - - - - - - - - -->

					<section class="section_offset">

						<h3>5. Payment Information</h3>

						<div class="theme_box">

							<a href="#" class="icon_btn button_dark_grey edit_button"><i class="icon-pencil"></i></a>

							<p class="subcaption bold">Free Shipping</p>

							<ul class="simple_vertical_list">

								<li>

									<input type="radio" id="radio_button_5" name="radio_4" checked="">
									<label for="radio_button_5">Check / Money order</label>

								</li>

								<li>

									<input type="radio" id="radio_button_6" name="radio_4">
									<label for="radio_button_6">Credit card (saved)</label>

								</li>

							</ul>

						</div>

						<footer class="bottom_box">

							<a class="button_blue middle_btn" href="#">Continue</a>

						</footer>

					</section>


					<!-- - - - - - - - - - - - - - End of payment information - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Order review - - - - - - - - - - - - - - - - -->

					<section class="section_offset">

						<h3>6. Order Review</h3>

						<div class="table_wrap">

							<table class="table_type_1 order_review">

								<thead>
									
									<tr>
										
										<th class="product_title_col">Product Name</th>
										<th class="product_sku_col">SKU</th>
										<th class="product_price_col">Price</th>
										<th class="product_qty_col">Quantity</th>
										<th class="product_total_col">Total</th>

									</tr>

								</thead>

								<tbody>

									<tr>
										
										<td data-title="Product Name">
											
											<a class="product_title" href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>

											<ul class="sc_product_info">

												<li>Size: Big</li>
												<li>Color: Red</li>

											</ul>

										</td>

										<td data-title="SKU">PS01</td>

										<td class="subtotal" data-title="Price">$5.99</td>

										<td data-title="Quantity">1</td>

										<td class="total" data-title="Total">$5.99</td>

									</tr>

									<tr>
										
										<td data-title="Product Name">
											
											<a class="product_title" href="#">Sed in lacus ut enim adipiscing dictum elementum velit<br>Relief 4.25 fl oz (126ml)</a>

											<ul class="sc_product_info">

												<li>Size: Big</li>
												<li>Color: Red</li>

											</ul>

										</td>

										<td data-title="SKU">PS02</td>

										<td class="subtotal" data-title="Price">$8.99</td>

										<td data-title="Quantity">1</td>

										<td class="total" data-title="Total">$8.99</td>

									</tr>

									<tr>

										<td data-title="Product Name">
											
											<a class="product_title" href="#">Donec porta diam eu massa quisque Mint 160 ea</a>

											<ul class="sc_product_info">

												<li>Size: Big</li>
												<li>Color: Red</li>

											</ul>

										</td>

										<td data-title="SKU">PS03</td>

										<td class="subtotal" data-title="Price">$76.99</td>

										<td data-title="Quantity">1</td>

										<td class="total" data-title="Total">$76.99</td>

									</tr>

								</tbody>

								<tfoot>

									<tr>
										
										<td class="bold" colspan="4">Subtotal</td>
										<td class="total">$146.87</td>

									</tr>

									<tr>
										
										<td class="bold" colspan="4">Shipping &amp; Heading (Flat Rate - Fixed)</td>
										<td class="total">$5.00</td>

									</tr>

									<tr>
										
										<td class="grandtotal" colspan="4">Grand Total</td>
										<td class="grandtotal">$151.87</td>

									</tr>

								</tfoot>

							</table>

						</div><!--/ .table_wrap -->

						<footer class="bottom_box on_the_sides">

							<div class="left_side v_centered">

								<span>Forgot an item?</span>

								<a class="button_grey middle_btn" href="#">Edit Your Cart</a>

							</div>

							<div class="right_side">

								<button class="button_blue middle_btn">Place Order</button>

							</div>

						</footer>


					</section>

					<!-- - - - - - - - - - - - - - End of order review - - - - - - - - - - - - - - - - -->

				</div><!--/ .container-->

</div>