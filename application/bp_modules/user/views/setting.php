      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 m-top-40" >
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo ucfirst(strtolower($user['first_name'])).' '.ucfirst(strtolower($user['middle_name']))?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> 
                <?php if($user['avatar'] == ''):?>
                	<?php $user['avatar'] = 'avatar.gif'; ?>
                <?php endif; ?>
                <img alt="Avatar" src="<?php echo base_url().'assets/img/user/'.$user['avatar'];?>" class="img-circle img-responsive"> 
                </div>
                <div class=" col-md-8 col-lg-8 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Nama Depan</td>
                        <td>: <?php echo $user['first_name']; ?></td>
                      </tr>
                      <tr>
                        <td>Nama Tengah</td>
                        <td>: <?php echo $user['middle_name']; ?></td>
                      </tr>
                      <tr>
                        <td>Nama Belakang</td>
                        <td>: <?php echo $user['last_name']; ?></td>
                      </tr>
                      <tr>
                        <td>Jenis Kelamin</td>
                        <td>: <?php echo $user['jk']; ?></td>
                      </tr>
                      <tr>
                      	<td>Birth Date</td>
                      	<td>: <?php echo $user['birth_date']; ?></td>
                      </tr>
                        <tr>
                        <td>Alamat</td>
                        <td>: <?php echo $address; ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td>: <?php echo $user['email']; ?></td>
                      </tr>
                        <td>Phone Number</td>
                        <td>: <?php $user['phone']; ?>
                        <!--(Landline)<br><br>: 555-4567-890(Mobile)-->
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  
                </div>
              </div>
            </div>
                 <div class="panel-footer">
                        <!--<a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>-->
                        <span>&nbsp;</span>
                        <span class="pull-right">
                            <a href="<?php echo base_url()?>user_index/setting_edit" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i>&nbsp; Ubah data</a>
                            <!--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i></a>-->
                        </span>
                        <div class="clearfix">&nbsp;</div>
                    </div>
            
          </div>
        </div>
      </div>
