<style>
    .error-code{
        font-size: 5em;
        font-weight: bold;
    }
</style>
<div class="error">
        <div class="error-code m-b-10 m-t-20">404 <i class="fa fa-warning"></i></div>
        <h3 class="font-bold">We couldn't find the page :(</h3>

        <div class="error-desc">
            Maaf, halaman ini sedang dalam maintenance
            <div>
                <a class=" login-detail-panel-button btn" href="http://www.viaramall.com/">
                        <i class="fa fa-arrow-left"></i>
                        Kembali ke homepage                    
                    </a>
            </div>
        </div>
    </div>