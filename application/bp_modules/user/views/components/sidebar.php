<aside>
      <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
          	<?php 
          		$avatar = $this->session->userdata('avatar'); 
          		if(!$avatar || $avatar == ''){
          			$avatar = 'avatar.gif';
          		}
          	?>
          	  <p class="centered"><a href="profile.html"><img src="<?php echo base_url().'assets/img/user/'.$avatar;?>" class="img-circle" width="60"></a></p>
          	  <h5 class="centered"><?php echo ucfirst($this->session->userdata('name')); ?></h5>
              <li class="mt sub-menu">
              	<?php if($toko_exist):?>
                	<a class="" id="link_managemen_toko" href="javascript:;" >
                      	<i class="fa fa-desktop"></i>
                      	<span>Managemen Toko</span>
                  	</a>
                  	<ul class="sub">
                  		<li><a href="<?php echo base_url()?>store/store_index/sales">Penjualan</a></li>
                      	<li><a href="<?php echo base_url()?>products/product_index/list_product">Daftar Produk</a></li>
                      	<li><a href="<?php echo base_url()?>products/product_index/add">Tambah Produk</a></li>
                      	<li><a href="<?php echo base_url()?>store/store_index/setting">Pengaturan Toko</a></li>
                  	</ul>
				
				<?php else:?>

                  <a class="active" id="link_open_toko" href="<?php echo base_url()?>store/store_index/add">
                      <i class="fa fa-dashboard"></i>
                      <span>Buka Toko anda</span>
                  </a>
                <?php endif; ?>
              </li>
              <li class="sub-menu">
                  <a href="<?php echo base_url()?>discussion/discussion_index" >
                      <i class="fa fa-desktop"></i>
                      <span>Diskusi Produk</span>
                  </a>
              </li>
              <li class="sub-menu">
                  <a href="<?php echo base_url()?>reviews/reviews_index" id="link_review">
                      <i class="fa fa-cogs"></i>
                      <span>Review</span>
                  </a>
              </li>
              <li class="sub-menu">
                  <a href="<?php echo base_url()?>nego/nego_index" id="link_nego">
                      <i class="fa fa-book"></i>
                      <span>Notifikasi Nego</span>
                  </a>
              </li>
              <li class="sub-menu">
                  <a href="<?php echo base_url()?>purchase/purchase_index" id="link_purchase">
                      <i class="fa fa-tasks"></i>
                      <span>Pembelian</span>
                  </a>
              </li>
              <li class="sub-menu">
                  <a href="<?php echo base_url()?>user/user_index/setting" id="link_setting">
                      <i class="fa fa-th"></i>
                      <span>Pengaturan</span>
                  </a>
              </li>
    	
          </ul>
          <!-- sidebar menu end-->
      </div>
</aside>

<script>
	$(document).ready(function(){
		<?php if(!empty($link_id)):?>
			$('#<?php echo $link_id; ?>').addClass('active');
		<?php endif; ?>
	});
</script>