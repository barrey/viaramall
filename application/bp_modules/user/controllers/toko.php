<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Toko extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$data_sidebar['toko_exist'] = false;

		//parameter buat set active menu sidebar
		//page yg sedang dibuka
		$data_sidebar['page_toko'] = true;
		
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('user/user_index',$data,'components/backend');
	}

	public function setting(){
		$data = array();
		$data_sidebar['toko_exist'] = false;

		//parameter buat set active menu sidebar
		//page yg sedang dibuka
		$data_sidebar['page_toko'] = true;
		
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('user/user_index',$data,'components/backend');
	}

}

/* End of file toko.php */
/* Location: ./application/controllers/toko.php */