<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->check_login();
		$this->load->model('store_model');
		$this->load->model('user_model');
	}

	public function index()
	{
		$id = $this->session->userdata('user_id');
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('user/user_index',$data,'components/backend');
	}

	public function check_login(){
		$data = $this->session->all_userdata();
		if(!empty($data['is_login']) && !empty($data['user_id'])){
			return TRUE;
		}
		else{
			redirect(base_url().'auth/login');
		}
	}

	public function setting(){
		$id = $this->session->userdata('user_id');

		//data untuk sidebar
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);

		//data untuk content
		$where = array('a.id' => array('where' => $id), 'a.status' => array('where' => 'y'));
		$this->user_model->set_param($where);
		$this->user_model->set_select('a.first_name, a.middle_name, a.last_name, a.sex as jk, a.email, a.birth_date, b.alamat, c.no_telp as phone, e.name as subdistric, d.name as regency, a.avatar');
		$this->user_model->set_table_from('user as a');
		$this->user_model->set_table_join(
			array(
				'user_address as b' => array('b.user_id = a.id','left'),
				'user_telp as c' => array('c.user_id = a.id','left'),
				'bp_regency as d' => array('d.id = b.regency_id','left'),
				'bp_subdistrict as e' => array('e.id = b.subdistrict_id','left')
				)
		);
		$this->user_model->format = 'array';
		$result = $this->user_model->get_join_one();
		
		$data['user'] = $result;
		$data['address'] = $result['alamat'].', '.$result['subdistric'].', '.$result['regency'];
		$this->render('user/setting',$data,'components/backend');
	}

	public function setting_edit(){
		$id = $this->session->userdata('user_id');
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		$this->render('user/setting',$data,'components/backend');
	}

	public function cart(){
		//keranjang belanja
	}

	public function cart_add(){
		//tambah keranjang belanja
	}

	public function cart_del(){
		//hapus keranjang belanja
	}

	public function cart_update(){
		//edit keranjang belanja
	}

	public function alamat_edit(){

	}

	public function alamat_add(){

	}

	public function alamat_del(){
		
	}
}

/* End of file user_index.php */
/* Location: ./application/controllers/user_index.php */