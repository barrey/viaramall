<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reviews_index extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function list_review()
	{
		//method to display all product list with jqgrid
		$objek = modules::load('components/jqgrid');

		//name, deskripsi, kategori, etalase(nanti dulu), harga, stok, upload date (created date)
		$col_model = json_encode(array(
			//array('name' => 'no', 'label' => 'No', 'width' => 'auto', 'align' => 'left'),
			array('name' => 'product_name', 'label' => 'Nama Produk', 'width' => 300, 'align' => 'left'),
			array('name' => 'product_kategori', 'label' => 'Produk Kategori', 'width' => 200, 'align' => 'left'),
			array('name' => 'product_status', 'label' => 'Status Produk', 'width' => 100, 'align' => 'center')
		));

		//parsing data to view
		$data = array(
			'gridname' => $gridname = '_'.time(),
			'dataurl' => site_url('review/reviews_index/list_json'),
			'rownum' => '20',
			'caption' => 'Produk',
			'colmodel' => $col_model,
			'sortname' => 'a.created_date',
		);

		$data['table'] = $objek->table($data);

		//pemanggilan cross controller [add data jqgrid]
		$data_add = array(
			'div_loader'=>'#dialog-produk',
			'url_to_load'=>base_url().'products/product_index/add/'.$gridname
                );
		$data2 = array_merge($data,$data_add);
		$data['add'] = $objek->add($data2);

		//pemanggilan cross controller [feature edit popup]
		$data_edit = array(
			'id_jqgrid'=>'#newapi'.$gridname,
			'class_link_jqgrid'=>'.edit-dialog-propinsi',
			'url_to_load'=>base_url().'referensi/propinsi/edit'
			);
		$data3 = array_merge($data,$data_edit);
		$data['edit_popup'] = $objek->edit_popup($data3);

		$user_id = $this->session->userdata('user_id');
		$data_sidebar = array();
		$data_sidebar['toko_exist'] = $this->store_model->is_exist($user_id);
		$data['sidebar'] = $this->load->view('user/components/sidebar',$data_sidebar,TRUE);
		//load view propinsi
		$this->render('products/product_index', $data, 'components/backend');
	}

	public function review_json(){
		$this->load->model('review_model');
		$response = $this->review_model->get_data();
		$rs = json_encode($response);
		echo $rs;
	}
}

/* End of file reviews_index.php */
/* Location: ./application/controllers/reviews_index.php */