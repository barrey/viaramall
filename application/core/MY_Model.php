<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

    protected $_table;
    public $select_cell;
    public $param;
    public $table_from;
    public $table_join;
    public $table_join_param;
    public $format;
    public $count = FALSE;


    private function _param_handler(){
        //$param = array('id' => array('where' => 34), 'limit' => array(0, 10), 'order_by' => array('name', 'asc'));

        foreach($this->param as $d => $v){
            if(is_array($v)){
                foreach($v as $vd => $vv){
                    if($d == "limit"){
                        $this->db->limit($v[1], $v[0]);
                    }
                    else if($vd == "like"){
                        $this->db->like($d, $vv);
                    }

                    else if($vd == "or_like"){
                        $this->db->or_like($d, $vv);
                    }

                    else if($vd == "not_like"){
                        $this->db->not_like($d, $vv);
                    }

                    else if($vd == "or_not_like"){
                        $this->db->or_not_like($d, $vv);
                    }
                    
                    else if($vd == "where"){
                        $this->db->where($d, $vv);
                    }

                    else if($vd == "where_in"){
                        $this->db->where_in($d, $vv);
                    }

                    else if($vd == "where_not_in"){
                        $this->db->where_not_in($d, $vv);
                    }

                    else if($vd == "or_where"){
                        $this->db->or_where($d, $vv);
                    }

                    else if($vd == "or_where_in"){
                        $this->db->or_where_in($d, $vv);
                    }
                    
                    else if($vd == "or_where_not_in"){
                        $this->db->or_where_not_in($d, $vv);
                    }

                    else if($vd == "order_by"){
                        $this->db->order_by($d, $vv);
                    }
                }
            }
        }
    }

    public function set_param($input){
        return $this->param = $input;
    }

    public function set_select($input){
        return $this->select_cell = $input;
    }
    public function __construct(){
        parent::__construct();
        
    }

    public function set_table($input){
        return $this->_table = $input;
    }

    public function set_table_from($input){
        return $this->table_from = $input;
    }

    public function set_table_join($arr){
        return $this->table_join = $arr;
    }

    public function get_all(){
        //$select_cell = NULL, $param = NULL, $format = NULL, $count = FALSE
        //for grabbing data from one table only
        if(is_null($this->select_cell)){
            $this->db->select('*');
        }

        else{
            $this->db->select($this->select_cell);
        }

        if(!is_null($this->param)){
            $this->_param_handler($this->param);
        }

        $query = $this->db->get($this->_table);

        if($query->num_rows() > 0){
            //return COUNT
            if($this->count === TRUE){
                //clearing counting parameter
                $this->count = FALSE;
                return $query->num_rows();
            }


            if(!is_null($this->format)){
                if($this->format == "object"){
                    $result = $query->result();
                }

                else if($this->format == "array"){
                    $result = $query->result_array();
                }
            }

            else{
                $result = $query->result();
            }
        }

        else{
            $result = FALSE;
        }


        return $result;
    }
    
    public function get_join_all(){
        //$select_cell, $table_from, $table_join, $format = NULL, $count = FALSE
        //for grabbing data from many tables
        if(empty($this->select_cell)){
            $this->db->select('*');
        }else{
            $this->db->select($this->select_cell);
        }

        $this->db->from($this->table_from);


            foreach($this->table_join as $t => $d){
                        if(isset($d[1])){
                            $this->db->join($t, $d[0], $d[1]);
                        }

                        else{
                            $this->db->join($t, $d[0]);
                        }
            }
        
        if(!empty($this->param)){
            $this->_param_handler($this->param);
        }
        
        $query = $this->db->get();

        if($query->num_rows() > 0){
            //return COUNT
            if($this->count === TRUE){
                //clearing counting parameter
                $this->count = FALSE;
                return $query->num_rows();
            }


            if(is_null($this->format)){
                return $query->result();
            }

            else{
                if($this->format == "object"){
                    return $query->result();
                }

                else if($this->format == "array"){
                    return $query->result_array();
                }
            }
            
        }

        else{

        }
    }


    public function get_one(){
        //$select_cell = NULL, $param, $format = NULL

        if(empty($this->select_cell)){
            $this->db->select('*');
        }

        else{
            $this->db->select($this->select_cell);
        }

        $this->_param_handler($this->param);

        $query = $this->db->get($this->_table);

        if($query->num_rows()>0){
            if(is_null($this->format)){
                $result = $query->row();
            }

            else{
                if($this->format == "object"){
                    $result = $query->row();
                }

                else if($this->format == "array"){
                    $result = $query->row_array();
                }
            }

            return $result;
        }

        else{
            return FALSE;
        }
    }

    public function get_join_one(){
        //$select_cell = NULL, $table_from, $table_join, $table_join_param, $format = NULL
        if(is_null($this->select_cell)){
            $this->db->select('*');
        }

        else{
            $this->db->select($this->select_cell);
        }

        $this->db->from($this->table_from);

        $x = 1;
        foreach($this->table_join as $t => $d){
            if(isset($d[1])){
                $this->db->join($t, $d[0], $d[1]);
            }

            else{
                $this->db->join($t, $d[0]);
            }
            $x++;
        }

        if(!empty($this->param)){
            $this->_param_handler($this->param);
        }

        $query = $this->db->get();

        if($query->num_rows() > 0){
            if(!empty($this->format)){
                if($this->format == "array"){
                    $result = $query->row_array();
                }

                else if($this->format == "object"){
                    $result = $query->row();
                }
            }

            else{
                $result = $query->row();
            }
            return $result;
        }

        else{
            return FALSE;
        }
    }

    public function count_all(){
        //$param
        //return sum data from one table only
        $this->count = TRUE;
        return $this->get_all();
    }

    public function count_join(){
        //$table_from, $table_join, $table_join_param
        //return sum data from many tables
        $this->count = TRUE;
        return $this->get_join_all();
    }

    public function get_count($manual, $alias){
        //make sure $manual already escaped. this method is provided to minimize the usage of db processing
        //$manual = "select count(*) as i from user"; an alias is a MUST
        //ex: get_count($manual, 'i');

        $q = $this->db->query($manual);
        $result = $q->result();

        if(!empty($result[0]->$alias)){
            return $result[0]->$alias;
        }

        else{
            return FALSE;
        }
    }
    
    public function insert($data){
        $this->db->insert($this->_table, $data);
    }

    public function insert_all($data){
        //  this method is for save many data
        /*
            ex: $data = array(
                array('title' => 'buku b.inggris', 'harga' => 19000), 
                array('title' => 'buku b.indo', 'harga' => 23000)
            );
            $this->model->insert_all($data);
        */

        $this->db->insert_batch($this->_table, $data);

    }

    public function update($data){
        $this->_param_handler($this->param);
        $this->db->update($this->_table, $data);
    }

    public function delete(){
        $this->_param_handler($this->param);
        $this->db->delete($this->_table);
    }

    public function check($method){
        $this->_param_handler($this->param);
        $query = $this->db->get($this->_table);

        if($method == "insert"){
            if($query->num_rows() > 0){
                return FALSE;
            }

            else{
                return TRUE;
            }
        }

        else if($method == "edit"){

            if($query->num_rows() >= 1){
                $this->_param_handler($this->param);
                $query2 = $this->db->get($this->_table);

                if($query2->num_rows() == 1){
                    return TRUE;
                }

                else{
                    return FALSE;
                }
            }

            else if($query->num_rows() == 0){
                return TRUE;
            }
        }
        
    }

}

/* End of file  */
/* Location: ./application/models/ */