<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General {

	protected 	$ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function check_component($directory,$controller=null,$method=null)
	{
		$this->ci->load->helper('directory');
		$map = directory_map("./application/modules/$directory/");
		if(!empty($map))
		{
			if(method_exists($controller, $method))
			{
				return TRUE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function increment($bilangan){
		//this method will increment any format number
		//ex: TH009 will increment to TH010
		preg_match('/^\D*(?=\d)/', $bilangan, $m);

		if(isset($m[0])){
			$pos = strlen($m[0]);
		}

		$first_number = substr($bilangan, $pos, 1);

		if($first_number == 0){
			//if leading zero
			$num2 = ltrim($bilangan, "0");
			$num_of_zero = strlen($bilangan)-strlen($num2);
			
			$number = str_pad($bilangan + 1, $num_of_zero+1, 0, STR_PAD_LEFT);
		}

		else{
			//if leading no zero
			$number = substr($bilangan,$pos);
			$number += 1;
		}

		return $number;	
	}

	public function pembilang($angka){
		$abil = array(
					"",
					"Satu", "Dua", "Tiga",
					"Empat", "Lima", "Enam",
					"Tujuh", "Delapan", "Sembilan",
					"Sepuluh", "Sebelas"
					);
		if ($x < 12)
			return " ".$abil[$x];
		elseif ($x<20)
			return numbertell($x-10)." Belas";
		elseif ($x<100)
			return numbertell($x/10)." Puluh".numbertell($x%10);
		elseif ($x<200)
			return " Seratus".numbertell($x-100);
		elseif ($x<1000)
			return numbertell($x/100)." Ratus".numbertell($x % 100);
		elseif ($x<2000)
			return " Seribu".numbertell($x-1000);
		elseif ($x<1000000)
			return numbertell($x/1000)." Ribu".numbertell($x%1000);
		elseif ($x<1000000000)
			return numbertell($x/1000000)." Juta".numbertell($x%1000000);
		elseif ($x<1000000000000)
			return numbertell($x/1000000000)." Milyar".numbertell($x%1000000000);
		elseif ($x<1000000000000000)
			return numbertell($x/1000000000000)." Trilyun".numbertell($x%1000000000000);
	}

	public function directory_check($dir){
		if (!file_exists($dir)) {
		    mkdir($dir, 0777);
		    return TRUE;
		} else {
		    return FALSE;
		}
	}

	public function resize_image($image, $location){
		//function to resize image
		$config['image_library'] = 'gd2';
		$config['source_image']	= $location.$image;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']	= 620;
		$config['height']	= 350;

		$this->load->library('image_lib', $config); 

		$this->image_lib->resize();
	}
}

/* End of file  */
/* Location: ./application/controllers/ */