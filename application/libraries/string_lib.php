<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Underscore\Types\String;

class String_lib {

	protected 	$ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	public function check_component($directory,$controller=null,$method=null)
	{
		$this->ci->load->helper('directory');
		$map = directory_map("./application/modules/$directory/");
		if(!empty($map))
		{
			if(method_exists($controller, $method))
			{
				return TRUE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function tes(){
		return Arrays::max(array(1, 2, 3));
	}

}

/* End of file  */
/* Location: ./application/controllers/ */