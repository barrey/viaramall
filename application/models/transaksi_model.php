<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			class Transaksi_model extends MY_Model {

				public $_table = "transaksi";

				public function __construct()
				{
					parent::__construct();
					
				}

				public function get_data(){
					$store_id = $this->session->userdata('store_id');
					$user_id = $this->session->userdata('user_id');
					$limit      = $this->input->get_post('rows');
		            $limitstart = $this->input->get_post('limitstart');
		            $sortby     = $this->input->get_post('sidx');
		            $sortdir    = $this->input->get_post('sord');
		            $page       = $this->input->get_post('page');

		            /*search*/
		            $search       = $this->input->get_post('_search');
		            $sField       = $this->input->get_post('searchField');
		            $sOper        = $this->input->get_post('searchOper');
		            $sString      = $this->input->get_post('searchString');
		            $ops = '';
		            if ($search == 'true') {
		                $ops = array(
		                    'eq'=>'=', //equal
		                    'ne'=>'<>',//not equal
		                    'lt'=>'<', //less than
		                    'le'=>'<=',//less than or equal
		                    'gt'=>'>', //greater than
		                    'ge'=>'>=',//greater than or equal
		                    'bw'=>'LIKE', //begins with
		                    'bn'=>'NOT LIKE', //doesn't begin with
		                    'in'=>'LIKE', //is in
		                    'ni'=>'NOT LIKE', //is not in
		                    'ew'=>'LIKE', //ends with
		                    'en'=>'NOT LIKE', //doesn't end with
		                    'cn'=>'LIKE', // contains
		                    'nc'=>'NOT LIKE'  //doesn't contain
		                );
		                foreach ($ops as $key=>$value){
		                    if ($sOper==$key) {
		                        $ops = $value;
		                    }
		                }
		                if($sOper == 'eq' ) $sString = $sString;
		                if($sOper == 'bw' || $sOper == 'bn') $sString .= '%';
		                if($sOper == 'ew' || $sOper == 'en' ) $sString = '%'.$sString;
		                if($sOper == 'cn' || $sOper == 'nc' || $sOper == 'in' || $sOper == 'ni') $sString = '%'.$sString.'%';
		                $sWhere = "$sField $ops '$sString'";
		            } else {
		                $sWhere = "1=1";
		            }                
		                $sWhere .= " and a.store_id = ".$store_id;
		    		#first, we set the rows, means LIMIT default is 20
		            if(isset($_POST['rows'])){
		                $limit=$this->input->get_post('rows');
		            }
		            else{
		                $limit=20;
		            }

		            #second, we read whether there's a PAGE-ing request
		            if(isset($_POST['page'])){
		                $page=$this->input->get_post('page');
		            }else{
		                $page=1;
		            }

		            #third, we read the sort
		            if(!empty($_POST['sidx'])){
		                $sidx=$this->input->get_post('sidx');
		                //$sidx = $this->col_identifier($sidx);
		            }else{
		                $sidx="id";
		            }

		            #fourth, read sorting method "desc" or "asc"
		            if(isset($_POST['sord'])){
		                $sord=$this->input->get_post('sord');
		            }else{
		                $sord="asc";
		                //$sord = $this->col_identifier($sord);
		            }
		            
		            $this->db->from('store_produk as a');

		            //PENCARIAN
		            if(!empty($_POST['pencarian']))
		            {
		                $nama_produk = $this->input->post('nama_produk');
		                $harga = $this->input->post('harga');
		                if(!empty($harga))
		                {
		                    $this->db->like('a.harga',$harga);
		                }
		                if(!empty($nama_produk))
		                {
		                    $this->db->like('a.name',$nama_produk);
		                }
		            }

		            $this->db->where($sWhere,null,false);
		            $count = $this->db->count_all_results();
		            $data['records']= $count;
		            
		            if($count>0){
		                $total_pages=ceil($count/$limit);
		            }else{
		                $total_pages=1;
		            }
		            if($page>$total_pages){
		                $data['page']=$total_pages;
		            }
		            $start=($limit*$page)-$limit;
		            $response = new stdClass;
		            $response->page = $page; 
		            $response->total = $total_pages; 
		            $response->records = $count;
		            $this->db->limit($limit,$start);
		            $this->db->order_by("$sidx","$sord");

		            $this->db->select('a.id as id, a.name as product_name, b.name as product_kategori, a.status as product_status, c.name as product_img');
		            $this->db->from('store_produk a');
		            $this->db->join('kategori as b','b.id = a.kategori_id', 'left');
		            $this->db->join('(select name, store_id, store_produk_id from image_produk as c group by store_produk_id) c','c.store_produk_id = a.id and a.store_id = c.store_id', 'left');
		            //PENCARIAN
		            if(!empty($_POST['pencarian']))
		            {
		                $nama_produk = $this->input->post('nama_produk');
		                $harga = $this->input->post('harga');
		                if(!empty($harga))
		                {
		                    $this->db->like('a.harga',$harga);
		                }
		                if(!empty($nama_produk))
		                {
		                    $this->db->like('a.name',$nama_produk);
		                }
		            }

		            $this->db->where($sWhere,null,false);
		            $query = $this->db->get();
		            if($query->num_rows()>0)
		            {
		                foreach ($query->result() as $key) 
		                    {
		                    	/*
		                        if($key->product_status=="y")
		                        {
		                            $status = '<a href="" class="btn btn-info">Ubah</a><a href="" class="btn btn-error">Hapus</a>';
		                        }
		                        else
		                        {
		                            $status = '<a href="">'..'</a>';
		                        }
		                        */
		                        $img = '<img src="'.base_url().'assets/store/product/'.$user_id.'/'.$key->product_img.'" style="height: 100px;">';
		                        $status = '<a href="'.base_url().'products/product_index/edit/'.$key->id.'" class="btn btn-info">Ubah</a> &nbsp; <a href="'.base_url().'products/product_index/del/'.$key->id.'" class="btn btn-warning del">Hapus</a>';	
		                        $name='<a href="#" title="Sunting" class="edit-dialog-propinsi link-biru" data-id="'.$key->id.'" alt="Sunting" style="font-weight:">'.$key->product_name.'</a>';
		                        $record_items[] = array(
		                                'id' => $key->id,
		                                'product_img' => $img,
		                                'product_name' => $key->product_name,
		                                'product_kategori' => $key->product_kategori,
		                                'product_status' => $status,
		                        );
		                    }
		                $data['data'] = $record_items;
		                $data['total'] = $total_pages;
		                if(empty($page))
		                {
		                    $data['page'] = 1;
		                }
		                else
		                {
		                    $data['page']=$page;
		                }
		                return $data;
		            }
		            else
		            {
		                //daripada return FALSE yang jqgrid gak kenalin
		                //mending di return data 0 aja
		                $data['data'] = 0;
		                $data['total'] = 0;
		                $data['records'] = 0;
		                return $data;
		            }
		        }

		        public function product_list(){
		        	$this->db->select('d.seller_id, a.store_id, a.id as id, a.name as product_name, b.name as product_kategori, a.status as product_status, c.name as product_img, a.harga as product_harga');
		            $this->db->from('store_produk a');
		            $this->db->join('kategori as b','b.id = a.kategori_id');
		            $this->db->join('(select name, store_id, store_produk_id from image_produk as c group by store_produk_id) c','c.store_produk_id = a.id and a.store_id = c.store_id');
		            $this->db->join('store as d','d.id = a.store_id');
		            $query = $this->db->get();
		            return $query->result();
		        }
			}

			/* End of file  */
			/* Location: ./application/models/transaksi_model.php */