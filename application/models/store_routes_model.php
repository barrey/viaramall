<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			class Store_routes_model extends MY_Model {

				public $_table = "store_routes";

				public function __construct()
				{
					parent::__construct();
					
				}

				public function check($store_name){
					$where = array('url_store' => array('where' => $store_name));
					$this->set_param($where);

					if($this->get_all()){
						return FALSE;
					}else{
						return TRUE;
					}
				}

			}

			/* End of file  */
			/* Location: ./application/models/store_routes_model.php */