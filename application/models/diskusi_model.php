<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Diskusi_model extends MY_Model {

		public $_table = "diskusi";

		public function __construct()
		{
			parent::__construct();
			
		}

		public function get_diskusi($id_store_product){
			$where = array('a.id' => array('where' => $id_store_product));
			$this->set_select(array());
			$this->set_param($where);
			$this->set_table_from('store_produk a');
			$this->set_table_join(array('diskusi b' => array('b.store_product_id = a.id')));
			return $this->get_join_all();
		}

	}

			/* End of file  */
			/* Location: ./application/models/diskusi_model.php */