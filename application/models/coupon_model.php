<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			class Coupon_model extends MY_Model {

				public $_table = "coupon";

				public function __construct()
				{
					parent::__construct();
					
				}

				public function list_coupon(){
					$param = array(
						//add more parameter here if you want more parameter
						'status' => array('where' => 'y')
					);

					$this->set_param($param);

					$coupon = $this->get_all();
					echo $this->db->last_query();
					foreach($coupon as $c){
						$result['tiket'] = $c->coupon;
						$result['store'] = $c->store_id;
					}

					return $result;
				}
				
			}

			/* End of file  */
			/* Location: ./application/models/coupon_model.php */