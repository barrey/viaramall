<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			class Store_model extends MY_Model {

				public $_table = "store";

				public function __construct()
				{
					parent::__construct();
					
				}

				public function store_list(){
					//nama toko, id toko, created date, lokasi toko (propinsi), lokasi toko (kota), status toko, owner toko
					$this->set_table_from($this->_table);
					$this->set_table_join(
							array()
						);
					$this->select_cell(
							array()
						);

					$where = array();
					$this->set_param($where);

					return $this->get_join_all();
				}

				public function store_list_json(){
					$data = $this->store_list();
					echo json_encode($data);
				}

				public function store_add($data){
					$this->insert($data);
					return TRUE;
				}

				public function store_soft_del($data, $param){
					$this->set_param($param);
					$this->update($data);
					return TRUE;
				}

				public function store_admin_view($id){
					//data provide untuk view store by admin
					$this->set_table_from($this->_table);
					$this->set_table_join(
							array('')
						);
					$this->set_param(array('store.id' => array('where' => $id)));

				}

				public function store_detail($id_store_product){
		            $this->db->from('store_produk a');
		            $this->db->join('store as b','b.id = a.store_id');
		            $this->db->where('a.id',$id_store_product);
		            $query = $this->db->get();
		            return $query->row();
				}

				public function is_exist($id){
					//check whether user has store
					$where = array('seller_id' => array('where' => $id), 'status' => array('where' => 'y'));
					$this->set_param($where);
					$this->set_select('seller_id,id,store_name');
					$user = $this->get_one();
					
					if($user){
						return $user;
					}

					return FALSE;
				}
			}

			/* End of file  */
			/* Location: ./application/models/store_model.php */