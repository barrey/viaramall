<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model {

	public $_table = "user";

	/**
	 * gimana kalo gw mau join misal join 3 table dengan masing masing parameter
	 * join tabel buku, profil, gaji
	 * korelasi buku = id user
	 * korelasi profil = id user
	 * korelasi gaji = id user
	 $_table_join1 = 'buku';
	 $_table_join2 = 'profil';
	 $_table_join3 = 'gaji';
	
	$this->db->select('*');
	$this->db->from('user');
	$this->db->join('buku', 'buku.iduser = user.id');
	$this->db->join('gaji', 'gaji.iduser = user.id');
	$this->db->join('profil', 'profil.iduser = user.id', 'left');
	$this->db->where('buku.id', 4);
	$this->db->get();

	$table_join1 = array('buku' => array('buku.iduser = user.id', ''), 'gaji' => array('gaji.iduser = user.id'), 'profil' => array('profil.iduser = user.id', 'left'));
	$table_join1_param = array('buku.id' => array('where' => 4));

	//execute
	$this->public->get_one_join($table_join1, $table_join1_param);
	$this->public->get_all_join($table_join1, $table_join1_param);

	 */


	public function __construct()
	{
		parent::__construct();
		
	}

	function out(){
		echo $this->_table;
	}


}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */