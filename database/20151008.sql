-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for viaramall
CREATE DATABASE IF NOT EXISTS `viaramall` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `viaramall`;


-- Dumping structure for table viaramall.app_routes
CREATE TABLE IF NOT EXISTS `app_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` char(50) NOT NULL,
  `controller` char(100) NOT NULL,
  `date_submitted` date NOT NULL,
  `date_modified` date NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='perkembangan kedepan harus ada pembeda antara routes milik system dengan yang bukan system. ini untuk kasus toko online. domain.com/namatoko dengan domain.com/setting atau domain.com/logout. setting dan logout adalah system routes.';

-- Dumping data for table viaramall.app_routes: ~0 rows (approximately)
/*!40000 ALTER TABLE `app_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_routes` ENABLE KEYS */;


-- Dumping structure for table viaramall.coupon
CREATE TABLE IF NOT EXISTS `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon` varchar(20) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL COMMENT 'coupon for specific store',
  `created_date` datetime DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='coupon yang ada di viaramall';

-- Dumping data for table viaramall.coupon: ~2 rows (approximately)
/*!40000 ALTER TABLE `coupon` DISABLE KEYS */;
REPLACE INTO `coupon` (`id`, `coupon`, `store_id`, `created_date`, `expired_date`, `status`) VALUES
	(1, 'JSHUINJDK-KJASDH-009', 8, '2015-09-26 21:54:35', '2015-09-27 21:54:36', 'y'),
	(2, 'SADKJH8700', 7, '2015-09-26 21:56:09', '2015-09-28 21:56:10', 'y');
/*!40000 ALTER TABLE `coupon` ENABLE KEYS */;


-- Dumping structure for table viaramall.courir_activity
CREATE TABLE IF NOT EXISTS `courir_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `date_sent` datetime NOT NULL,
  `courir_agent_id` smallint(6) NOT NULL,
  `courir_packet_id` smallint(6) NOT NULL,
  `courir_resi` varchar(50) NOT NULL,
  `courir_from` smallint(6) NOT NULL,
  `courir_to` smallint(6) NOT NULL,
  `courir_status` varchar(50) DEFAULT 'DIKIRIM' COMMENT 'value: DIKIRIM, SAMPAI',
  `date_created` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='aktivitas pengiriman barang';

-- Dumping data for table viaramall.courir_activity: ~0 rows (approximately)
/*!40000 ALTER TABLE `courir_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `courir_activity` ENABLE KEYS */;


-- Dumping structure for table viaramall.courir_agent
CREATE TABLE IF NOT EXISTS `courir_agent` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='available kurir/ agen pengiriman';

-- Dumping data for table viaramall.courir_agent: ~0 rows (approximately)
/*!40000 ALTER TABLE `courir_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `courir_agent` ENABLE KEYS */;


-- Dumping structure for table viaramall.courir_packet
CREATE TABLE IF NOT EXISTS `courir_packet` (
  `id` int(11) DEFAULT NULL,
  `courir_agent_id` smallint(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `from` smallint(6) DEFAULT NULL,
  `to` smallint(6) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='paket-paket/ produk yang tersedia di agen kurir';

-- Dumping data for table viaramall.courir_packet: ~0 rows (approximately)
/*!40000 ALTER TABLE `courir_packet` DISABLE KEYS */;
/*!40000 ALTER TABLE `courir_packet` ENABLE KEYS */;


-- Dumping structure for table viaramall.discount
CREATE TABLE IF NOT EXISTS `discount` (
  `id` int(11) NOT NULL,
  `discount_type_id` int(11) DEFAULT NULL,
  `discount_value` int(11) DEFAULT NULL,
  `store_produk_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_type_id` (`discount_type_id`),
  KEY `discount_storeproduk_id` (`store_produk_id`),
  CONSTRAINT `discount_storeproduk_id` FOREIGN KEY (`store_produk_id`) REFERENCES `store_produk` (`id`),
  CONSTRAINT `discount_type_id` FOREIGN KEY (`discount_type_id`) REFERENCES `discount_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.discount: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount` ENABLE KEYS */;


-- Dumping structure for table viaramall.discount_type
CREATE TABLE IF NOT EXISTS `discount_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.discount_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_type` ENABLE KEYS */;


-- Dumping structure for table viaramall.dompet
CREATE TABLE IF NOT EXISTS `dompet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL COMMENT 'admin yang proses',
  `date_in` datetime NOT NULL COMMENT 'tanggal masuk saldo',
  `date_input` datetime NOT NULL,
  `saldo` int(11) NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`),
  KEY `dompet_user_id` (`user_id`),
  KEY `dompet_admin_id` (`admin_id`),
  CONSTRAINT `dompet_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `user_admin` (`id`),
  CONSTRAINT `dompet_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi saldo user yang masuk';

-- Dumping data for table viaramall.dompet: ~0 rows (approximately)
/*!40000 ALTER TABLE `dompet` DISABLE KEYS */;
/*!40000 ALTER TABLE `dompet` ENABLE KEYS */;


-- Dumping structure for table viaramall.dompet_transaksi
CREATE TABLE IF NOT EXISTS `dompet_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `activity` varchar(50) DEFAULT NULL COMMENT 'jenis aktifitas. contoh PEMBELIAN',
  `activity_info` varchar(200) DEFAULT NULL COMMENT 'deskripsi aktifitas transaksi. contoh: membeli jam casio tipe DX/390 Rp. 2.000.380',
  `activity_date` datetime DEFAULT NULL,
  `proceed_by` int(11) DEFAULT NULL COMMENT 'id admin yang memproses',
  PRIMARY KEY (`id`),
  KEY `dompettransaksi_user_id` (`user_id`),
  KEY `dompettransaksi_admin_id` (`proceed_by`),
  CONSTRAINT `dompettransaksi_admin_id` FOREIGN KEY (`proceed_by`) REFERENCES `user_admin` (`id`),
  CONSTRAINT `dompettransaksi_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi transaksi yang menggunakan saldo pada dompet viaramall';

-- Dumping data for table viaramall.dompet_transaksi: ~0 rows (approximately)
/*!40000 ALTER TABLE `dompet_transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `dompet_transaksi` ENABLE KEYS */;


-- Dumping structure for table viaramall.email_setting
CREATE TABLE IF NOT EXISTS `email_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_sender` varchar(50) NOT NULL,
  `smtp_host` varchar(100) NOT NULL,
  `smtp_port` smallint(6) NOT NULL,
  `smtp_timeout` smallint(6) NOT NULL,
  `email_password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='setting untuk pengiriman email';

-- Dumping data for table viaramall.email_setting: ~0 rows (approximately)
/*!40000 ALTER TABLE `email_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_setting` ENABLE KEYS */;


-- Dumping structure for table viaramall.general_data
CREATE TABLE IF NOT EXISTS `general_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` varchar(200) NOT NULL,
  `created_date` date NOT NULL,
  `admin_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='data data yang umum digunakan di aplikasi seperti nomor telepon, alamat dan data-data umum lainnya';

-- Dumping data for table viaramall.general_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `general_data` ENABLE KEYS */;


-- Dumping structure for table viaramall.image_produk
CREATE TABLE IF NOT EXISTS `image_produk` (
  `id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `store_produk_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  KEY `store_produk_id` (`store_produk_id`),
  CONSTRAINT `store_produk_id` FOREIGN KEY (`store_produk_id`) REFERENCES `store_produk` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.image_produk: ~0 rows (approximately)
/*!40000 ALTER TABLE `image_produk` DISABLE KEYS */;
/*!40000 ALTER TABLE `image_produk` ENABLE KEYS */;


-- Dumping structure for table viaramall.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.kategori: ~0 rows (approximately)
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;


-- Dumping structure for table viaramall.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL,
  `transaksi_id` int(11) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_bank` varchar(255) DEFAULT NULL,
  `payment_rek` varchar(255) DEFAULT NULL,
  `payment_struk` varchar(255) DEFAULT NULL COMMENT 'bukti struk pembayaran yang diberikan user',
  `payment_cost` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL COMMENT 'admin yang approve pembayaran dari user',
  `created_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT 'waiting' COMMENT 'waiting, lunas, cancel',
  PRIMARY KEY (`id`),
  KEY `transaksi_id` (`transaksi_id`),
  CONSTRAINT `transaksi_id` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.payment: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


-- Dumping structure for table viaramall.point
CREATE TABLE IF NOT EXISTS `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `value` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `point_user_id` (`user_id`),
  CONSTRAINT `point_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.point: ~0 rows (approximately)
/*!40000 ALTER TABLE `point` DISABLE KEYS */;
/*!40000 ALTER TABLE `point` ENABLE KEYS */;


-- Dumping structure for table viaramall.point_transaksi
CREATE TABLE IF NOT EXISTS `point_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity` varchar(50) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pointtransaksi_user_id` (`user_id`),
  CONSTRAINT `pointtransaksi_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi pendapatan point user viaramall';

-- Dumping data for table viaramall.point_transaksi: ~0 rows (approximately)
/*!40000 ALTER TABLE `point_transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `point_transaksi` ENABLE KEYS */;


-- Dumping structure for table viaramall.store
CREATE TABLE IF NOT EXISTS `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `store_name` varchar(30) NOT NULL,
  `description` varchar(144) DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  `img` varchar(50) DEFAULT NULL,
  `location` smallint(6) DEFAULT NULL COMMENT 'lokasi toko',
  PRIMARY KEY (`id`),
  KEY `seller_id` (`seller_id`),
  CONSTRAINT `seller_id` FOREIGN KEY (`seller_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi store yang dimiliki user';

-- Dumping data for table viaramall.store: ~0 rows (approximately)
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
/*!40000 ALTER TABLE `store` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_courir
CREATE TABLE IF NOT EXISTS `store_courir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` smallint(6) NOT NULL,
  `courir_agent_id` smallint(6) NOT NULL,
  `courir_packet_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='agen pengiriman yang dipakai oleh toko';

-- Dumping data for table viaramall.store_courir: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_courir` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_courir` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_etalase
CREATE TABLE IF NOT EXISTS `store_etalase` (
  `id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `etalase_name` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='etalase toko contoh: satu toko ada etalase xiaomi, samsung, sony, meizu';

-- Dumping data for table viaramall.store_etalase: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_etalase` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_etalase` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_invoice
CREATE TABLE IF NOT EXISTS `store_invoice` (
  `id` int(11) DEFAULT NULL,
  `transaksi_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  KEY `transaksiinvoice_transaksi_id` (`transaksi_id`),
  KEY `transaksiinvoice_invoice_id` (`store_id`),
  CONSTRAINT `transaksiinvoice_invoice_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `transaksiinvoice_transaksi_id` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi invoice dan status pengiriman';

-- Dumping data for table viaramall.store_invoice: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_invoice` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_notifikasi
CREATE TABLE IF NOT EXISTS `store_notifikasi` (
  `id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `notif` varchar(200) DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `storenotif_read_id` (`store_id`),
  CONSTRAINT `storenotif_read_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='notifikasi untuk tiap2 store';

-- Dumping data for table viaramall.store_notifikasi: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_notifikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_notifikasi` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_notifikasi_read
CREATE TABLE IF NOT EXISTS `store_notifikasi_read` (
  `id` int(11) DEFAULT NULL,
  `store_notifikasi_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `store_notifikasi_id` (`store_notifikasi_id`),
  KEY `storenotifikasi_store_id` (`store_id`),
  CONSTRAINT `storenotifikasi_store_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `store_notifikasi_id` FOREIGN KEY (`store_notifikasi_id`) REFERENCES `store_notifikasi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='notifikasi yang telah di read';

-- Dumping data for table viaramall.store_notifikasi_read: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_notifikasi_read` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_notifikasi_read` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_produk
CREATE TABLE IF NOT EXISTS `store_produk` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL COMMENT 'termasuk ukuran produk ada disini',
  `kategori_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `store_etalase_id` int(11) DEFAULT NULL COMMENT 'produk ini masuk etalase mana',
  `harga` int(11) NOT NULL,
  `stok` smallint(6) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `status` enum('y','n') DEFAULT 'y',
  KEY `id` (`id`),
  KEY `kategori_id` (`kategori_id`),
  CONSTRAINT `kategori_id` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi produk yang ada di viaramall';

-- Dumping data for table viaramall.store_produk: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_produk` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_produk` ENABLE KEYS */;


-- Dumping structure for table viaramall.store_routes
CREATE TABLE IF NOT EXISTS `store_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_store` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` varchar(50) DEFAULT 'aktif',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.store_routes: ~0 rows (approximately)
/*!40000 ALTER TABLE `store_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_routes` ENABLE KEYS */;


-- Dumping structure for table viaramall.transaksi
CREATE TABLE IF NOT EXISTS `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` varchar(50) DEFAULT NULL COMMENT 'jenis aktifitas yang terjadi',
  `activity_info` varchar(200) DEFAULT NULL COMMENT 'info tambahan aktifitas',
  `activity_status` char(50) DEFAULT '---',
  `store_produk_id` int(11) DEFAULT NULL,
  `product_count` tinyint(4) DEFAULT NULL,
  `product_cost` int(11) DEFAULT NULL,
  `payment_cost` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `store_product_id` (`store_produk_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `coupon_id` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`),
  CONSTRAINT `store_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `store_product_id` FOREIGN KEY (`store_produk_id`) REFERENCES `store_produk` (`id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi transaksi yang terjadi di store user viaramall\r\ntransaksi yang terjadi hanya pembelian';

-- Dumping data for table viaramall.transaksi: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;


-- Dumping structure for table viaramall.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `join_date` datetime NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `sex` enum('m','f') DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='informasi user viaramall';

-- Dumping data for table viaramall.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `first_name`, `middle_name`, `last_name`, `username`, `join_date`, `email`, `password`, `sex`, `birth_date`, `status`) VALUES
	(12, 'aji', '', '', NULL, '2015-10-04 09:21:09', 'barrey@ovi.com', '346df72b0f527251070758886aacf7b2', NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table viaramall.user_address
CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `marked_as` varchar(50) NOT NULL COMMENT 'penanda alamat, contoh: alamat kantor',
  `regency_id` int(11) NOT NULL,
  `subdistrict_id` int(11) NOT NULL,
  `village_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='alamat user untuk pengiriman';

-- Dumping data for table viaramall.user_address: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;


-- Dumping structure for table viaramall.user_admin
CREATE TABLE IF NOT EXISTS `user_admin` (
  `id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `sex` enum('m','f') DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.user_admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_admin` ENABLE KEYS */;


-- Dumping structure for table viaramall.user_bank
CREATE TABLE IF NOT EXISTS `user_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bank_id` tinyint(4) DEFAULT NULL,
  `bank_rekening` varchar(20) DEFAULT NULL,
  `nama_rekening` varchar(100) DEFAULT NULL COMMENT 'nama pemilik rekening',
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='bank yang dipakai oleh user untuk melakukan pembayaran';

-- Dumping data for table viaramall.user_bank: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_bank` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_bank` ENABLE KEYS */;


-- Dumping structure for table viaramall.user_register_temp
CREATE TABLE IF NOT EXISTS `user_register_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `link` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `expired_date` datetime NOT NULL,
  `status` enum('y','n') DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporary link registration ';

-- Dumping data for table viaramall.user_register_temp: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_register_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_register_temp` ENABLE KEYS */;


-- Dumping structure for table viaramall.user_telp
CREATE TABLE IF NOT EXISTS `user_telp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='telepon user';

-- Dumping data for table viaramall.user_telp: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_telp` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_telp` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
