-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for viaramall
CREATE DATABASE IF NOT EXISTS `viaramall` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `viaramall`;


-- Dumping structure for table viaramall.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table viaramall.kategori: ~11 rows (approximately)
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id`, `name`, `status`) VALUES
	(1, 'Fashion dan Aksesoris', 'y'),
	(2, 'Pakaian', 'y'),
	(3, 'Kecantikan', 'y'),
	(4, 'Kesehatan', 'y'),
	(5, 'Rumah Tangga', 'y'),
	(6, 'Dapur', 'y'),
	(7, 'Perawatan Bayi', 'y'),
	(8, 'Handphone dan Tablet', 'y'),
	(9, 'Office dan Stationery', 'y'),
	(10, 'Laptop dan Aksesoris', 'y'),
	(11, 'Olahraga', 'y');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
