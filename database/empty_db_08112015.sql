-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for viaramall
DROP DATABASE IF EXISTS `viaramall`;
CREATE DATABASE IF NOT EXISTS `viaramall` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `viaramall`;


-- Dumping structure for table viaramall.app_routes
DROP TABLE IF EXISTS `app_routes`;
CREATE TABLE IF NOT EXISTS `app_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` char(50) NOT NULL,
  `controller` char(100) NOT NULL,
  `date_submitted` date NOT NULL,
  `date_modified` date NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='perkembangan kedepan harus ada pembeda antara routes milik system dengan yang bukan system. ini untuk kasus toko online. domain.com/namatoko dengan domain.com/setting atau domain.com/logout. setting dan logout adalah system routes.';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.bp_province
DROP TABLE IF EXISTS `bp_province`;
CREATE TABLE IF NOT EXISTS `bp_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(2) unsigned zerofill DEFAULT NULL,
  `name` varchar(225) NOT NULL,
  `del` set('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.bp_regency
DROP TABLE IF EXISTS `bp_regency`;
CREATE TABLE IF NOT EXISTS `bp_regency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `id_bp_province` int(11) NOT NULL,
  `del` enum('y','n') DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.bp_subdistrict
DROP TABLE IF EXISTS `bp_subdistrict`;
CREATE TABLE IF NOT EXISTS `bp_subdistrict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(3) unsigned zerofill DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `id_bp_regency` int(11) DEFAULT NULL,
  `del` set('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.bp_villages
DROP TABLE IF EXISTS `bp_villages`;
CREATE TABLE IF NOT EXISTS `bp_villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(4) unsigned zerofill NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_bp_subdistrict` int(11) NOT NULL,
  `del` set('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.coupon
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE IF NOT EXISTS `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon` varchar(20) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL COMMENT 'coupon for specific store',
  `created_date` datetime DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='coupon yang ada di viaramall';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.courir_activity
DROP TABLE IF EXISTS `courir_activity`;
CREATE TABLE IF NOT EXISTS `courir_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `date_sent` datetime NOT NULL,
  `courir_agent_id` smallint(6) NOT NULL,
  `courir_packet_id` smallint(6) NOT NULL,
  `courir_resi` varchar(50) NOT NULL,
  `courir_from` smallint(6) NOT NULL,
  `courir_to` smallint(6) NOT NULL,
  `courir_status` varchar(50) DEFAULT 'DIKIRIM' COMMENT 'value: DIKIRIM, SAMPAI',
  `date_created` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='aktivitas pengiriman barang';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.courir_agent
DROP TABLE IF EXISTS `courir_agent`;
CREATE TABLE IF NOT EXISTS `courir_agent` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='available kurir/ agen pengiriman';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.courir_packet
DROP TABLE IF EXISTS `courir_packet`;
CREATE TABLE IF NOT EXISTS `courir_packet` (
  `id` int(11) DEFAULT NULL,
  `courir_agent_id` smallint(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `from` smallint(6) DEFAULT NULL,
  `to` smallint(6) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='paket-paket/ produk yang tersedia di agen kurir';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.discount
DROP TABLE IF EXISTS `discount`;
CREATE TABLE IF NOT EXISTS `discount` (
  `id` int(11) NOT NULL,
  `discount_type_id` int(11) DEFAULT NULL,
  `discount_value` int(11) DEFAULT NULL,
  `store_produk_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_type_id` (`discount_type_id`),
  KEY `discount_storeproduk_id` (`store_produk_id`),
  CONSTRAINT `discount_storeproduk_id` FOREIGN KEY (`store_produk_id`) REFERENCES `store_produk` (`id`),
  CONSTRAINT `discount_type_id` FOREIGN KEY (`discount_type_id`) REFERENCES `discount_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.discount_type
DROP TABLE IF EXISTS `discount_type`;
CREATE TABLE IF NOT EXISTS `discount_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.dompet
DROP TABLE IF EXISTS `dompet`;
CREATE TABLE IF NOT EXISTS `dompet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL COMMENT 'admin yang proses',
  `date_in` datetime NOT NULL COMMENT 'tanggal masuk saldo',
  `date_input` datetime NOT NULL,
  `saldo` int(11) NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`),
  KEY `dompet_user_id` (`user_id`),
  KEY `dompet_admin_id` (`admin_id`),
  CONSTRAINT `dompet_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `user_admin` (`id`),
  CONSTRAINT `dompet_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi saldo user yang masuk';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.dompet_transaksi
DROP TABLE IF EXISTS `dompet_transaksi`;
CREATE TABLE IF NOT EXISTS `dompet_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `activity` varchar(50) DEFAULT NULL COMMENT 'jenis aktifitas. contoh PEMBELIAN',
  `activity_info` varchar(200) DEFAULT NULL COMMENT 'deskripsi aktifitas transaksi. contoh: membeli jam casio tipe DX/390 Rp. 2.000.380',
  `activity_date` datetime DEFAULT NULL,
  `proceed_by` int(11) DEFAULT NULL COMMENT 'id admin yang memproses',
  PRIMARY KEY (`id`),
  KEY `dompettransaksi_user_id` (`user_id`),
  KEY `dompettransaksi_admin_id` (`proceed_by`),
  CONSTRAINT `dompettransaksi_admin_id` FOREIGN KEY (`proceed_by`) REFERENCES `user_admin` (`id`),
  CONSTRAINT `dompettransaksi_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi transaksi yang menggunakan saldo pada dompet viaramall';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.email_setting
DROP TABLE IF EXISTS `email_setting`;
CREATE TABLE IF NOT EXISTS `email_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_sender` varchar(50) NOT NULL,
  `smtp_host` varchar(100) NOT NULL,
  `smtp_port` smallint(6) NOT NULL,
  `smtp_timeout` smallint(6) NOT NULL,
  `email_password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='setting untuk pengiriman email';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.feedback
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `store_product_id` int(11) NOT NULL,
  `review` tinytext NOT NULL,
  `stars` tinyint(4) NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='feedback / review dari user yang membeli produk';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.general_data
DROP TABLE IF EXISTS `general_data`;
CREATE TABLE IF NOT EXISTS `general_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` varchar(200) NOT NULL,
  `created_date` date NOT NULL,
  `admin_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='data data yang umum digunakan di aplikasi seperti nomor telepon, alamat dan data-data umum lainnya';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.image_produk
DROP TABLE IF EXISTS `image_produk`;
CREATE TABLE IF NOT EXISTS `image_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `store_produk_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_produk_id` (`store_produk_id`),
  CONSTRAINT `store_produk_id` FOREIGN KEY (`store_produk_id`) REFERENCES `store_produk` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.kategori
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.payment
DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL,
  `transaksi_id` int(11) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_bank` varchar(255) DEFAULT NULL,
  `payment_rek` varchar(255) DEFAULT NULL,
  `payment_struk` varchar(255) DEFAULT NULL COMMENT 'bukti struk pembayaran yang diberikan user',
  `payment_cost` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL COMMENT 'admin yang approve pembayaran dari user',
  `created_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT 'waiting' COMMENT 'waiting, lunas, cancel',
  PRIMARY KEY (`id`),
  KEY `transaksi_id` (`transaksi_id`),
  CONSTRAINT `transaksi_id` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.point
DROP TABLE IF EXISTS `point`;
CREATE TABLE IF NOT EXISTS `point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `value` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `point_user_id` (`user_id`),
  CONSTRAINT `point_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.point_transaksi
DROP TABLE IF EXISTS `point_transaksi`;
CREATE TABLE IF NOT EXISTS `point_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity` varchar(50) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pointtransaksi_user_id` (`user_id`),
  CONSTRAINT `pointtransaksi_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi pendapatan point user viaramall';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store
DROP TABLE IF EXISTS `store`;
CREATE TABLE IF NOT EXISTS `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `store_name` varchar(30) NOT NULL,
  `description` varchar(144) DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  `img` varchar(50) DEFAULT NULL,
  `location` smallint(6) DEFAULT NULL COMMENT 'lokasi toko',
  PRIMARY KEY (`id`),
  KEY `seller_id` (`seller_id`),
  CONSTRAINT `seller_id` FOREIGN KEY (`seller_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi store yang dimiliki user';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_courir
DROP TABLE IF EXISTS `store_courir`;
CREATE TABLE IF NOT EXISTS `store_courir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` smallint(6) NOT NULL,
  `courir_agent_id` smallint(6) NOT NULL,
  `courir_packet_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='agen pengiriman yang dipakai oleh toko';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_etalase
DROP TABLE IF EXISTS `store_etalase`;
CREATE TABLE IF NOT EXISTS `store_etalase` (
  `id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `etalase_name` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='etalase toko contoh: satu toko ada etalase xiaomi, samsung, sony, meizu';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_invoice
DROP TABLE IF EXISTS `store_invoice`;
CREATE TABLE IF NOT EXISTS `store_invoice` (
  `id` int(11) DEFAULT NULL,
  `transaksi_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  KEY `transaksiinvoice_transaksi_id` (`transaksi_id`),
  KEY `transaksiinvoice_invoice_id` (`store_id`),
  CONSTRAINT `transaksiinvoice_invoice_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `transaksiinvoice_transaksi_id` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi invoice dan status pengiriman';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_notifikasi
DROP TABLE IF EXISTS `store_notifikasi`;
CREATE TABLE IF NOT EXISTS `store_notifikasi` (
  `id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `notif` varchar(200) DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `id` (`id`),
  KEY `storenotif_read_id` (`store_id`),
  CONSTRAINT `storenotif_read_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='notifikasi untuk tiap2 store';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_notifikasi_read
DROP TABLE IF EXISTS `store_notifikasi_read`;
CREATE TABLE IF NOT EXISTS `store_notifikasi_read` (
  `id` int(11) DEFAULT NULL,
  `store_notifikasi_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  KEY `store_notifikasi_id` (`store_notifikasi_id`),
  KEY `storenotifikasi_store_id` (`store_id`),
  CONSTRAINT `storenotifikasi_store_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `store_notifikasi_id` FOREIGN KEY (`store_notifikasi_id`) REFERENCES `store_notifikasi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='notifikasi yang telah di read';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_produk
DROP TABLE IF EXISTS `store_produk`;
CREATE TABLE IF NOT EXISTS `store_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL COMMENT 'termasuk ukuran produk ada disini',
  `kategori_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `store_etalase_id` int(11) DEFAULT NULL COMMENT 'produk ini masuk etalase mana',
  `harga` int(11) NOT NULL,
  `asuransi` enum('y','n') NOT NULL,
  `kondisi` enum('baru','bekas') NOT NULL,
  `pengembalian` enum('y','n') NOT NULL,
  `berat` smallint(6) NOT NULL,
  `berat_tipe` varchar(50) NOT NULL,
  `stok` smallint(6) NOT NULL,
  `minimum_order` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`),
  KEY `kategori_id` (`kategori_id`),
  CONSTRAINT `kategori_id` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi produk yang ada di viaramall';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.store_routes
DROP TABLE IF EXISTS `store_routes`;
CREATE TABLE IF NOT EXISTS `store_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_store` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` varchar(50) DEFAULT 'aktif',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.transaksi
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE IF NOT EXISTS `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` varchar(50) DEFAULT NULL COMMENT 'jenis aktifitas yang terjadi',
  `activity_info` varchar(200) DEFAULT NULL COMMENT 'info tambahan aktifitas',
  `activity_status` char(50) DEFAULT '---',
  `store_produk_id` int(11) DEFAULT NULL,
  `product_count` tinyint(4) DEFAULT NULL,
  `product_cost` int(11) DEFAULT NULL,
  `payment_cost` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `store_product_id` (`store_produk_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `coupon_id` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`),
  CONSTRAINT `store_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `store_product_id` FOREIGN KEY (`store_produk_id`) REFERENCES `store_produk` (`id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi transaksi yang terjadi di store user viaramall\r\ntransaksi yang terjadi hanya pembelian';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `join_date` datetime NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `sex` enum('m','f') DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  `avatar` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='informasi user viaramall';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.user_address
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `marked_as` varchar(50) NOT NULL COMMENT 'penanda alamat, contoh: alamat kantor',
  `regency_id` int(11) NOT NULL,
  `subdistrict_id` int(11) NOT NULL,
  `village_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='alamat user untuk pengiriman';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.user_admin
DROP TABLE IF EXISTS `user_admin`;
CREATE TABLE IF NOT EXISTS `user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `sex` enum('m','f') DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table viaramall.user_bank
DROP TABLE IF EXISTS `user_bank`;
CREATE TABLE IF NOT EXISTS `user_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bank_id` tinyint(4) DEFAULT NULL,
  `bank_rekening` varchar(20) DEFAULT NULL,
  `nama_rekening` varchar(100) DEFAULT NULL COMMENT 'nama pemilik rekening',
  `created_date` datetime DEFAULT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='bank yang dipakai oleh user untuk melakukan pembayaran';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.user_register_temp
DROP TABLE IF EXISTS `user_register_temp`;
CREATE TABLE IF NOT EXISTS `user_register_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `link` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `expired_date` datetime NOT NULL,
  `status` enum('y','n') DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='temporary link registration ';

-- Data exporting was unselected.


-- Dumping structure for table viaramall.user_telp
DROP TABLE IF EXISTS `user_telp`;
CREATE TABLE IF NOT EXISTS `user_telp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `status` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='telepon user';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
