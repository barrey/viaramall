-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_rumahone6
CREATE DATABASE IF NOT EXISTS `db_rumahone6` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_rumahone6`;


-- Dumping structure for table db_rumahone6.tb_province
CREATE TABLE IF NOT EXISTS `tb_province` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `del` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rumahone6.tb_province: ~34 rows (approximately)
/*!40000 ALTER TABLE `tb_province` DISABLE KEYS */;
REPLACE INTO `tb_province` (`id`, `name`, `del`) VALUES
	(1, 'Nanggroe Aceh Darussalam', 'n'),
	(2, 'Sumatera Utara', 'n'),
	(3, 'Sumatera Barat', 'n'),
	(4, 'Riau', 'n'),
	(5, 'Kepulauan Riau', 'n'),
	(6, 'Kepulauan Bangka-Belitung', 'n'),
	(7, 'Jambi', 'n'),
	(8, 'Bengkulu', 'n'),
	(9, 'Sumatera Selatan', 'n'),
	(10, 'Lampung', 'n'),
	(11, 'Banten', 'n'),
	(12, 'DKI Jakarta', 'n'),
	(13, 'Jawa Barat', 'n'),
	(14, 'Jawa Tengah', 'n'),
	(15, 'Daerah Istimewa Yogyakarta  ', 'n'),
	(16, 'Jawa Timur', 'n'),
	(17, 'Bali', 'n'),
	(18, 'Nusa Tenggara Barat', 'n'),
	(19, 'Nusa Tenggara Timur', 'n'),
	(20, 'Kalimantan Barat', 'n'),
	(21, 'Kalimantan Tengah', 'n'),
	(22, 'Kalimantan Selatan', 'n'),
	(23, 'Kalimantan Timur', 'n'),
	(24, 'Gorontalo', 'n'),
	(25, 'Sulawesi Selatan', 'n'),
	(26, 'Sulawesi Tenggara', 'n'),
	(27, 'Sulawesi Tengah', 'n'),
	(28, 'Sulawesi Utara', 'n'),
	(29, 'Sulawesi Barat', 'n'),
	(30, 'Maluku', 'n'),
	(31, 'Maluku Utara', 'n'),
	(32, 'Papua Barat', 'n'),
	(33, 'Papua', 'n'),
	(34, 'Kalimantan Utara', 'n');
/*!40000 ALTER TABLE `tb_province` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
